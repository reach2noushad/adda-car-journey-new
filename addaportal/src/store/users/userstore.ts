
import AppDispatcher from '../../app/dispatcher';
import storeBase from '../base/storebase';
import actions from '../../actions/base/actiontypes';
import { IPlayerInfo, ICurrentUserProfile } from '../../interface/interface';
import storageManager from '../base/sessionstoremanager';


class UserStore extends storeBase {

    constructor() {
        super();
        this.dispatchToken = AppDispatcher.register(this.handleActions);


    }

    private playerList: IPlayerInfo[] = [];
    private showModalCloseBtn = true;
    private _bookingId: any = '';
    handleActions = (payload: any) => {

        let action = payload.action;

        switch (action.actionType) {

            case actions.SEND_CAR_QUOTATION:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SAVE_BOOKING:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.GET_ALL_BOOKINGS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.GET_BOOKING_STATUS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.BLOCKCHAIN_TIMELINE:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SEND_VERIFICATION_REQUEST:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.GET_MOBILE_RESPONSE:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SAVE_LOAN_DETAILS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.CHECK_APPROVE_TRANSACTION_STATUS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.GET_INSURANCE_QR:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.INSURANCE_CONFRIMATION_DETAILS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.CONFRIM_INSURANCE:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.CHECK_CONFIRM_INSURANCE_STATUS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SEND_CREDENTIAL:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SAVE_INSURANCE_DETAILS:
                this.emitChange(action.actionType, action.data)
                break;

            //police
            case actions.GET_POLICE_QR:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.POLICE_CONFIRMATION_DETAILS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.CONFRIM_REGISTRATION:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.CHECK_CONFIRM_REGISTRATION_STATUS:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SAVE_REGISTRATION_DETAILS:
                this.emitChange(action.actionType, action.data)
                break;
        }

        return true; // No errors. Needed by promise in Dispatcher.
    }

    public setModalCloseBtn(value: any) {
        this.showModalCloseBtn = value;
    }

    public get modalCloseBtn() {
        return this.showModalCloseBtn;
    }

    public setBookingId(bookingId: any) {
        this._bookingId = bookingId;
        storageManager.setValue('bookingId', bookingId);
    }

    public get bookingId() {
        if (this._bookingId) {
            return this._bookingId;
        } else {
            return storageManager.getValue('bookingId');
        }
    }
}

export default new UserStore();
