
import AppDispatcher from '../../app/dispatcher';
import storeBase from '../base/storebase';
import actions from '../../actions/base/actiontypes';
import storageManager from '../base/sessionstoremanager';
import { IDialogues } from '../../interface/interface'
class LoginStore extends storeBase {
    constructor() {
        super();
        this.dispatchToken = AppDispatcher.register(this.handleActions);
    }
    //sets loggedin user info.for the time being it is setting only the role vale
    private loggedInUserInfo: any;
    private _userRole: any;
    private _userId: any;
    private userInfo: any;
    private _socketStatus: boolean;
    private _userLoginDetails: any;
    private _dialogues: IDialogues
    private _roleMaps: any;
    handleActions = (payload: any) => {

        let action = payload.action;
        switch (action.actionType) {
            // case actions.LOGIN:
            //     this.setloggedInUser(action.data);
            //     this.emitChange(action.actionType, action.data);
            //     break;
            case actions.QRCODE:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.CHECK_STATUS:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.LOGOUT:

                storageManager.clearStorage();
                //this.emitChange(action.actionType)
                break;
            case actions.OPEN_MODAL:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.CLOSE_MODAL:
                this.emitChange(action.actionType);
                break;
            case actions.OPEN_CONFIRM:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.OPEN_DELETE_CONFIRM:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.OPEN_SIGNOUT:
                this.emitChange(action.actionType, action.data);
                break
            case actions.CLOSE_CONFIRM:
                this.emitChange(action.actionType);
                break;
            case actions.OPEN_ALERT:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.CLOSE_ALERT:
                this.emitChange(action.actionType);
                break;
            case actions.ON_MODAL_CLOSE:
                this.emitChange(action.actionType);
                break;
            case actions.SEND_TRANSACTION:
                this.emitChange(action.actionType,action.data);
                break;
            case actions.CHECK_TRANSACTION_STATUS:
                this.emitChange(action.actionType,action.data);
                break;
        }

        return true; // No errors. Needed by promise in Dispatcher.
    }


    //used for setting the roleID for the time being
    public setloggedInUser(user: any) {
        // this._userRole = user.role;
        this._userId = user.userId;
        storageManager.setValue('userInfo', user);
    }

    public get userId() {

        if (this._userId) {
            return this._userId;

        } else {
            return storageManager.getValue('userInfo').userId;
        }
    }

    public setUserLogginDetails(obj: any) {
        this._userLoginDetails = obj;
    }
    public get userLoginDetails() {
        return this._userLoginDetails;
    }


}

export default new LoginStore();
