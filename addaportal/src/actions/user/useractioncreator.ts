import ActionBase from '../base/actionbase';
import actions from '../base/actiontypes';
import UserApi from '../../api/userinfoapi';

import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import { IConfirmationModalData } from '../../interface/interface';
import loginStore from '../../store/login/loginstore';

import userStore from '../../store/users/userstore';
import authentication from '../../api/authentication';
import { any } from 'prop-types';

class UserActionCreator extends ActionBase {
    
    /**
     * user logout
    */
    userSignOut = () => {
        this.dispatchAction(actions.LOGOUT,{});
        // UserInfoApi.userLogout((response: any) => {
        //     this.dispatchAction(actions.LOGOUT, response);
        // });
    }
    sendTransaction = (data:any) => {
       
        UserApi.sendTransaction(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_TRANSACTION, response);
            }
            
        });
    }
    checkTransactionStatus=(data:any)=>{
        UserApi.checkTransactionStatus(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CHECK_TRANSACTION_STATUS, response);
            }
            
        });
    }
    checkApproveTransactionStatus=(data:any)=>{
        UserApi.checkTransactionStatus(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CHECK_APPROVE_TRANSACTION_STATUS, response);
            }
            
        });
    }
    sendCarQuotation=(data:any)=>{
        UserApi.sendCarQuotation(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_CAR_QUOTATION, response);
            }
            
        });
    }
    saveBooking=(data:any)=>{
        UserApi.saveBooking(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SAVE_BOOKING, response);
            }
            
        });
    }
    getAllBookings=(useraddress:any)=>{
        UserApi.getAllBookings(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_ALL_BOOKINGS, response);
            }
            
        });
    }
    getBookingStatus=(bookingId:any)=>{
        UserApi.getBookingStatus(bookingId,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_BOOKING_STATUS, response);
            }
            
        });
    }
    blockChainTimeline=(bookingId:any)=>{
        UserApi.blockChainTimeline(bookingId,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.BLOCKCHAIN_TIMELINE, response);
            }
            
        });
    }
    sendVerificationRequest=(useraddress:any)=>{
        UserApi.sendVerificationRequest(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_VERIFICATION_REQUEST, response);
            }
            
        });
    }
    getMobileResponse=(useraddress:any)=>{
        UserApi.getMobileResponse(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_MOBILE_RESPONSE, response);
            }
            
        });
    }
    saveLoanDetails=(data:any)=>{
        UserApi.saveLoanDetails(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SAVE_LOAN_DETAILS, response);
            }
            
        });
    }
    
    getInsuranceQr=(useraddress:any)=>{
        UserApi.getInsuranceQr(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_INSURANCE_QR, response);
            }
            
        });
    }
    getInsuranceConfirmationDetails=(useraddress:any)=>{
        UserApi.getInsuranceConfirmationDetails(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.INSURANCE_CONFRIMATION_DETAILS, response);
            }
            
        });
    }
    confirmInsurance = (data:any) => {
       
        UserApi.confirmInsurance(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CONFRIM_INSURANCE, response);
            }
            
        });
    }
    checkConfirmInsuranceStatus=(data:any)=>{
        UserApi.checkTransactionStatus(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CHECK_CONFIRM_INSURANCE_STATUS, response);
            }
            
        });
    }
    sendCredential=(data:any)=>{
        UserApi.sendCredential(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_CREDENTIAL, response);
            }
            
        });
    }
    saveInsuranceDetails=(data:any)=>{
        UserApi.saveInsuranceDetails(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SAVE_INSURANCE_DETAILS, response);
            }
            
        });
    }


    //police
    getPoliceQr=(useraddress:any)=>{
        UserApi.getPoliceQr(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_POLICE_QR, response);
            }
            
        });
    }
    getPoliceConfirmationDetails=(useraddress:any)=>{
        UserApi.getPoliceConfirmationDetails(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.POLICE_CONFIRMATION_DETAILS, response);
            }
            
        });
    }
    confirmRegistration = (data:any) => {
       
        UserApi.confirmRegistration(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CONFRIM_REGISTRATION, response);
            }
            
        });
    }
    checkConfirmRegistrationStatus=(data:any)=>{
        UserApi.checkTransactionStatus(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CHECK_CONFIRM_REGISTRATION_STATUS, response);
            }
            
        });
    }
    // sendCredential=(data:any)=>{
    //     UserApi.sendCredential(data,(response: any) => {
    //         if(response)
    //         {
    //             this.dispatchAction(actions.SEND_CREDENTIAL, response);
    //         }
            
    //     });
    // }
    saveRegistrationDetails=(data:any)=>{
        UserApi.saveRegistrationDetails(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SAVE_REGISTRATION_DETAILS, response);
            }
            
        });
    }
}
export default new UserActionCreator();