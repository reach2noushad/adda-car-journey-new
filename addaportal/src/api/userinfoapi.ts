import Promise from './base/promise'
import { URLs } from './base/urls'

import { config } from './base/config';



class UserApi extends Promise {
    constructor() {
        super()
    }
    
    // userLogout = (callback: Function) => {
    //     this.post(URLs.LOGOUT, callback, {});
    // }
    sendTransaction=(data:any,callback: Function)=>{
        
        this.post(URLs.SENDTRANSACTION, callback, data);
    }
    checkTransactionStatus=(data:any,callback: Function)=>{
        
        this.get(`mobile/isresponsereceived?useraddress=${data}`, callback);
    }
    sendCarQuotation=(data:any,callback: Function)=>{
        
        this.post(`mobile/sendcredential`, callback,data);
    }
    saveBooking=(data:any,callback: Function)=>{
        
        this.post(`bookings/save`, callback,data);
    }
    getAllBookings=(useraddress:any,callback: Function)=>{
        this.get(`user/bookings?useraddress=${useraddress}`, callback);
    }
    getBookingStatus=(bookingId:any,callback: Function)=>{
        this.get(`bookings/status?id=${bookingId}`, callback);
    }
    blockChainTimeline=(bookingId:any,callback: Function)=>{
        this.get(`bookings/blockchaintimeline?id=${bookingId}`, callback);
    }
    sendVerificationRequest=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/bank?useraddress=${useraddress}`, callback);
    }
    getMobileResponse=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/bank/mobileresponse?useraddress=${useraddress}`, callback);
    }
    saveLoanDetails=(data:any,callback: Function)=>{
        
        this.post(`loan/save`, callback,data);
    }
    getInsuranceQr=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/insurance?useraddress=${useraddress}`, callback);
    }
    getInsuranceConfirmationDetails=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/insurance/mobileresponse?useraddress=${useraddress}`, callback);
    }
    confirmInsurance=(data:any,callback: Function)=>{
        
        this.post(URLs.SENDTRANSACTION, callback, data);
    }
    sendCredential=(data:any,callback: Function)=>{
        
        this.post(`mobile/sendcredential`, callback, data);
    }
    saveInsuranceDetails=(data:any,callback: Function)=>{
        
        this.post(`insurance/save`, callback,data);
    }
    getPoliceQr=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/police?useraddress=${useraddress}`, callback);
    }

    //police
    getPoliceConfirmationDetails=(useraddress:any,callback: Function)=>{
        this.get(`verificationrequest/police/mobileresponse?useraddress=${useraddress}`, callback);
    }
    confirmRegistration=(data:any,callback: Function)=>{
        
        this.post(URLs.SENDTRANSACTION, callback, data);
    }
    // sendCredential=(data:any,callback: Function)=>{
        
    //     this.post(`mobile/sendcredential`, callback, data);
    // }
    saveRegistrationDetails=(data:any,callback: Function)=>{
        
        this.post(`police/save`, callback,data);
    }
    
}

export default new UserApi()