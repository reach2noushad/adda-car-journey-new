export module URLs {
    export const AUTHENTICATION = 'api/Users/login';
    export const LOGOUT = 'api/Users/logout';
    export const SENDTRANSACTION = 'mobile/sendtransaction';
}

// game id: 'a188289a-d98d-11e6-b686-497e3def9cc0'