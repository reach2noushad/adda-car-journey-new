import Promise from './base/promise'
import { URLs } from './base/urls'

class Authentication extends Promise {
    constructor() {
        super()
    }

    getQrCode = (loginsessionid:any, callback: Function) => {
        this.get(`login/qrcode?loginsessionid=${loginsessionid}`, callback)
    }
    checkLoginStatus = (loginsessionid:any, callback: Function) => {
        this.get(`login/status?loginsessionid=${loginsessionid}`, callback)
    }
}

export default new Authentication()
