import * as React from 'react';
import Header from '../includes/header';
import CarDetails from './cardetails';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
'reactstrap';
import userStore from '../../store/users/userstore';
//var Modal = require('react-bootstrap-modal')
import actions from '../../actions/base/actiontypes';
import userActionCreator from '../../actions/user/useractioncreator';

export default class ProcessDetails extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
    super(props, context);
    this.state = {
    showPopup: false,
    modal: false,

    };
    this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        userStore.addChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);
        
        this.checkTimelineStatusInterval = setInterval(this.checkTimelineStatus, 3000);
    };
    
    componentWillUnmount() {
        userStore.removeChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);
    }
    private checkTimelineStatusInterval: any;
    private checkTimelineStatus = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        else{
            clearInterval(this.checkTimelineStatusInterval);
        }
    }
    setTimelineData = (response: any) => {
        console.log('setTimelineDataAfterBooking', response);
        if(response && response.details.status=='loanapproved')
        {
            clearInterval(this.checkTimelineStatusInterval);
            this.props.setTimelineData(response.details);
            
        }
    }
    



    togglePopup() {
    this.setState({
    showPopup: !this.state.showPopup
    });
    }
    private cancel = () => {
    //ModalPopupActionCreator.closeModal()
    }
    toggle() {
    this.setState({
    modal: !this.state.modal
    });

    }

    render() {

    return (

        
        <div className="dealerDetailsContainer">
            <div className="dealerDetailsWraper">
                <div className="dealerTilteWraper">
                    <div className="titleWraper">
                        <h2>Car Loan Details</h2>
                            <span className="status processing">PROCESSING</span>
                            {/* <span className="status approved">PROCESSING</span> */}
                    </div>
                        {/* <div className="export">
                            <Button className="exportbtn" outline color="warning">EXPORT TO MOBILE</Button>
                        </div> */}
                </div>


                    {/* police verification -- licence plate  */}

                    <div className="verification">

                        {/* <div className="contentWraper">
                         <p className="mainTitle">Your license plate</p>
                         <div className="numPlate"></div>
                        </div>

                        <div className="contentWraper">
                            <p className="mainTitle">Vehicle Registration Reference</p>
                            <span className="subText">CAR988R54</span>
                        </div> */}

                            {/* page details  */}

                        <div className="detailsPortion">
                            <div className="detailstitleWraper">
                                <p className="detailsMaintitle registation">Loan Details <span className="date">Applied on {this.props.timelineData.loanapplieddate}</span> </p> 
                            </div>

                            <div className="detailsContent">

                            <div className="contents">
                                    <p className="contentMain">Loan Reference Number</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.loandetails.loanreferencenumber}</span>
                            </div>
                            <div className="contents">
                                    <p className="contentMain">Bank Name</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.loandetails.bankname}</span>
                            </div>
                                {/* <div className="contents">
                                    <p className="contentMain">Car Manufacturer</p>
                                    <span className="contentSub">Toyota</span>
                                </div>
                                <div className="contents">
                                    <p className="contentMain">Car Manufacturer</p>
                                    <span className="contentSub">Toyota</span>
                                </div> */}

                            </div>
                        </div>
                        <div className="detailsPortion">
                            <div className="detailstitleWraper">
                                <p className="detailsMaintitle registation">Car Details </p> 
                            </div>

                            <div className="detailsContent">

                            <div className="contents">
                                    <p className="contentMain">Dealer</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.carbookingdetails.cardealer}</span>
                            </div>
                            <div className="contents">
                                    <p className="contentMain">Booking Id</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.carbookingdetails.bookingId}</span>
                            </div>
                                <div className="contents">
                                    <p className="contentMain">Car Manufacturer</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.carbookingdetails.carmake}</span>
                                </div>
                                <div className="contents">
                                    <p className="contentMain">Model</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.carbookingdetails.carmodel}</span>
                                </div>
                                <div className="contents">
                                    <p className="contentMain">Ex-Showroom Price (AED)</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.carbookingdetails.carprice}</span>
                                </div>
                            </div>
                        </div>

                    </div>

            </div>


        </div>


        
    )
    }

    }