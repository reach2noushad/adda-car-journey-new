import * as React from 'react';
import Header from '../includes/header';
import CarDetails from './cardetails';
import ProcessDetails from './processdetails';
import RegistrationApplied from '../registration/registrationapplied'
import RegistrationCompleted from '../registration/registrationcompleted'
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import './car.css';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
    'reactstrap';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import userStore from '../../store/users/userstore';
import Timeline from '../../components/includes/timeline';
import FinancingHome from '../financing/financinghome';
import InsuranceHome from '../insurance/insurancehome';
import RegistrationHome from '../registration/registrationhome';
//var Modal = require('react-bootstrap-modal')

export default class CarHome extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            showPopup: false,
            modal: false,
            active:false,
            timelineData:[],
            activeStage:'',

        };
        this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        //alert(userStore.bookingId);
    };

    componentWillUnmount() {

    }
    private timelineData:any=[];
    setTimelineData=(timelineData:any)=>{
       
        this.timelineData=timelineData
        this.setState({activeStage:timelineData.status});
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
    private cancel = () => {
        //ModalPopupActionCreator.closeModal()
    }
    toggle() {
        ModalPopupActionCreator.openModal(<CarDetails />, true, 'md')
        // this.setState({
        //     modal: !this.state.modal
        // });

    }
    
    private toggleHover = () => {
        this.setState({
            active: !this.state.active
        });
    }
    render() {

        return (

            <div className="carSection">
                <Timeline setTimelineData={this.setTimelineData}/>
                
                {this.state.activeStage=='' && <div className="dealerDetailsContainer">
                    <div className="dealerDetailsWraper">
                        <div className="dealerTilteWraper">
                            <div className="titleWraper">
                                <h2>Select a Dealer to begin</h2>
                            </div>
                            <div className="searchWraper">
                                <FormGroup>
                                    <Input type="search" className="search" id="Search" placeholder="Search car dealers" />
                                </FormGroup>
                            </div>

                        </div>
                        <div className="applyCardContainer">
                            <div className="cardLayout cp" onMouseEnter={this.toggleHover}
                                onMouseLeave={this.toggleHover} onClick={this.toggle}>
                                <div className="cardContainer">
                                    <div className={this.state.active ? "card active" : "card"}>
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                            <img src="/public/images/alfahim/alfahimlogo.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">ALFAHIM Motors</h3>
                                                
                                                {this.state.active && <div className="siteNav active">
                                                    <a >Visit Website</a>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout">
                                <div className="cardContainer">
                                    <div className="card  ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img src="/public/images/habtoor.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">Al Habtoor Royal Car LLC</h3>
                                                
                                                <div className="siteNav ">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout">
                                <div className="cardContainer">
                                    <div className="card  ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img src="/public/images/masaood.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">Al Masaood Automobiles Company LLC</h3>
                                                
                                                <div className="siteNav ">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>


                </div>

        }
        {this.state.activeStage=='carquotationsaved' && <FinancingHome/>
        }
        
        {this.state.activeStage=='waitingforloan' && <ProcessDetails timelineData={this.timelineData} setTimelineData={this.setTimelineData}/>
        }
        {this.state.activeStage=='loanapproved' && <InsuranceHome/>
        }
        
        {this.state.activeStage=='insurancereceived' && <RegistrationHome/>
        }
        
        {this.state.activeStage=='vehicleregistrationapplied' && <RegistrationApplied timelineData={this.timelineData} setTimelineData={this.setTimelineData}/>
        }
        {this.state.activeStage=='completed' && <RegistrationCompleted timelineData={this.timelineData} setTimelineData={this.setTimelineData}/>
        }
                <div>
                    
                </div>
            </div>
        )
    }

}