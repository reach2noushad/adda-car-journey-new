import * as React from 'react';
import './timeline.css';
import userActionCreator from '../../actions/user/useractioncreator';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import userStore from '../../store/users/userstore';
import actions from '../../actions/base/actiontypes';
import { withRouter } from 'react-router-dom';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
class BlockchainTimeline extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.toggle = this.toggle.bind(this);
        this.state = {
            //collapse: false,
            collapseCarQuotation:false,
            collapseLoanApplication:false,
            collapseLoanApproval:false,
            collapseInsuranceRequest:false,
            collapseRegistrationRequest:false,
            collapseRegistrationApproval:false,
            blockchainData: {
                // "status": "Vehicle License Requested",
                // "nextstage": "Vehicle License Approved",
                // "carquote": {
                //     "date": "26-08-2019",
                //     "blockdetails": {
                //         "transactionhash": "0x63c0c30ff0ca52da4b18a8e0f2e6f145186c9a503e6a13a39885ac9516015b2c",
                //         "transactionstatus": "success",
                //         "blocknumber": "5159066",
                //         "timestamp": "12 Sept 2017 7:30 PM"
                //     }
                // },
                // "loanapplication": {
                //     "date": "26-08-2019",
                //     "blockdetails": {
                //         "transactionhash": "0x63c0c30ff0ca52da4b18a8e0f2e6f145186c9a503e6a13a39885ac9516015b2c",
                //         "transactionstatus": "success",
                //         "blocknumber": "5159066",
                //         "timestamp": "12 Sept 2017 7:30 PM"
                //     }
                // },
                // "loanapproval": {
                //     "date": "26-08-2019",
                //     "blockdetails": {
                //         "transactionhash": "0x63c0c30ff0ca52da4b18a8e0f2e6f145186c9a503e6a13a39885ac9516015b2c",
                //         "transactionstatus": "success",
                //         "blocknumber": "5159066",
                //         "timestamp": "12 Sept 2017 7:30 PM"
                //     }

                // },
                // "insurance": {
                //     "date": "26-08-2019",
                //     "blockdetails": {
                //         "transactionhash": "0x63c0c30ff0ca52da4b18a8e0f2e6f145186c9a503e6a13a39885ac9516015b2c",
                //         "transactionstatus": "success",
                //         "blocknumber": "5159066",
                //         "timestamp": "12 Sept 2017 7:30 PM"
                //     }
                // },
                // "vehiclelicenserequest": {
                //     "date": "26-08-2019",
                //     "blockdetails": {
                //         "transactionhash": "0x63c0c30ff0ca52da4b18a8e0f2e6f145186c9a503e6a13a39885ac9516015b2c",
                //         "transactionstatus": "success",
                //         "blocknumber": "5159066",
                //         "timestamp": "12 Sept 2017 7:30 PM"
                //     }

                // },
                // "vehiclelicenseapproval": {
                //     "date": '',
                //     "blockdetails": {
                //         "transactionhash": '',
                //         "transactionstatus": '',
                //         "blocknumber": '',
                //         "timestamp": '',
                //     }

                // }
            }
        };
        
    }
    componentDidMount() {
        userStore.addChangeListener(actions.BLOCKCHAIN_TIMELINE, this.setBlockchainTimelineData);
        userActionCreator.blockChainTimeline(userStore.bookingId);
        //userActionCreator.blockChainTimeline(125);
        //let response =  


    };

    componentWillUnmount() {
        userStore.removeChangeListener(actions.BLOCKCHAIN_TIMELINE, this.setBlockchainTimelineData);
    }
    setBlockchainTimelineData = (response: any) => {


        if (response) {
            this.setState({
                blockchainData: response.details,
            })
            console.log('setBlockchainTimelineData', response)
        }
    }
    toggle(activeStage:any) {
        
        if(activeStage=='collapseCarQuotation')
        {
            this.setState({ collapseCarQuotation: !this.state.collapseCarQuotation });
        }
        else if(activeStage=='collapseLoanApplication')
        {
            this.setState({ collapseLoanApplication: !this.state.collapseLoanApplication });
        }
        else if(activeStage=='collapseLoanApproval')
        {
            this.setState({ collapseLoanApproval: !this.state.collapseLoanApproval });
        }
        else if(activeStage=='collapseInsuranceRequest')
        {
            this.setState({ collapseInsuranceRequest: !this.state.collapseInsuranceRequest });
        }
        else if(activeStage=='collapseRegistrationRequest')
        {
            this.setState({ collapseRegistrationRequest: !this.state.collapseRegistrationRequest });
        }
        else if(activeStage=='collapseRegistrationApproval')
        {
            this.setState({ collapseRegistrationApproval: !this.state.collapseRegistrationApproval });
        }
    }
    private backToPage = () => {
        let routesArr = RouterBase.getRoutesArray();

        if (routesArr.length > 1) {
            this.props.history.push(routesArr[routesArr.length - 2]);
            RouterBase.changeRoute(routesArr[routesArr.length - 2]);

        }
        else {

            RouterBase.changeRoute(constants.ROUTES.USERHOME);
            this.props.history.push(constants.ROUTES.USERHOME);
        }
    }
    render() {
        console.log('this.state.blockchainData',this.state.blockchainData);
        if(this.state.blockchainData.status)
        {

        
        return (

            <div className="carSection blockChainContainer">
                <div className="steperContainer">
                    <div className="steperDataContainer bookingStatusBar">
                        <Button onClick={this.backToPage} color="link" className="stepetBackbtn">Back</Button>
                        <h3 className="steperDatatilte">Buy a New Car</h3>

                        <div className="statusbox">
                            <span className="statusTitle">Booking ID</span>
                            <p className="statusSub">{userStore.bookingId}</p>
                        </div>
                        <div className="statusbox">
                            <p className="statusTitle">Current Status</p>
                            <span className="homeLoanbtn btn btn-outline-secondary">{this.state.blockchainData.status}</span>
                        </div>
                    </div>
                </div>
                <div className="dealerDetailsContainer">
                    <div className="dealerDetailsWraper">
                        <div className="dealerTilteWraper">
                            <div className="titleWraper">
                                <h2>Blockchain Timeline </h2>
                            </div>
                        </div>

                        <div className="blockchainStepper">
                            {/* <div className="step ">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel active">Next: Loan Application Approval</p>

                                </div>
                            </div> */}
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Car Registration Approval</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.vehiclelicenseapproval.date}</span>

                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseRegistrationApproval')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.vehiclelicenseapproval.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
                            <i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseRegistrationApproval}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenseapproval.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenseapproval.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenseapproval.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Car Registration Request</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.vehiclelicenserequest.date}</span>

                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseRegistrationRequest')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.vehiclelicenserequest.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
                            <i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseRegistrationRequest}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenserequest.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenserequest.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.vehiclelicenserequest.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Insurance Request</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.insurance.date}</span>

                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseInsuranceRequest')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.insurance.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
                            <i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseInsuranceRequest}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.insurance.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.insurance.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.insurance.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- active --> */}
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Loan Application Approval</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.loanapproval.date}</span>

                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseLoanApproval')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.loanapproval.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
                            <i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseLoanApproval}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapproval.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapproval.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapproval.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- active --> */}
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Loan Application Request</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.loanapplication.date}</span>

                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseLoanApplication')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.loanapplication.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
                            <i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseLoanApplication}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapplication.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapplication.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.loanapplication.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>



                            {/* <!-- regular --> */}
                            {/* <div className="step active"> */}
                            <div className="step completed">
                                <div className="v-stepper">
                                    <div className="circle"></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <p className="bChainLabel">Car Quotation Received</p>
                                    <span className="sublabel">Sent on: {this.state.blockchainData.carquote.date}</span>
                                    <div className="accordianWraper">

                                        <div className="toggleBtn" color="primary" onClick={this.toggle.bind(this,'collapseCarQuotation')} style={{}}>
                                            <div className="textWraper">
                                                <p className="bright">Transaction Hash</p>
                                                <span className="shade">{this.state.blockchainData.carquote.blockdetails.transactionhash}</span>
                                            </div>
                                            <div className="toggleIcon cp">View More
<i className="fa fa-caret-down"></i>
                                            </div>

                                        </div>

                                        <Collapse isOpen={this.state.collapseCarQuotation}>
                                            <Card>
                                                <CardBody>
                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle transaction">Transaction Status</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.carquote.blockdetails.transactionstatus}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle blocknum">Block Number</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.carquote.blockdetails.blocknumber}</span>
                                                    </div>


                                                    <div className="contentDown">
                                                        <p className="toggleDownTitle timestamp">Timestamp</p>
                                                        <span className="toggleDownStatus">{this.state.blockchainData.carquote.blockdetails.timestamp}</span>
                                                        {/* <span className="toggleDownStatus">07:05:53 AM +UTC</span> */}
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div>


                </div>
            </div>

        )
    }
    else{
        return '';
    }
    }

}
export default withRouter(BlockchainTimeline);