import * as React from 'react';
import { Modal,ModalHeader, ModalBody, ModalFooter,Button } from 'reactstrap';
import UtilityStore from '../../store/utility/utilitystore';
import actions from '../../actions/base/actiontypes';
// import modalPopupActionCreator from '../../actions/utility/modalpopupaction'
export default class ModalPopup extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        
        super(props, context);
        this.state = {
            showModal: false,
            component: null,
            title: '',
            modalClassName: '',
            modalSize: 'lg'
        }
    }

    componentDidMount() {
        UtilityStore.addChangeListener(actions.OPEN_MODAL, this.open);
        UtilityStore.addChangeListener(actions.CLOSE_MODAL, this.close);
    }

    componentWillUnmount() {
        UtilityStore.removeChangeListener(actions.OPEN_MODAL, this.open);
        UtilityStore.removeChangeListener(actions.CLOSE_MODAL, this.close);
    }
    private componentDetail: any = false;
    close = () => {
        // modalPopupActionCreator.onModalClose()
        if (this.componentDetail && this.componentDetail.Component && this.componentDetail.Component.props.onCloseModal) {
            this.componentDetail.Component.props.onCloseModal();
        }
        this.setState({ showModal: false, component: null }, () => {
            this.componentDetail = false;
        });
    }

    closeClick = () => {
        if (this.componentDetail && this.componentDetail.Component && this.componentDetail.Component.props.onCloseModal) {
            this.componentDetail.Component.props.onCloseModal();
        }
        if (this.componentDetail && this.componentDetail.Component && this.componentDetail.Component.props.confirmForCloseModal) {
            let closeModal = this.componentDetail.Component.props.confirmForCloseModal();
            if (closeModal) {
                this.setState({ showModal: false, component: null }, () => {
                    this.componentDetail = false;
                });
            }

        } else {
            this.setState({ showModal: false, component: null }, () => {
                this.componentDetail = false;
            });
        }
    }


    open = (componentDetail: any) => {
        this.componentDetail = componentDetail;
        console.log('this.componentDetail',this.componentDetail)
        let timer = 0;
        this.setState({
            showModal: true,
            component: null,
            title: componentDetail.Component.props.popupHead,
            hasCloseButton: componentDetail.hasCloseButton,
            modalClassName: componentDetail.Component.props.popupClassName,
            modalSize: componentDetail.modalSize ? componentDetail.modalSize : 'lg'
        }, () => {
            setTimeout(() => {
                this.setState({
                    component: componentDetail.Component,
                    title: componentDetail.Component.props.popupHead,
                    hasCloseButton: componentDetail.hasCloseButton,
                    modalClassName: componentDetail.Component.props.popupClassName,
                    modalSize: componentDetail.modalSize ? componentDetail.modalSize : 'lg'
                });
            }, timer);
        });
        
        // this.setState({
        //     showModal: true,
        //     component: null,
        //     title: componentDetail.Component.props.popupHead,
        //     hasCloseButton: componentDetail.hasCloseButton,
        //     modalClassName: componentDetail.Component.props.popupClassName,
        //     modalSize: componentDetail.modalSize ? componentDetail.modalSize : 'lg'
        // }, () => {
        //     setTimeout(() => {
        //         this.setState({
        //             component: componentDetail.Component,
        //             title: componentDetail.Component.props.popupHead,
        //             hasCloseButton: componentDetail.hasCloseButton,
        //             modalClassName: componentDetail.Component.props.popupClassName,
        //             modalSize: componentDetail.modalSize ? componentDetail.modalSize : 'lg'
        //         });
        //     }, timer);
        // });

    }

    render() {//showModalCloseBtn
        let hideStyle = this.state.hasCloseButton ? {} : { display: 'none' }
        //close button comes depends on toggle on header
        return (
            <Modal isOpen={this.state.showModal}  >
                <ModalHeader toggle={this.close}>
                     {/* <button type="button" className="close" onClick={this.closeClick} style={hideStyle}>&times;</button> */}
                     {this.state.title}
                 </ModalHeader>
                <ModalBody>
                    {this.state.component ? this.state.component :
                        <div style={{ minHeight: '275px' }}>
                            Loading!
                    </div>
                    }
                </ModalBody>
            </Modal>
            // <Modal show={this.state.showModal} onHide={this.close} bsSize={this.state.modalSize} backdrop="static" dialogClassName={this.state.modalClassName}>
            //     <ModalHeader>
            //         <button type="button" className="close" onClick={this.closeClick} style={hideStyle}>&times;</button>
            //         {this.state.title}
            //     </ModalHeader>
            //     <ModalBody>
            //         {this.state.component ? this.state.component :
            //             <div style={{ minHeight: '275px' }}>
            //                 Loading!
            //         </div>
            //         }
            //     </ModalBody>
            // </Modal>
    //         <div>
    //     <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button>
    //     <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
    //       <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
    //       <ModalBody>
    //         Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    //       </ModalBody>
    //       <ModalFooter>
    //         <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
    //         <Button color="secondary" onClick={this.toggle}>Cancel</Button>
    //       </ModalFooter>
    //     </Modal>
    //   </div>
        )
    }
}
