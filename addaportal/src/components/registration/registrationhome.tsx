import * as React from 'react';
import Header from '../includes/header';
//import CarDetails from './cardetails';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
    'reactstrap';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import userStore from '../../store/users/userstore';
import Timeline from '../../components/includes/timeline';
import PoliceDetails from '../../components/registration/policedetails';
//var Modal = require('react-bootstrap-modal')

export default class RegistrationHome extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            showPopup: false,
            modal: false,
            active: false,
            timelineData: [],
            activeStage: '',

        };
        this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        //alert(userStore.bookingId);
    };

    componentWillUnmount() {

    }
    private timelineData: any = [];
    setTimelineData = (timelineData: any) => {

        this.timelineData = timelineData
        this.setState({ activeStage: timelineData.status });
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
    private cancel = () => {
        //ModalPopupActionCreator.closeModal()
    }
    toggle() {
        ModalPopupActionCreator.openModal(<PoliceDetails />, true, 'md')
        // this.setState({
        //     modal: !this.state.modal
        // });

    }

    private toggleHover = () => {
        this.setState({
            active: !this.state.active
        });
    }
    render() {

        return (

                     <div className="dealerDetailsContainer">
                        <div className="dealerDetailsWraper">
                            <div className="dealerTilteWraper">
                                <div className="titleWraper">
                                    <h2>Register your car</h2>
                                </div>
                                

                            </div>
                            <div className="applyCardContainer">
                                <div className="cardLayout cp policeCard" onMouseEnter={this.toggleHover}
                                    onMouseLeave={this.toggleHover} onClick={this.toggle}>
                                    <div className="cardContainer">
                                <div className={this.state.active ? "card active" : "card"}>
                                            <div className="cardContent">
                                                <div className="image-wraper policeLogoWraper">
                                            <img src="/public/images/policeLogo.png" alt="" />
                                                </div>
                                                <div className="content-wraper">
                                                    <h3 className="cardContentTitle applytitle police-Title">Abu Dhabi Police</h3>
                                                 <span className="policeLink">https://es.adpolice.gov.ae</span>

                                                    {this.state.active &&
                                                         <div className="siteNav policeNav active">
                                                        <a >Register your vehicle</a>
                                                    </div>
                                                     } 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>


                    </div>

                
        )
    }

}