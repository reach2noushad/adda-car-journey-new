import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';



export default class RegistrationConfirmation extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {

            
        };
    }

    componentDidMount() {
       
    };

    componentWillUnmount() {
        
    }
    componentWillMount(){

    }
    
    render() {

        return (
            
                <div className="Navimages Banking">
            <img src="/public/images/Police Flow/Group 251.png" alt="" />
            <div className="personalBanking">

                {/* verified status cards */}

                <div className="detailsContainer">

                        <div className="docVerificationContainer">
                            <div className="verificationCard">
                                <div className="verificationTitleWraper">
                                    <p className="verificationTitle smartPass">SmartPass ID</p>
                                </div>
                                <div className="btnVerifiedWraper">
                                    <span className="statusLabel">VERIFIED</span>
                                </div>
                            </div>
                        </div>

                        <div className="docVerificationContainer">
                            <div className="verificationCard">
                                <div className="verificationTitleWraper">
                                    <p className="verificationTitle licence">Driving License</p>
                                </div>
                                <div className="btnVerifiedWraper">
                                    <span className="statusLabel">VERIFIED</span>
                                </div>
                            </div>
                        </div>


                        <div className="docVerificationContainer">
                            <div className="verificationCard">
                                <div className="verificationTitleWraper">
                                    <p className="verificationTitle bank">Insurance Receipt</p>
                                </div>
                                <div className="btnVerifiedWraper">
                                    <span className="statusLabel">VERIFIED</span>
                                </div>
                            </div>
                        </div>




                    <div className="expendDetails">
                        <p className="expandTitle car">Car Details</p>
                    </div>

                    <div className="detailsWraper">
                        <div className="content">
                            <p className="title">Dealer</p>
                            <span className="sub-title">{this.props.registrationDetails.cardetails.dealer}</span>
                        </div>

                        <div className="content">
                            <p className="title">Booking ID</p>
                            <span className="sub-title">{this.props.registrationDetails.cardetails.bookingid}</span>
                        </div>

                        <div className="content">
                            <p className="title">Car Manufacturer</p>
                            <span className="sub-title">{this.props.registrationDetails.cardetails.carmanufacturer}</span>
                        </div>
                        <div className="content">
                            <p className="title">Model</p>
                            <span className="sub-title">{this.props.registrationDetails.cardetails.model}</span>
                        </div>
                        <div className="content">
                            <p className="title">Ex-Showroom Price (AED)</p>
                            <span className="sub-title">{this.props.registrationDetails.cardetails.exshowroonprice}</span>
                        </div>
                        
                    </div>
                </div> 
                <div className="btnSection">
            <div className="btnContainer">
                <div className="btnWraper">
                        <Button className="link" onClick={this.props.cancel} color="link">Cancel</Button>
                        <Button className="submit" onClick={this.props.registrationConfirmation} color="danger">CONFIRM & SUBMIT</Button>
                </div>

            </div>
                    </div>

                </div>

            </div>
            
        )
    }




}