import * as React from 'react';
import Header from '../includes/header';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
'reactstrap';
import userStore from '../../store/users/userstore';
//var Modal = require('react-bootstrap-modal')
import actions from '../../actions/base/actiontypes';
import userActionCreator from '../../actions/user/useractioncreator';

export default class RegistrationCompleted extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
    super(props, context);
    this.state = {
    showPopup: false,
    modal: false,

    };
    this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        userStore.addChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);
        
        this.checkTimelineStatusInterval = setInterval(this.checkTimelineStatus, 3000);
    };
    
    componentWillUnmount() {
        userStore.removeChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);
    }
    private checkTimelineStatusInterval: any;
    private checkTimelineStatus = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        else{
            clearInterval(this.checkTimelineStatusInterval);
        }
    }
    setTimelineData = (response: any) => {
        console.log('setTimelineDataAfterBooking', response);
        if(response && response.details.status=='completed')
        {
            clearInterval(this.checkTimelineStatusInterval);
            this.props.setTimelineData(response.details);
            
        }
    }
    



    togglePopup() {
    this.setState({
    showPopup: !this.state.showPopup
    });
    }
    private cancel = () => {
    //ModalPopupActionCreator.closeModal()
    }
    toggle() {
    this.setState({
    modal: !this.state.modal
    });

    }

    render() {

    return (

        
        <div className="dealerDetailsContainer">
            <div className="dealerDetailsWraper">
                <div className="dealerTilteWraper">
                    <div className="titleWraper">
                        <h2>Register your car</h2>
                            {/* <span className="status processing">PROCESSING</span> */}
                            <span className="status approved">APPROVED</span>
                    </div>
                        <div className="export">
                            <Button className="exportbtn" outline color="warning">EXPORT TO MOBILE</Button>
                        </div>
                </div>


                    {/* police verification -- licence plate  */}

                    <div className="verification">

                        <div className="contentWraper">
                         <p className="mainTitle">Your license plate</p>
                         <div className="numPlate">
                         {/* {this.props.timelineData.windowdetails.plateno} */}
                            <img src="/public/images/numPlate.png" alt="" />
                         </div>
                        </div>

                        
                        <div className="contentWraper">
                            <p className="mainTitle">Vehicle Registration Reference</p>
                            <span className="subText  ">{this.props.timelineData.windowdetails.vehicleregrefno}</span>
                        </div>
                            {/* page details  */}

                        <div className="detailsPortion">
                            <div className="detailstitleWraper">
                                <span className="detailsMaintitle registation">Registration Details </span> 
                            
                            </div>

                            <div className="detailsContent">

                            
                                <div className="contents">
                                    <p className="contentMain">Registration Start Date</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.regstartdate}</span>
                                </div>
                                <div className="contents">
                                    <p className="contentMain">Registration Expiry Date</p>
                                    <span className="contentSub">{this.props.timelineData.windowdetails.regexpdate}</span>
                                </div>
                                
                            </div>
                        </div>
                        

                    </div>

            </div>


        </div>


        
    )
    }

    }