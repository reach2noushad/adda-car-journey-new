import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';
import RegistrationConfirmation from './registrationconfirmation';
import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import constants from '../../constants/constant';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import userstore from '../../store/users/userstore';

export default class InsuranceDetails extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            qr: '',
            active: false,
            quoteActive: false,
            activePage: 'register_home',
            bookingnumber: userStore.bookingId,
            approveTransactionActive: false,
             registrationDetails: {
            //     "insurancequotedetails": {
            //         "vehicleestimatedvalue": 58700,
            //         "annualquotevalue": 1362,
            //         "startdate": "29-08-2019",
            //         "enddate": "29-08-2020"
            //     },
            //     "cardetails": {
            //         "dealer": "Al Futtaim Motors",
            //         "bookingid": 130,
            //         "carmanufacturer": "Toyota",
            //         "model": "Yaris",
            //         "exshowroonprice": "AED 58700"
            //     },
            //     "personaldetails": {
            //         "firstname": "Aladdin",
            //         "lastname": "G",
            //         "gender": "Male",
            //         "nationality": "United Arab Emirates"
            //     }
            },
        };
    }
    componentWillMount()
    {
        this.getPoliceQr();
        userStore.addChangeListener(actions.GET_POLICE_QR, this.setQrCode);
    }
    componentDidMount() {
        
        
        //after qr success
        loginStore.addChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getQrStatus);
        userStore.addChangeListener(actions.POLICE_CONFIRMATION_DETAILS, this.getRegistrationConfirmationDetails);
        userStore.addChangeListener(actions.CHECK_CONFIRM_REGISTRATION_STATUS, this.getConfirmRegistrationStatus);

        
        userStore.addChangeListener(actions.SAVE_REGISTRATION_DETAILS, this.afterSaveRegistration);
    };

    componentWillUnmount() {
        clearInterval(this.checkPoliceQrStatusInterval);
        clearInterval(this.checkConfirmRegistrationInterval);

        userStore.removeChangeListener(actions.GET_INSURANCE_QR, this.setQrCode);
        userStore.removeChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getQrStatus);
        userStore.removeChangeListener(actions.POLICE_CONFIRMATION_DETAILS, this.getRegistrationConfirmationDetails);
        userStore.removeChangeListener(actions.CHECK_CONFIRM_INSURANCE_STATUS, this.getConfirmRegistrationStatus);

        
        userStore.removeChangeListener(actions.SAVE_REGISTRATION_DETAILS, this.afterSaveRegistration);
    }


    private changePage = (activePage: any) => {
        
        this.setState({
            activePage: activePage
        })

    }
    getPoliceQr = () => {
        let userInfo = storageManager.getValue('userInfo');

        userActionCreator.getPoliceQr(userInfo.useraddress);
    }
    private checkPoliceQrStatusInterval: any;
    private setQrCode = (response: any) => {

        this.setState({
            'qr': response.qr,
        });
        //checking status in every 3sec
        this.checkPoliceQrStatusInterval = setInterval(this.checkPoliceQrStatus, 3000);



    }
    private checkPoliceQrStatus = () => {
        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkTransactionStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    getQrStatus = (response: any) => {
        if (response && response.status) {

            clearInterval(this.checkPoliceQrStatusInterval);
            let userInfo = storageManager.getValue('userInfo');

            userActionCreator.getPoliceConfirmationDetails(userInfo.useraddress);



        }
        else {
            // clearInterval(this.checkInsuranceQrStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getInsuranceConfirmationDetails(userInfo.useraddress);
        }
    }
    getRegistrationConfirmationDetails = (response: any) => {
        if (response && response.result ) {
            this.setState({ registrationDetails: response.result, activePage: 'confirm_details' })

        }
        console.log('insuranceDetails', response);

    }
    registrationConfirmation = () => {
        this.changePage('approve_transaction');
        let userInfo = storageManager.getValue('userInfo');
        let data = {
            useraddress: userInfo.useraddress,
            //useraddress: '0xdfc77789e1e6eaeb884a3b631648539532cb78e2',
            transactiontype: constants.TRANSACTION.APPLY_FOR_VEHICLE_REGISTRATION,
            bookingid: userstore.bookingId
        }
        userActionCreator.confirmRegistration(data);
        this.checkConfirmRegistrationInterval = setInterval(this.checkConfirmRegistrationStatus, 3000);
        this.setState({ approveTransactionActive: true })
    }
    private checkConfirmRegistrationInterval: any;
    private checkConfirmRegistrationStatus = () => {

        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkConfirmRegistrationStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    getConfirmRegistrationStatus = (response: any) => {
        if (response && response.status) {


            clearInterval(this.checkConfirmRegistrationInterval);
            let userInfo = storageManager.getValue('userInfo');
        let today = new Date();

        var day = today.getDate();
        var month_index = today.getMonth()+1;
        var year = today.getFullYear();

        let modifiedMonthIndex;

        if (month_index > 9) {

            modifiedMonthIndex = month_index;

        } else {

            modifiedMonthIndex = "0" + month_index;

        }

        let date = "" + day + "-" + modifiedMonthIndex + "-" + year;

        let registartionDetails = {
            bookingno: this.state.bookingnumber,
            policereferencenumber: 'PR' + this.state.bookingnumber,
            policeapplicationdate: date,
            smartpassidsubmitted: true,
            carquotesubmitted: true,
            carloansubmitted: true,
            drivinglicensesubmitted: true,
            insurancereceiptsubmitted:true
        }
        userActionCreator.saveRegistrationDetails(registartionDetails);



        }
        else {
            // clearInterval(this.checkBankApproveTransactionStatusInterval);

            // let userInfo = storageManager.getValue('userInfo');


            // let loanDetails = {
            //     bookingno: this.state.bookingnumber,
            //     loanreferencenumber: 'LR' + this.state.bookingnumber,
            //     loanprovidingbank: "HSBC",
            //     loanapplieddate: this.getTodaysDate,
            //     fname: userInfo.userfulldetails['First Name'],
            //     lname: userInfo.userfulldetails['Second Name'],
            //     statementbname: this.state.bankStatement.bankstatement.bankname,
            //     statementacctno: this.state.bankStatement.bankstatement.accountnumber,
            //     statementcreditscore: this.state.bankStatement.bankstatement.creditscore,
            //     smartpassidsubmitted: true,
            //     carquotesubmitted: true,
            //     bankstatementsubmitted: true
            // }
            // userActionCreator.saveLoanDetails(loanDetails);
            // clearInterval(this.checkTransactionStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getMobileResponse(userInfo.useraddress);
        }
    }
   
    
    afterSaveRegistration=(response:any)=>{
        if (response && response.message=='success') {
            
            this.setState({
                activePage: 'transaction_success'
            })
        }
    }
    private closeModal = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        ModalPopupActionCreator.closeModal()
    }
    render() {

        return (
            <div>
                {this.state.activePage == 'register_home' && <div className="Navimages policeLanding">
                    <img src="/public/images/Police Flow/Group 250.png" alt="" />
                    <a  className="applyLoanNav " >
                        <div className="policeLandingNav">
                        <div className="QRcode">
                         <img src={this.state.qr} alt="" />
                 </div>
                            </div></a>
                </div>}
                
                {/* {this.state.activePage == 'confirm_details' && <div className="Navimages Insurance_quote">
                    <img src="/public/images/Insurance Flow/Group 249.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'models')} className="applyLoanNav cp"><div className="insureQuoteNav"></div>
                        </a>
                </div>} */}
                {this.state.activePage == 'confirm_details' && <RegistrationConfirmation changePage={this.changePage} cancel={this.closeModal} registrationDetails={this.state.registrationDetails} registrationConfirmation={this.registrationConfirmation} />}
                
                {this.state.activePage == 'approve_transaction' && <div className="Navimages mobaprovalSection">
                    <img src="/public/images/Police Flow/Group 252.png" alt="" />
                    <div className="mobaprovalContainer">
                        <div className="mobileContainer">

                            <div className="mobileWraper ">
                                <a >
                                    <div className={this.state.approveTransactionActive ? "mobile active" : "mobile"}></div>
                                </a>

                            </div>
                            <div className="mobileText">
                                <span className="shade">Tap </span>
                                <span className="bright">Approve Transaction</span>
                                <span className="shade"> in your mobile phone to continue...</span>



                            </div>
                        </div>

                    </div>
                </div>
                }
                   
                {//transaction_success
                    this.state.activePage == 'transaction_success' && <div className="Navimages mobaprovalSection">
                        <img src="/public/images/Police Flow/Group 253.png" alt="" />
                        <div className="mobaprovalContainer">
                            <div className="mobileContainer">

                                <div onClick={this.closeModal} className="mobileWraper verified cp">
                                    <a >
                                        <div className="mobile"></div>
                                    </a>

                                </div>
                                <div className="mobileText">


                                    <p className="verifiedText">Your request is currently under process.</p>
                                    <p className="verifiedText">Your will be notified once your registration is approved.</p>
                                    <span className="verifiedIDno">Your Vehicle Registration Reference Number: {'PR' +this.state.bookingnumber}</span>


                                </div>
                            </div>

                        </div>
                    </div>
                }
            </div>
        )
    }




}