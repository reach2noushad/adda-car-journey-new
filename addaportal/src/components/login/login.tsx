import * as React from 'react';
import { Router,Redirect, Route, Switch, BrowserRouter,HashRouter } from 'react-router-dom';

import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import actions from '../../actions/base/actiontypes';
import loginActionCreator from '../../actions/login/loginactioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import userActionCreator from '../../actions/user/useractioncreator';
import './styles/login.css';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import history from '../../history';

export default class Login extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {

            qr: '',
            loginsessionid:12
        }
    }
    refs: any;

    componentDidMount() {
        loginStore.addChangeListener(actions.CHECK_STATUS, this.getLoginStatus);
        loginStore.addChangeListener(actions.QRCODE, this.setQrCode);
        // let _that=this;
        // $.get("http://192.168.11.79:8080/login/qrcode?loginsessionid=12", function(data:any, status:any){alert(data.qr)
        // _that.setState({
        //         'qr':data.qr,
        //     });

        //   });
        //http://192.168.11.79:8080/login/qrcode?loginsessionid=12
        loginActionCreator.getQrCode(this.state.loginsessionid);

    };

    componentWillUnmount() {//2,3sec
        loginStore.removeChangeListener(actions.QRCODE, this.setQrCode);
        loginStore.removeChangeListener(actions.CHECK_STATUS, this.getLoginStatus);

    }

    private checkLoginStatusInterval:any;
    private setQrCode = (response: any) => {
        
        this.setState({
            'qr': response.qr,
        });

        //checking status in every 3sec
        this.checkLoginStatusInterval = setInterval(this.checkLoginStatus, 3000);

        
    }
    private checkLoginStatus = () => {
        loginActionCreator.checkStatus(this.state.loginsessionid);
        
    }
    private getLoginStatus = (response: any) => {
        if(response.status)
        {
            //if logged in
            console.log('response33:::',response)
            //save response to store and redirect to home page
            //response.userfulldetails=JSON.parse(response.userfulldetails)
            loginStore.setloggedInUser(response);
            clearInterval(this.checkLoginStatusInterval);
            RouterBase.changeRoute(constants.ROUTES.USERHOME);
            this.props.history.push(constants.ROUTES.USERHOME)
        }
        else{
          //login failed
           // for testing, remove this code
            // let userInfo = storageManager.getValue('userInfo');
            // console.log('userInfo4444',userInfo);
            // let response=
            // {
            //     "status": true,
            //     "useraddress": "0x57296c0d37590e8514756da5adb273d64e2bdb21",//naudhads
            //     //"useraddress": "0xdfc77789e1e6eaeb884a3b631648539532cb78e2",//derins
                
            //     "userfulldetails": {
            //         "Gender": "Male",
            //         "Last Seen": "Mon Sep 23 2019 13:59:39 GMT+0530 (IST)",
            //         "First Name": "Aladdin",
            //         "Nationality": "United Arab Emirates",
            //         "Second Name": "G"
            //     }
            // }
            // loginStore.setloggedInUser(response);
            // clearInterval(this.checkLoginStatusInterval);
            // RouterBase.changeRoute(constants.ROUTES.USERHOME);
            // this.props.history.push(constants.ROUTES.USERHOME)  
        }
    }
    componentWillMount() {
        let userInfo = storageManager.getValue('userInfo');
        if (userInfo && userInfo.useraddress) {
            userActionCreator.userSignOut();
        }

        window.onbeforeunload = null;
    }
    render() {
        return (
            
                
            
            <div className="loginPageContainer">
                <div className="BrandContainer">


            <div className="logoWraper">
                        <img src="/public/images/ADDA.svg" alt="" />

            </div>


                </div>

                <div className="qrContainer">
                <div className="loginContentWraper">
                        <h2 className ="main-title">Welcome back!</h2>
                        <p className='sub-title'>Login back to your account</p>
                        <div className="qrcodeImageWraper">
                             {/* <img src="/public/images/qrcode.svg" alt="" />  */}
                             <img style={{height:'271px',width:'266px'}} src={this.state.qr} alt="" /> 
                            {/* <img style={{height:'208px',width:'204px'}} src="data:image/png;charset=utf-8;base64, iVBORw0KGgoAAAANSUhEUgAAAdEAAAHRCAAAAAAQqNxHAAALiUlEQVR42u3b0W4dNxIE0Pv/P508BjCsUVU3ObKDwydtbF3N8FBAuYv7+cf6f62PLSBqEbWIWkQtokQtohZRi6hFlKhF1CJqEbWIErWIWkStd0Q/36///t4v3/blR/3uD755tubjf/dUv3wVvMeXf/r8vcMfvvm2L/ePKFGiRIkSJXpDNNjoDWFFHbzI8/cGD/ksFXhvXiH9lOBPiRIlSpQoUaLXRPtkGHxK8KhpvqyC5zNDcPbOfECaXFMFokSJEiVKlOifIZokr3CPq3lgekyqM/D8eV/+z2qWOJx6EiVKlChRokT/BtFh0xeMAqvou25jP4uVjjnTuE6UKFGiRIkS/XNFq0hWdaHDivJZIGhyn78aVrfBuU3Rf7bxJkqUKFGiRImOst9wx3xVflVFfaJEiRL1FVFf3RDtVxU307CX5svNHa3gjKZvubkW1lfGX1MQJUqUKFGiRM+K9vu0mZ6lk8Hq9lfgcxqpeoVnzOoOGVGiRIkSJUr0rmiQvNJWNAi36QkZtpjBHlfl7LoFTjrOJp8TJUqUKFGiRO+K9pGsulP1zFrNHNPC9vmw9Tubpt70QPcHhihRokSJEiX6lmiV5Ppk2O9TdabWXMNetj/G/a9Pss9EiRIlSpQo0bOifXJNN/B5BpdWlGk+7w9MdVCHt8TSJ0g7U6JEiRIlSpToXdGgKe1fcz1VTOd8n3BVc7m0uu1Zq90lSpQoUaJEib4vmsbSoMFLZ3/pwaqiYBWMq2Pc17lpXk29iRIlSpQoUaJviaZ1aZCOh/u+GYMNx2r9m1cX53rH4e8BUaJEiRIlSvSwaJDzqsFUSliFzN57c2MtlRrOJjcJnChRokSJEiX6k6L9pCx96PSVgnFZMLlMS9L01PQzwnRK2f1eESVKlChRokTPigbDtA1/anbw/PRd4/rvVem9OklEiRIlSpQo0fdFq+cIvPuL61WLOZxXVvPFajYZJPr+D4IoTZQoUaJEiRK9Kxq89aZC7T/q8/0KHndTjQala/Azei6iRIkSJUqU6A+J9jXjpgwMPqqaSFYJvArkaWFbbWL1qoObY0SJEiVKlCjRuWgaaYOsVk3FqkycRu602Uzzfn/20v+Wbuwg6xIlSpQoUaJEL4gGt5z6mNvP26p8nta5/YQzDa0pXLrjJ7MuUaJEiRIlSnQ0BexLvuHoLr2TFkgFnelz5kzbzvTCWX83rNoNokSJEiVKlOg10eGArZ929Zs6zLpVNu2/o5rupXUuUaJEiRIlSvTHRfvKM3jrapSVxtIqbQcJPC0hq4D/vBFVUidKlChRokSJvi9a9YBpGhv+vWH6fD4Xm/52Pcmrwu3gnBElSpQoUaJEz4p+6lXly2BP+veqgKszkP7IoEEevmV3oIkSJUqUKFGiZ0WDxNdVdnXiCwSGOTmNm2msHw4F+9NAlChRokSJEv0h0WqIt5ldpUViUC5uroVVT5AWu8N5aj8sJUqUKFGiRIneFU33aV2hppG2f7SqehxO8tLQP0zgQTAmSpQoUaJEib4guulHq3nWpjSsppTDMNpf3kqvlPWbHbwWUaJEiRIlSvSGaBD7DibSqkNMdyJ9tPRmVnU40olp1U3vG2+iRIkSJUqU6LGbY8F2bAZs/eAxHc6tM3s6zgsy+0F0okSJEiVKlOhbov1WVp9STfyGU7bhRbJNwt30vJshKFGiRIkSJUr0BdEgqg6/CqZ7VeYMWIPvTZ802IjhieszO1GiRIkSJUr0BdF1VtsEzzRFB+8fHMVqQJnWoNW/FfpflW+0iBIlSpQoUaIXRNOiM/h71fSs6iTTvNpfEAs+Pm4s653sB4pEiRIlSpQo0Wui6xKyBx7eEjszWxtm2H5kuB4t3pnrEiVKlChRokSzm2PBYG/digZIafmZBvdgUBj8oL5B3lSy8bySKFGiRIkSJXpB9OBVsecPrW56bWg2t8SG0TetS4P2ND68RIkSJUqUKNGzomnq7eNwP7qrbnoFYTndyoowvWAXlKnVthMlSpQoUaJE3xKtAlbfSaYFa5oW111oVb8GP6O/SLZ+faJEiRIlSpToDdFg1FblsuQ5wjSb7nZab6bTx/QHVcr9L02Sk4kSJUqUKFGit0SfP7Wy7ZNw2hxWK5gWbna2KkSrLUnPAFGiRIkSJUr0sGg6ajsYLdPcuMmXQWitDnRKOJwlToeMRIkSJUqUKNELotU4L42RaXINNrAfDw4dK7003AZPUM1iiRIlSpQoUaJ3RYMIGkzZ0lq171HjO+bf/9zndNwXoulMdHNtbToFJEqUKFGiRInO+9G0mTtdiFb/rdrKij/4g34UmP4y3Gm8iRIlSpQoUaKjrBs8QjWNC/JgFez6617ppDEd4qVRNUj06XW5ZJpJlChRokSJEj0ruo65ac6rys/qSKylqqeq/t8EaQ06vPNPlChRokSJEj0smibc4FpTVX5WP3x4sNIJ5/Clh/fU0vti+8abKFGiRIkSJToXTdu/YcNYXeMaTs/S+JreF1tn7KpHPZ51iRIlSpQoUaKjKeDm1dN3rUrIir/P3X1wr+6k9RPJ6ucSJUqUKFGiRO+K3rv5VJWQZ/L08zW44UcFY7/+Xwj91hElSpQoUaJEXxDty8oUuCowN3eq+slb9SzpNLOfKlbfS5QoUaJEiRK9K1oN+54TZLUxwUAsGNj1ByE9qOnpD7ak+ifGPusSJUqUKFGiRJeiqW1QpqYlZFVR9lLDaWaa/DedafV506xLlChRokSJEp1PAfud7XvANEZWReyZSjbN9lX8T6nTcpYoUaJEiRIl+qpotTHVpvaZuGpZg2D8PKurmuHh36uCdnwaiBIlSpQoUaJnRav0mX5blfjSt0kvsKXHrgrf/dW44Yd2n0eUKFGiRIkSvSWahsz1K/URtHqgzWMc3I3gZG5ekChRokSJEiV6Q7S6cZWGzE1ZGbSJwfNthm7VyDDdsGA7B8GdKFGiRIkSJXpW9GDd15eQafDcNKDpPq2PRBXhh7NOokSJEiVKlOhd0XSfNqOxdHeGhyjY3n4Gl35VvXT6g+IymihRokSJEiX6pujlVBlMu1LRaowYHImgbz19P67aF6JEiRIlSpToC6JppH3Ob9WOBehdafj9xlR6m7dMx3lprUqUKFGiRIkSvStabWo/C3t+tmFafPauWuDgPaoznz5B/21EiRIlSpQo0bdEg4+pJoPBxqQ729OkP6hK0el0tMrx1W4QJUqUKFGiRF8QTQdiQarsny2Y6QWhsLpN9ryV1e2v9PSnheig7CVKlChRokSJnhVN53fV5qfvELxS2tqmqbI/U1V1GzhWLTBRokSJEiVK9FXRKt2luz2UGvaUVUYcDgXToziMr+leESVKlChRokRfEK26wWEO3QhUY79h/TpsWdMp5fAYT/tRokSJEiVKlOgJ0Srr9mPEambWd6vVy6SDwiqMVkVsejlvlXWJEiVKlChRoqN+dPj+fcgMwl4/VuvvwgX96PCwVUn9OfASJUqUKFGiRF8VfX6sdByVjgc3oXDYgG7CaH9V7Mxbptf0iBIlSpQoUaKHRSuptAwM0l16Q+o0SDCv7Dvd4NdiWKYSJUqUKFGiRF8VHU7y+ly26R+rXQxq2jRF91JpW9wPX4kSJUqUKFGib4luitNgSBYE3oPZuT8XaY/ax/W+90xPA1GiRIkSJUr0rmjVRAahtbqUlabtapI37EyDH1QdwCpyd/90IEqUKFGiRIn+OaKbM5AWhEE6Pngzq5rGpd/bJ/9DU0CiRIkSJUqU6C3RvhVNG8a+mBzWpdVIrk/b6Ryyj/pEiRIlSpQo0fdFq9lV8DBV8ZfG12ritw7Q/Skcnsd+O4kSJUqUKFGi10SrdjLdu+EtsWp4WCX1Kn1u4nA160wPNFGiRIkSJUr0BVHrL15EiVpELaIWUYsoUYuoRdQiahElahG1iFpELaJELaIWUev8+hebUs3T7Kn//gAAAABJRU5ErkJggg==" alt="" /> */}
                        </div>
                        <p className='qr-sub-title'>Scan the QR code to login with the <b>Smart</b>Pass ID</p>
                    </div>  


                </div>


            </div>







        )
    }

}


