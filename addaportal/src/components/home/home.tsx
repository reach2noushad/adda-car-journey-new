import * as React from 'react';
import Header from '../includes/header';
import './home.css';
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import constants from '../../constants/constant';
import history from '../../history';
import RouterBase from '../utility/routerbase';
import MyJourney from './myjourney';
import userstore from '../../store/users/userstore';
export default class Home extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);



        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',
            active: false,
            showArrow: false,
        };


    }

    componentDidMount() {
        userstore.setBookingId(false)
    };

    componentWillUnmount() {

    }


    toggle(tab: any) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    private driveACar = () => {

        // if(this.state.active)
        // {
        //     RouterBase.changeRoute(constants.ROUTES.CAR);
        //     this.props.history.push(constants.ROUTES.CAR)
        // }
        // else{
        //     this.setState({
        //         active: true
        //     }); 
        // }
        RouterBase.changeRoute(constants.ROUTES.CAR);
        this.props.history.push(constants.ROUTES.CAR)
    }
    private toggleHover = () => {
        this.setState({
            active: !this.state.active
        });
    }
    render() {

        return (

            <div className="homeDataContainer">

                <div className="tabContainer">
                    <Nav tabs>
                        <NavItem>
                            <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => {
                                this.toggle('1');
                            }}
                            >
                                All Journeys
                        </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => {
                                this.toggle('2');
                            }}
                            >
                                My Journeys
                        </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <div className="cardLayout cp " onMouseEnter={this.toggleHover}
                                onMouseLeave={this.toggleHover} onClick={this.driveACar
                                }>
                                <div className="cardContainer">
                                    <div className={this.state.active ? "card active" : "card"}>
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                {/* <img src="/public/images/car.svg" alt="" /> */}
                                                <i className="icon-car icon"></i>
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Drive a Car</h3>
                                                {this.state.active && <img className="arrow" src="/public/images/right-arrow-forward.svg" alt="" />}
                                                <div className="siteNav">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="cardLayout">
                                <div className="cardContainer">
                                    <div className="card  ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                {/* <img src="/public/images/home.svg" alt="" /> */}
                                                <i className="icon-home icon"></i>
                                                   
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Buy a Home</h3>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="cardLayout" >
                                <div className="cardContainer">
                                    <div className="card">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                {/* <img src="/public/images/medical.svg" alt="" /> */}
                                                <i className="icon-medical icon"></i>
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Medical Treatment</h3>
                                                <img className="arrow" src="/public/images/right-arrow-forward.svg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </TabPane>
                        <TabPane tabId="2">
                        { <MyJourney driveACar={this.driveACar}/>}
                            {/* {this.state.activeTab === '2' && <MyJourney driveACar={this.driveACar}/>} */}
                            {/* <div className="cardLayout bookedcarWraper">
                                <div className="cardContainer ">
                                    <div className="card active">
                                        <div className="cardContent">
                                            <div className="image-wraper carLoanSelection ">
                                                <img src="/public/images/car.svg" alt="" />
                                                <Button outline color="secondary" className="homeLoanbtn">Awaiting Bank Loan</Button>
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle bookedcar">Drive a Car</h3>
                                                <p className="bookingId">Booking ID: C23R334</p>
                                               

                                                <div className="blockchainNavWraper">
                                                    <a href="">
                                                        <span className="blockNavText">View Blockchain Timeline
                                                        <i className="fa fa-angle-right"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="ContinueBtnWraper">
                                            <a href="">
                                                <span>Continue
                                                <i className="fa fa-angle-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div> */}


                            {/* loan applying portion    */}

                            {/* <div className="applyCardContainer">
                                <div className="cardLayout">
                                    <div className="cardContainer">
                                        <div className="card active ">
                                            <div className="cardContent">
                                                <div className="image-wraper">
                                                    <img src="/public/images/al_futtaim.png" alt="" />
                                                </div>
                                                <div className="content-wraper">
                                                    <h3 className="cardContentTitle applytitle">Buy a Home</h3>
                                                    
                                                    <div className="siteNav active">
                                                        <a href="">Visit Website</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        </TabPane>
                    </TabContent>
                </div>
            </div>
            // </div>





        )
    }

}