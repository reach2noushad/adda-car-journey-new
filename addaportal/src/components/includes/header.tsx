import * as React from 'react';
import './header.css';
import storageManager from '../../store/base/sessionstoremanager';
import { withRouter } from 'react-router-dom';
import constants from '../../constants/constant';
import RouterBase from '../utility/routerbase';
import { Collapse, Button, CardBody, Card } from 'reactstrap';




class Header extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.toggle = this.toggle.bind(this);
        this.state = {
            collapse: false,
            blockchainData: []
        };
    
    }

    componentDidMount() {
        //console.log('userDetails111',userDetails);
    };

    componentWillUnmount() {

    }
    logout=()=>{
        RouterBase.changeRoute(constants.ROUTES.LOGIN);
        this.props.history.push(constants.ROUTES.LOGIN);   
    }
    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    render() {
        let userInfo=storageManager.getValue('userInfo');
        return (
            <div className="navbar">
                <div className="logoWraper">
                    <div className="logoImageWraper">
                        <img src="/public/images/ADDA.svg" alt="" />
                    </div>
                </div>

                <div className="loginDetailsContainer">
                    <div className="avatar">
                        <img src="/public/images/avatar.svg" alt="" />
                    </div>
                    <span>Logged in as:</span>
                    <div className="toggleBtn" color="primary" onClick={this.toggle} style={{}}>
                        <p>{(userInfo && userInfo.status) ? `${userInfo.userfulldetails['First Name']} ${userInfo.userfulldetails['Second Name']}` : ''}</p>
                  
                    </div>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="cp" onClick={this.logout}>
                                <p  >Logout</p>
                            </CardBody>
                        </Card>
                    </Collapse>
                  

           
                </div>
            </div>


        )
    }

}
export default withRouter(Header);

