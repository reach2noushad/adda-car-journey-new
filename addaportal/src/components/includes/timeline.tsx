import * as React from 'react';
import { withRouter } from 'react-router-dom';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
    'reactstrap';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import userStore from '../../store/users/userstore';
import userActionCreator from '../../actions/user/useractioncreator';
import actions from '../../actions/base/actiontypes';

//var Modal = require('react-bootstrap-modal')

class Timeline extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            showPopup: false,
            modal: false,
            active: false,
            timelineData: []
        };


    }
    componentWillMount() {
        //alert(userStore.bookingId);
        userStore.addChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);

        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
    }
    componentDidMount() {
        
    }

    componentWillUnmount() {
        userStore.removeChangeListener(actions.GET_BOOKING_STATUS, this.setTimelineData);
    }
    setTimelineData = (response: any) => {
        console.log('setTimelineDataAfterBooking', response);
        if(response)
        {
            this.props.setTimelineData(response.details);
            this.setState({
                timelineData:response.details
            });
            
        }
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
    private cancel = () => {
        //ModalPopupActionCreator.closeModal()
    }

    private backToPage = () => {
        let routesArr = RouterBase.getRoutesArray();

        if (routesArr.length > 1) {
            this.props.history.push(routesArr[routesArr.length - 2]);
            RouterBase.changeRoute(routesArr[routesArr.length - 2]);

        }
        else {

            RouterBase.changeRoute(constants.ROUTES.USERHOME);
            this.props.history.push(constants.ROUTES.USERHOME);
        }
    }

    render() {
        
        return (

            <div className="steperContainer">
                <div className="steperDataContainer">
                    <Button onClick={this.backToPage} color="link" className="stepetBackbtn">Back</Button>
                    <h3 className="steperDatatilte">Buy a New Car</h3>

                    <div className="steperWraper">

                        <div className="">

                            <div className={(this.state.timelineData.length<=0)?"step active":(this.state.timelineData && this.state.timelineData.carquotationreceiveddate)?"step completed":"step"}>
                                <div className="v-stepper">
                                    <div className="circle"><span>1</span></div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <div className="stepDetailsWraper">
                                        <div className="details">
                                            <span className="stepPosition">Step 1</span>
                                            <p className="stepLabel">Buy a Car</p>
                                        </div>
                                        <div className="chkmark"></div>
                                    </div>
                                    
                                    <div  className={this.state.timelineData && this.state.timelineData.carquotationreceiveddate?"subText active":"subText"}>
                                        <span>Car Quotation Received</span>
                                        {/* <p>13-09-2019</p> */}
                                        <p>{this.state.timelineData &&
                                            this.state.timelineData.carquotationreceiveddate
                                        }</p>
                                    </div>

                                </div>


                            </div>


                            <div className={(this.state.timelineData && this.state.timelineData.loanapproveddate)?"step completed":(this.state.timelineData && this.state.timelineData.status=='carquotationsaved')?"step active":"step"}>
                                <div className="v-stepper">
                                    <div className="circle">
                                        <span>2</span>
                                    </div>
                                    <div className="line"></div>
                                </div>

                                <div className="content">
                                    <div className="stepDetailsWraper">
                                        <div className="details">
                                            <span className="stepPosition">Step 2</span>
                                            <p className="stepLabel">Get Financing</p>
                                        </div>
                                        <div className="chkmark"></div>
                                    </div>
                                    <div className={this.state.timelineData &&this.state.timelineData.loanapproveddate?"subText active":"subText"}>
                                        <span>Bank Loan Approved</span>
                                        <p>{this.state.timelineData && this.state.timelineData.loanapproveddate}</p>
                                    </div>
                                    <div className={this.state.timelineData &&this.state.timelineData.loanapplieddate?"subText active":"subText"}>
                                        <span>Applied for Bank Loan</span>
                                        <p>{this.state.timelineData && this.state.timelineData.loanapplieddate}</p>
                                    </div>
                                    {/* <div className="subText active">
                                        <span>Car Quotation Received</span>
                                        <p>13-09-2019</p>
                                    </div>
                                    <div className="subText active">
                                        <span>Car Quotation Received</span>
                                        <p>13-09-2019</p>
                                    </div> */}
                                </div>
                            </div>


                            <div className={(this.state.timelineData && this.state.timelineData.insurancereceiveddate)?"step completed":(this.state.timelineData && this.state.timelineData.status=='loanapproved'?"step active":"step")}>
                                <div className="v-stepper">
                                    <div className="circle">3</div>
                                    <div className="line"></div>
                                </div>
                                <div className="content">
                                    <div className="stepDetailsWraper">
                                        <div className="details">
                                            <span className="stepPosition">Step 3</span>
                                            <p className="stepLabel">Insure your Car</p>
                                        </div>
                                        <div className="chkmark"></div>
                                    </div>
                                    <div className={this.state.timelineData &&this.state.timelineData.insurancereceiveddate?"subText active":"subText"}>
                                        <span>Car Insurance Received</span>
                                        <p>{this.state.timelineData &&this.state.timelineData.insurancereceiveddate}</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div className={(this.state.timelineData && this.state.timelineData.status=='completed')?"step completed":this.state.timelineData && (this.state.timelineData.status=='insurancereceived'||this.state.timelineData.status=='vehicleregistrationapplied')?"step active":"step"}>
                                <div className="v-stepper">
                                    <div className="circle">4</div>
                                    <div className="line active"></div>
                                </div>
                                <div className="content">
                                    <div className="stepDetailsWraper">
                                        <div className="details">
                                            <span className="stepPosition">Step 4</span>
                                            <p className="stepLabel">Register your Car</p>
                                        </div>
                                        <div className="chkmark"></div>
                                    </div>
                                    <div className={this.state.timelineData && this.state.timelineData.windowdetails && this.state.timelineData.windowdetails.regstartdate?"subText active":"subText"}>
                                        <span>Registration Approved</span>
                                        <p>{this.state.timelineData && this.state.timelineData.windowdetails && this.state.timelineData.windowdetails.regstartdate?this.state.timelineData.windowdetails.regstartdate:''}</p>
                                    </div>
                                    <div className={this.state.timelineData &&this.state.timelineData.registrationapplieddate?"subText active":"subText"}>
                                        <span>Applied for Registration</span>
                                        <p>{this.state.timelineData &&this.state.timelineData.registrationapplieddate?this.state.timelineData.registrationapplieddate:''}</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }

}
export default withRouter(Timeline);