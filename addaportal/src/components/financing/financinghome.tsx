import * as React from 'react';
import Header from '../includes/header';
//import CarDetails from './cardetails';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
    'reactstrap';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import userStore from '../../store/users/userstore';
import Timeline from '../../components/includes/timeline';
import BankDetails from '../../components/financing/bankdetails';
//var Modal = require('react-bootstrap-modal')

export default class FinancingHome extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            showPopup: false,
            modal: false,
            active: false,
            timelineData: [],
            activeStage: '',

        };
        this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        //alert(userStore.bookingId);
    };

    componentWillUnmount() {

    }
    private timelineData: any = [];
    setTimelineData = (timelineData: any) => {

        this.timelineData = timelineData
        this.setState({ activeStage: timelineData.status });
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
    private cancel = () => {
        //ModalPopupActionCreator.closeModal()
    }
    toggle() {
        ModalPopupActionCreator.openModal(<BankDetails />, true, 'md')
        

    }

    private toggleHover = () => {
        this.setState({
            active: !this.state.active
        });
    }
    render() {

        return (

            
                     <div className="dealerDetailsContainer">
                        <div className="dealerDetailsWraper">
                            <div className="dealerTilteWraper">
                                <div className="titleWraper">
                                    <h2>Select a Bank</h2>
                                </div>
                                <div className="searchWraper">
                                    <FormGroup>
                                        <Input type="search" className="search" id="Search" placeholder="Search banks" />
                                    </FormGroup>
                                </div>

                            </div>
                            <div className="applyCardContainer">
                                <div className="cardLayout cp" onMouseEnter={this.toggleHover}
                                    onMouseLeave={this.toggleHover} onClick={this.toggle}>
                                    <div className="cardContainer">
                                        <div className={this.state.active ? "card active" : "card"}>
                                            <div className="cardContent">
                                                <div className="image-wraper">
                                                    <img src="/public/images/hsbc.png" alt="" />
                                                </div>
                                                <div className="content-wraper">
                                                    <h3 className="cardContentTitle applytitle">HSBC Bank</h3>

                                                    {this.state.active && <div className="siteNav active">
                                                        <a >Visit Website</a>
                                                    </div>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="cardLayout">
                                    <div className="cardContainer">
                                        <div className="card  ">
                                            <div className="cardContent">
                                                <div className="image-wraper">
                                                    <img src="/public/images/standard.png" alt="" />
                                                </div>
                                                <div className="content-wraper">
                                                    <h3 className="cardContentTitle applytitle">Standard Chartered Bank</h3>

                                                    <div className="siteNav ">
                                                        <a href="">Visit Website</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="cardLayout">
                                    <div className="cardContainer">
                                        <div className="card  ">
                                            <div className="cardContent">
                                                <div className="image-wraper">
                                                    <img src="/public/images/citi.png" alt="" />
                                                </div>
                                                <div className="content-wraper">
                                                    <h3 className="cardContentTitle applytitle">Citi Bank</h3>

                                                    <div className="siteNav ">
                                                        <a href="">Visit Website</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                
        )
    }

}