import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';

//import './car.css';
import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import constants from '../../constants/constant';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import loginActionCreator from '../../actions/login/loginactioncreator';

import BankingConfirmation from './bankingconfirmation';
import userstore from '../../store/users/userstore';
export default class BankDetails extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {

            active: false,
            quoteActive: false,
            activePage: 'applyloan',
            bookingnumber: userStore.bookingId,
            approveTransactionActive: false,
            qr: '',
            bankStatement: {
                // "smartpassverified": true,
                // "cardetails": {
                //     "dealer": "Al Futtaim Motors",
                //     "bookingid": 129,
                //     "carmanufacturer": "Toyota",
                //     "model": "Yaris",
                //     "exshowroonprice": "AED 58700"
                // },
                // "bankstatement": {
                //     "bankname": "First Abu Dhabi Bank",
                //     "accountnumber": "5242431234567890",
                //     "creditscore": "720",
                //     "acccountholdername": "Aladdin G"
                // }

            },
        };
    }

    componentDidMount() {

        userStore.addChangeListener(actions.SEND_VERIFICATION_REQUEST, this.setQrCode);
        loginStore.addChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getTransactionStatus);
        userStore.addChangeListener(actions.GET_MOBILE_RESPONSE, this.getMobileResponse);

        userStore.addChangeListener(actions.CHECK_APPROVE_TRANSACTION_STATUS, this.getApproveTransactionStatus);
        userStore.addChangeListener(actions.SAVE_LOAN_DETAILS, this.changePage.bind(this, 'transaction_success'));
    };

    componentWillUnmount() {
        clearInterval(this.checkBankApproveTransactionStatusInterval);
        clearInterval(this.checkBankTransactionStatusInterval);
        userStore.removeChangeListener(actions.SEND_VERIFICATION_REQUEST, this.setQrCode);
        loginStore.removeChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getTransactionStatus);
        userStore.removeChangeListener(actions.GET_MOBILE_RESPONSE, this.getMobileResponse);

        userStore.removeChangeListener(actions.CHECK_APPROVE_TRANSACTION_STATUS, this.getApproveTransactionStatus);
        userStore.removeChangeListener(actions.SAVE_LOAN_DETAILS, this.changePage.bind(this, 'transaction_success'));
    }

    getTodaysDate = () => {

        let today = new Date();

        var day = today.getDate();
        var month_index = today.getMonth()+1;
        var year = today.getFullYear();

        let modifiedMonthIndex;

        if (month_index > 9) {

            modifiedMonthIndex = month_index;

        } else {

            modifiedMonthIndex = "0" + month_index;

        }

        return "" + day + "-" + modifiedMonthIndex + "-" + year;
    }
    private setQrCode = (response: any) => {

        this.setState({
            'qr': response.qr,
        });
        //checking status in every 3sec
        this.checkBankTransactionStatusInterval = setInterval(this.checkBankTransactionStatus, 3000);



    }

    private checkBankTransactionStatusInterval: any;

    private checkBankTransactionStatus = () => {
        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkTransactionStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    private checkBankApproveTransactionStatus = () => {

        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkApproveTransactionStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    sendVerificationRequest = () => {
        let userInfo = storageManager.getValue('userInfo');

        userActionCreator.sendVerificationRequest(userInfo.useraddress);
    }
    getMobileResponse = (response: any) => {
        if (response && response.result && response.result.smartpassverified) {
            this.setState({ bankStatement: response.result, activePage: 'confirm_details' })

        }
        console.log('getMobileResponse', response);

    }
    getApproveTransactionStatus = (response: any) => {
        if (response && response.status) {


            clearInterval(this.checkBankApproveTransactionStatusInterval);

            let userInfo = storageManager.getValue('userInfo');
            let today = new Date();

            var day = today.getDate();
            var month_index = today.getMonth()+1;
            var year = today.getFullYear();

            let modifiedMonthIndex;

            if (month_index > 9) {

                modifiedMonthIndex = month_index;

            } else {

                modifiedMonthIndex = "0" + month_index;

            }

            let date = "" + day + "-" + modifiedMonthIndex + "-" + year;

            let loanDetails = {
                bookingno: this.state.bookingnumber,
                loanreferencenumber: 'LR' + this.state.bookingnumber,
                loanprovidingbank: constants.VENDOR.BANK,
                loanapplieddate: date,
                fname: userInfo.userfulldetails['First Name'],
                lname: userInfo.userfulldetails['Second Name'],
                statementbname: this.state.bankStatement.bankstatement.bankname,
                statementacctno: this.state.bankStatement.bankstatement.accountnumber,
                statementcreditscore: this.state.bankStatement.bankstatement.creditscore,
                smartpassidsubmitted: true,
                carquotesubmitted: true,
                bankstatementsubmitted: true
            }
            userActionCreator.saveLoanDetails(loanDetails);



        }
        else {
            // clearInterval(this.checkBankApproveTransactionStatusInterval);

            // let userInfo = storageManager.getValue('userInfo');


            // let loanDetails = {
            //     bookingno: this.state.bookingnumber,
            //     loanreferencenumber: 'LR' + this.state.bookingnumber,
            //     loanprovidingbank: "HSBC",
            //     loanapplieddate: this.getTodaysDate,
            //     fname: userInfo.userfulldetails['First Name'],
            //     lname: userInfo.userfulldetails['Second Name'],
            //     statementbname: this.state.bankStatement.bankstatement.bankname,
            //     statementacctno: this.state.bankStatement.bankstatement.accountnumber,
            //     statementcreditscore: this.state.bankStatement.bankstatement.creditscore,
            //     smartpassidsubmitted: true,
            //     carquotesubmitted: true,
            //     bankstatementsubmitted: true
            // }
            // userActionCreator.saveLoanDetails(loanDetails);
            // clearInterval(this.checkTransactionStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getMobileResponse(userInfo.useraddress);
        }
    }
    getTransactionStatus = (response: any) => {
        if (response && response.status) {

            clearInterval(this.checkBankTransactionStatusInterval);
            let userInfo = storageManager.getValue('userInfo');

            userActionCreator.getMobileResponse(userInfo.useraddress);



        }
        else {
            // clearInterval(this.checkTransactionStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getMobileResponse(userInfo.useraddress);
        }
    }
    private toggleHover = () => {
        this.setState({
            active: !this.state.active,

        });
    }
    private quoteToggleHover = () => {
        this.setState({

            quoteActive: !this.state.quoteActive,
        });
    }
    private changePage = (activePage: any) => {
        if (activePage == 'submit_details') {
            this.sendVerificationRequest();
        }
        this.setState({
            activePage: activePage
        })

    }
    private closeModal = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        ModalPopupActionCreator.closeModal()
    }
    private checkBankApproveTransactionStatusInterval: any;
    sendTransaction = () => {
        this.changePage('approve_transaction');
        let userInfo = storageManager.getValue('userInfo');
        let data = {
            useraddress: userInfo.useraddress,
            //useraddress: '0xdfc77789e1e6eaeb884a3b631648539532cb78e2',
            transactiontype: constants.TRANSACTION.APPLY_LOAN,
            bookingid: userstore.bookingId
        }
        userActionCreator.sendTransaction(data);
        this.checkBankApproveTransactionStatusInterval = setInterval(this.checkBankApproveTransactionStatus, 3000);
        this.setState({ approveTransactionActive: true })
    }
    render() {

        return (
            <div>
                {this.state.activePage == 'applyloan' && <div className="Navimages applyLoan">
                    <img src="/public/images/bank-flow/Group 236.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'submit_details')} className="applyLoanNav cp" >
                    </a>
                </div>}
                {this.state.activePage == 'submit_details' && <div className="Navimages LoanQRcode">
                    <img src="/public/images/bank-flow/Group237.png" alt="qr" />
                    <a className="QRcode ">
                        <div className=" QRCodeWraper">
                            <div className="QRcode">
                                <img src={this.state.qr} alt="" />
                            </div>
                        </div>
                    </a>
                </div>}
                {/* {this.state.activePage == 'confirm_details' && <div className="Navimages Banking">
                    <img src="/public/images/bank-flow/Group238.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'submit')} className="applyLoanNav selectcarNav cp"><div className="personalBanking"></div>
                    </a>
                </div>} */}
                {this.state.activePage == 'confirm_details' && <BankingConfirmation changePage={this.changePage} cancel={this.closeModal} bankStatement={this.state.bankStatement} sendTransaction={this.sendTransaction} />}


                {this.state.activePage == 'approve_transaction' && <div className="Navimages mobaprovalSection">
                    <img src="/public/images/bank-flow/Group 239.png" alt="" />
                    <div className="mobaprovalContainer">
                        <div className="mobileContainer">

                            <div className="mobileWraper ">
                                <a>
                                    <div className={this.state.approveTransactionActive ? "mobile active" : "mobile"}></div>
                                </a>

                            </div>
                            <div className="mobileText">
                            <span className="shade">Tap </span>
                                <span className="bright">Approve Transaction</span>
                                <span className="shade"> in your mobile phone to continue...</span>



                            </div>
                        </div>

                    </div>
                </div>
                }
                {//transaction_success
                    this.state.activePage == 'transaction_success' && <div className="Navimages mobaprovalSection">
                        <img src="/public/images/bank-flow/Group 240.png" alt="" />
                        <div className="mobaprovalContainer">
                            <div className="mobileContainer">

                                <div onClick={this.closeModal} className="mobileWraper verified cp">
                                    <a >
                                        <div className="mobile"></div>
                                    </a>

                                </div>
                                <div className="mobileText">


                                    <p className="verifiedText">Your request is currently under process.</p>
                                    <p className="verifiedText">You will be notified once your loan is approved.</p>
                                    <span className="verifiedIDno">Your Loan Reference: {'LR' + this.state.bookingnumber}</span>


                                </div>
                            </div>

                        </div>
                    </div>
                }
            </div>
        )
    }




}