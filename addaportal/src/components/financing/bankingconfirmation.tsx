import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';


import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import constants from '../../constants/constant';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import loginActionCreator from '../../actions/login/loginactioncreator';


export default class BankingConfirmation extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {

            
        };
    }

    componentDidMount() {
       
    };

    componentWillUnmount() {
        
    }
    componentWillMount(){

    }
    
    render() {

        return (
            
                <div className="Navimages Banking">
            <img src="/public/images/bank-flow/Group238.png" alt="" />
            <div className="personalBanking">
                <div className="detailsContainer">
                <div className="docVerificationContainer">
                            <div className="verificationCard">
                                <div className="verificationTitleWraper">
                                    <p className="verificationTitle smartPass">SmartPass ID</p>
                                </div>
                                <div className="btnVerifiedWraper">
                                    <span className="statusLabel">VERIFIED</span>
                                </div>
                            </div>
                        </div>
                        <div className="expendDetails">
                            <p className="expandTitle car">Car Details</p>
                            </div>

                    <div className="detailsWraper">
                        <div className="content">
                            <p className="title">Dealer</p>
                            <span className="sub-title">{this.props.bankStatement.cardetails.dealer}</span>
                        </div>

                        <div className="content">
                            <p className="title">Booking ID</p>
                            <span className="sub-title">{this.props.bankStatement.cardetails.bookingid}</span>
                        </div>

                        <div className="content">
                            <p className="title">Car Manufacturer</p>
                            <span className="sub-title">{this.props.bankStatement.cardetails.carmanufacturer}</span>
                        </div>
                        <div className="content">
                            <p className="title">Model</p>
                            <span className="sub-title">{this.props.bankStatement.cardetails.model}</span>
                        </div>
                        <div className="content">
                            <p className="title">Ex-Showroom Price (AED)</p>
                            <span className="sub-title">{this.props.bankStatement.cardetails.exshowroonprice}</span>
                        </div>
                        
                    </div>
                    </div>  


                <div className="detailsContainer">
                    <div className="expendDetails">
                        <p className="expandTitle bank">Bank Statement</p>
                    </div>

                    <div className="detailsWraper">
                        <div className="content">
                            <p className="title">Bank Name</p>
                            <span className="sub-title">{this.props.bankStatement.bankstatement.bankname}</span>
                        </div>

                        <div className="content">
                            <p className="title">Account Number</p>
                            <span className="sub-title">{this.props.bankStatement.bankstatement.accountnumber}</span>
                        </div>

                        <div className="content">
                            <p className="title">Credit Score</p>
                            <span className="sub-title">{this.props.bankStatement.bankstatement.creditscore}</span>
                        </div>
                        <div className="content">
                            <p className="title">Account Holder Name</p>
                            <span className="sub-title">{this.props.bankStatement.bankstatement.acccountholdername}</span>
                        </div>
                        
                    </div>
                </div>  

                <div className="btnSection">
            <div className="btnContainer">
                <div className="btnWraper">
                        <Button className="link" onClick={this.props.cancel} color="link">Cancel</Button>
                        <Button className="submit" onClick={this.props.sendTransaction} color="danger">CONFIRM & SUBMIT</Button>
                </div>
            </div>
                    </div>


                </div>

            </div>
            
        )
    }




}