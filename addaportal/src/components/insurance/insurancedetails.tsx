import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';
import InsuranceConfirmation from './insuranceconfirmation';
import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import constants from '../../constants/constant';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import userstore from '../../store/users/userstore';

export default class InsuranceDetails extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            qr: '',
            active: false,
            quoteActive: false,
            activePage: 'insurance_home',
            bookingnumber: userStore.bookingId,
            approveTransactionActive: false,
            insuranceDetails: {
                // "insurancequotedetails": {
                //     "vehicleestimatedvalue": 58700,
                //     "annualquotevalue": 1362,
                //     "startdate": "29-08-2019",
                //     "enddate": "29-08-2020"
                // },
                // "cardetails": {
                //     "dealer": "Al Futtaim Motors",
                //     "bookingid": 130,
                //     "carmanufacturer": "Toyota",
                //     "model": "Yaris",
                //     "exshowroonprice": "AED 58700"
                // },
                // "personaldetails": {
                //     "firstname": "Aladdin",
                //     "lastname": "G",
                //     "gender": "Male",
                //     "nationality": "United Arab Emirates"
                // }
            },
        };
    }

    componentDidMount() {
        userStore.addChangeListener(actions.GET_INSURANCE_QR, this.setQrCode);
        //after qr success
        loginStore.addChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getQrStatus);
        userStore.addChangeListener(actions.INSURANCE_CONFRIMATION_DETAILS, this.getInsuranceConfirmationDetails);
        userStore.addChangeListener(actions.CHECK_CONFIRM_INSURANCE_STATUS, this.getConfirmInsuranceStatus);
        userStore.addChangeListener(actions.SEND_CREDENTIAL, this.afterSendCredential);
        userStore.addChangeListener(actions.SAVE_INSURANCE_DETAILS, this.afterSaveInsurance);
    };

    componentWillUnmount() {
        clearInterval(this.checkInsuranceQrStatusInterval);
        clearInterval(this.checkConfirmInsuranceInterval);
        userStore.removeChangeListener(actions.GET_INSURANCE_QR, this.setQrCode);
        userStore.removeChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getQrStatus);
        userStore.removeChangeListener(actions.INSURANCE_CONFRIMATION_DETAILS, this.getInsuranceConfirmationDetails);
        userStore.removeChangeListener(actions.CHECK_CONFIRM_INSURANCE_STATUS, this.getConfirmInsuranceStatus);
        userStore.removeChangeListener(actions.SEND_CREDENTIAL, this.afterSendCredential);
        userStore.removeChangeListener(actions.SAVE_INSURANCE_DETAILS, this.afterSaveInsurance);
    }


    private changePage = (activePage: any) => {
        if (activePage == 'submit_details') {
            this.getInsuranceQr();
        }
        this.setState({
            activePage: activePage
        })

    }
    getInsuranceQr = () => {
        let userInfo = storageManager.getValue('userInfo');

        userActionCreator.getInsuranceQr(userInfo.useraddress);
    }
    private checkInsuranceQrStatusInterval: any;
    private setQrCode = (response: any) => {

        this.setState({
            'qr': response.qr,
        });
        //checking status in every 3sec
        this.checkInsuranceQrStatusInterval = setInterval(this.checkInsuranceQrStatus, 3000);



    }
    private checkInsuranceQrStatus = () => {
        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkTransactionStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    getQrStatus = (response: any) => {
        if (response && response.status) {

            clearInterval(this.checkInsuranceQrStatusInterval);
            let userInfo = storageManager.getValue('userInfo');

            userActionCreator.getInsuranceConfirmationDetails(userInfo.useraddress);



        }
        else {
            // clearInterval(this.checkInsuranceQrStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getInsuranceConfirmationDetails(userInfo.useraddress);
        }
    }
    getInsuranceConfirmationDetails = (response: any) => {
        if (response && response.result ) {
            this.setState({ insuranceDetails: response.result, activePage: 'confirm_details' })

        }
        console.log('insuranceDetails', response);

    }
    insuranceConfirmation = () => {
        this.changePage('approve_transaction');
        let userInfo = storageManager.getValue('userInfo');
        let data = {
            useraddress: userInfo.useraddress,
            //useraddress: '0xdfc77789e1e6eaeb884a3b631648539532cb78e2',
            transactiontype: constants.TRANSACTION.BUY_INSURANCE,
            bookingid: userstore.bookingId
        }
        userActionCreator.confirmInsurance(data);
        this.checkConfirmInsuranceInterval = setInterval(this.checkConfirmInsuranceStatus, 3000);
        this.setState({ approveTransactionActive: true })
    }
    private checkConfirmInsuranceInterval: any;
    private checkConfirmInsuranceStatus = () => {

        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkConfirmInsuranceStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    getConfirmInsuranceStatus = (response: any) => {
        if (response && response.status) {


            clearInterval(this.checkConfirmInsuranceInterval);
            let userInfo = storageManager.getValue('userInfo');
            let credentials = {
                useraddress: userInfo.useraddress,
                credentialtype: "insurance",
                bookingid: this.state.bookingnumber,
                insurancepolicyno: 'IR' + this.state.bookingnumber,

            }
            userActionCreator.sendCredential(credentials);



        }
        else {
            // clearInterval(this.checkBankApproveTransactionStatusInterval);

            // let userInfo = storageManager.getValue('userInfo');


            // let loanDetails = {
            //     bookingno: this.state.bookingnumber,
            //     loanreferencenumber: 'LR' + this.state.bookingnumber,
            //     loanprovidingbank: "HSBC",
            //     loanapplieddate: this.getTodaysDate,
            //     fname: userInfo.userfulldetails['First Name'],
            //     lname: userInfo.userfulldetails['Second Name'],
            //     statementbname: this.state.bankStatement.bankstatement.bankname,
            //     statementacctno: this.state.bankStatement.bankstatement.accountnumber,
            //     statementcreditscore: this.state.bankStatement.bankstatement.creditscore,
            //     smartpassidsubmitted: true,
            //     carquotesubmitted: true,
            //     bankstatementsubmitted: true
            // }
            // userActionCreator.saveLoanDetails(loanDetails);
            // clearInterval(this.checkTransactionStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');

            // userActionCreator.getMobileResponse(userInfo.useraddress);
        }
    }
    afterSendCredential=(response:any)=>{
        if (response) {
            
            this.setState({
                activePage: 'payment_page'
            })
        }
    }
    saveInsurance = () => {
        

        let userInfo = storageManager.getValue('userInfo');
        let today = new Date();

        var day = today.getDate();
        var month_index = today.getMonth()+1;
        var year = today.getFullYear();

        let modifiedMonthIndex;

        if (month_index > 9) {

            modifiedMonthIndex = month_index;

        } else {

            modifiedMonthIndex = "0" + month_index;

        }

        let date = "" + day + "-" + modifiedMonthIndex + "-" + year;

        let insuranceDetails = {
            bookingno: this.state.bookingnumber,
            insurancereferencenumber: 'IR' + this.state.bookingnumber,
            insurancevendor: constants.VENDOR.INSUARANCE,
            insurancereceiveddate: date,
            premiumamount: this.state.insuranceDetails.insurancequotedetails.annualquotevalue,
            smartpassidsubmitted: true,
            carquotesubmitted: true,
            carloansubmitted: true,
            drivinglicensesubmitted: true
        }
        userActionCreator.saveInsuranceDetails(insuranceDetails);
        
    }
    afterSaveInsurance=(response:any)=>{
        if (response && response.message=='success') {
            
            this.setState({
                activePage: 'transaction_success'
            })
        }
    }
    private closeModal = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        ModalPopupActionCreator.closeModal()
    }
    render() {

        return (
            <div>
                {this.state.activePage == 'insurance_home' && <div className="Navimages Insurance_Landing">
                    <img src="/public/images/Insurance Flow/Group 241.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'submit_details')} className="applyLoanNav cp" >
                        <div className="insurelandingNav"></div></a>
                </div>}
                {this.state.activePage == 'submit_details' && <div className="Navimages LoanQRcode">
                    <img src="/public/images/Insurance Flow/Group 242.png" alt="qr" />
                    <a  className="QRcode">
                        <div className=" QRCodeWraper">
                            <div className="QRcode">
                                <img src={this.state.qr} alt="" />
                            </div>
                        </div>
                    </a>
                </div>}
                {/* {this.state.activePage == 'confirm_details' && <div className="Navimages Insurance_quote">
                    <img src="/public/images/Insurance Flow/Group 249.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'models')} className="applyLoanNav cp"><div className="insureQuoteNav"></div>
                        </a>
                </div>} */}
                {this.state.activePage == 'confirm_details' && <InsuranceConfirmation changePage={this.changePage} cancel={this.closeModal} insuranceDetails={this.state.insuranceDetails} insuranceConfirmation={this.insuranceConfirmation} />}
                
                {this.state.activePage == 'approve_transaction' && <div className="Navimages mobaprovalSection">
                    <img src="/public/images/Insurance Flow/Group 244.png" alt="" />
                    <div className="mobaprovalContainer">
                        <div className="mobileContainer">

                            <div className="mobileWraper ">
                                <a >
                                    <div className={this.state.approveTransactionActive ? "mobile active" : "mobile"}></div>
                                </a>

                            </div>
                            <div className="mobileText">
                            <span className="shade">Tap </span>
                                <span className="bright">Approve Transaction</span>
                                <span className="shade"> in your mobile phone to continue...</span>



                            </div>
                        </div>

                    </div>
                </div>
                }
                   {this.state.activePage == 'payment_page' &&  <div className="Navimages Insurance_payment">
                        <img src="/public/images/Insurance Flow/Group 245.png" alt="" />
                        <a onClick={this.saveInsurance} className="applyLoanNav cp">
                            <div className="insurePaymentNav"></div></a>
                    </div>
                    }
                {//transaction_success
                    this.state.activePage == 'transaction_success' && <div className="Navimages mobaprovalSection">
                        <img src="/public/images/Insurance Flow/Group 246.png" alt="" />
                        <div className="mobaprovalContainer">
                            <div className="mobileContainer">

                                <div onClick={this.closeModal} className="mobileWraper verified cp">
                                    <a >
                                        <div className="mobile"></div>
                                    </a>

                                </div>
                                <div className="mobileText">


                                    <p className="verifiedText">You will receive a quote in your mobile phone in a moment</p>
                                    <span className="verifiedIDno">Your Insurance Policy Number: {'IR' +this.state.bookingnumber}</span>


                                </div>
                            </div>

                        </div>
                    </div>
                }
            </div>
        )
    }




}