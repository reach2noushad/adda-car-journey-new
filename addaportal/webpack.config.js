var path = require('path');

module.exports = { 
    entry: './src/index.js',
    
    output: { 
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/dist/' 
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    // Source maps support ('inline-source-map' also works)
    devtool: 'source-map',
  
    devServer: { historyApiFallback: true, }, 
    module: { 
        rules: [
            { 
                test: /\.(js|jsx)$/, 
                exclude: /node_modules/, 
                use: ['babel-loader'] 
            },
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            },
            
            {
                test: /\.(png|ttf|eot|woff|woff2|svg?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader'
            },
            
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
        ] 
    }
};
