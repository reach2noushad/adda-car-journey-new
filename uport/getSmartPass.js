const express = require('express')
const bodyParser = require('body-parser')
const decodeJWT = require('did-jwt').decodeJWT
const { Credentials } = require('uport-credentials')
const transports = require('uport-transports').transport
const message = require('uport-transports').message.util
const ngrok = require('ngrok')

let endpoint;
const app = express();
app.use(bodyParser.json({ type: '*/*' }))

//setup Credentials object with newly created application identity.
const credentials = new Credentials({
  appName: 'Login Example',
  did: 'did:ethr:0x9aadd100341ce940610b216b2b688617f05e0ba9',
  privateKey: '951ed41d161eea72e46c10717d30b88766848bf981bf4fd2f9568f15e011d332' });


  app.get('/', async function (req, res) {

    await ngrok.kill(); 

    endpoint = await ngrok.connect(5600);


    credentials.createDisclosureRequest({
      notifications: true,
      callbackUrl: endpoint + '/callback'
    }).then(requestToken => {
      console.log(decodeJWT(requestToken))  //log request token to console
      const uri = message.paramsToQueryString(message.messageToURI(requestToken), {callback_type: 'post'})
      const qr =  transports.ui.getImageDataURI(uri)
      res.send(`<div><img src="${qr}"/></div>`)
    })
  })




  app.post('/callback', (req, res) => {
    const jwt = req.body.access_token
    credentials.authenticateDisclosureResponse(jwt).then(creds => {
      // take this time to perform custom authorization steps... then,
      // set up a push transport with the provided 
      // push token and public encryption key (boxPub)
      const push = transports.push.send(creds.pushToken, creds.boxPub)
  
      credentials.createVerification({
        sub: creds.did,
        exp: Math.floor(new Date().getTime() / 1000) + 30 * 24 * 60 * 60,
        claim: {'Smart Pass' : {'Last Seen' : `${new Date()}`,
        "First Name" : "Abdullah",
        "Second Name" : "Al Marzouqi",
        "Gender":"Male",
        "Nationality" : "United Arab Emirates"
      }}
      }).then(attestation => {
        console.log(`Encoded JWT sent to user: ${attestation}`)
        console.log(`Decodeded JWT sent to user: ${JSON.stringify(decodeJWT(attestation))}`)
        return push(attestation)  // *push* the notification to the user's uPort mobile app.
      }).then(res => {
        console.log(res)
        console.log('Push notification sent and should be recieved any moment...')
        console.log('Accept the push notification in the uPort mobile application')
        ngrok.disconnect(); 
      })
    })
  })


  // run the app server and tunneling service
const server = app.listen(5600, () => {
  console.log("Smart Pass listening at 5600");

  })