const express = require('express')
const bodyParser = require('body-parser')
const decodeJWT = require('did-jwt').decodeJWT
const { Credentials } = require('uport-credentials')
const transports = require('uport-transports').transport
const message = require('uport-transports').message.util
var mnid = require('mnid');
const ethers = require('ethers');
const ngrok = require('ngrok')
const provider = ethers.providers.getDefaultProvider('rinkeby');

let endpoint;
const app = express();
app.use(bodyParser.json({ type: '*/*' }))

//setup Credentials object with newly created application identity.
const credentials = new Credentials({
    appName: 'Login Example',
    did: 'did:ethr:0x9aadd100341ce940610b216b2b688617f05e0ba9',
    privateKey: '951ed41d161eea72e46c10717d30b88766848bf981bf4fd2f9568f15e011d332' });


app.get('/', async function (req, res)  {

  await ngrok.kill(); 

  endpoint = await ngrok.connect(5900);


  credentials.createDisclosureRequest({
    notifications: true,
    accountType: 'keypair',
    network_id: '0x4',
    callbackUrl: endpoint + '/callback'
    }).then(requestToken => {
      console.log(requestToken)
      console.log(decodeJWT(requestToken))  //log request token to console
      const uri = message.paramsToQueryString(message.messageToURI(requestToken), {callback_type: 'post'})
      const qr =  transports.ui.getImageDataURI(uri)
      res.send(`<div><img src="${qr}"/></div>`)
      })
  })

  app.post('/callback', async function (req, res) {
        console.log("Callback hit")
        const jwt = req.body.access_token
        credentials.authenticateDisclosureResponse(jwt).then(creds => {
        console.log("this is creds mnid",creds.mnid);
        let decodedmnid = mnid.decode(creds.mnid);
        console.log(decodedmnid);
        let addressToSendEther = decodedmnid.address;

        console.log("this is the address to send ethers to",addressToSendEther);

        var privateKey = '0xF6F73ED9A00412658BFD82BCBEE2435764D97AD3B3C2C9FA5CB3B6CDF9B50C30'
        var wallet = new ethers.Wallet(privateKey);
        wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
        var amount = ethers.utils.parseEther('1');

        wallet.send(addressToSendEther, amount).then (function(sendPromise){
          provider.waitForTransaction(sendPromise.hash);
          console.log(addressToSendEther+" loaded with 2 Ethers");

        })


    })
 })


// run the app server and tunneling service
const server = app.listen(5900, () => {
  console.log("Get Ethers Filled listening at 5900");

 })
