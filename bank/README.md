### Cymorg ###

# Requirements #

For development, you will only need Node.js installed on your environement. And please use the appropriate Editorconfig plugin for your Editor (not mandatory).

# Node #

Node is really easy to install & now include NPM. You should be able to run the following command after the installation procedure below.

$ node --version
v4.4.3

$ npm --version
2.15.1

Just go on official Node.js website & grab the installer. Also, be sure to have git available in your PATH, npm might need it.

# Install #

$ bitbucket clone
$ cd PROJECT
$ npm install

# Start & watch #

$ gulp watch

# linting #

$ gulp tslint

# Build for production #

$ gulp dist