var gulp = require('gulp');
var gutil = require('gulp-util');
var tslint = require('gulp-tslint');
var webpack = require('webpack');
var webpackConfig = require('./webpack.config.js');
var exec = require('child_process').exec;
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglifycss = require('gulp-uglifycss');
var runSequence = require('run-sequence');
var environments = require('gulp-environments');
var replace = require('gulp-string-replace');
// var pump = require('pump')
// create a single instance of the compiler to allow caching
var compiler = webpack(webpackConfig);
var development = environments.development;
var staging = environments.make("staging");
var production = environments.production;
// var gulp_remove_logging = require("gulp-remove-logging");
// var stripDebug = require('gulp-strip-debug');

gulp.task('webpack:build', function (done) {
    compiler.run(function (err, stats) {
        if (err) throw new gutil.PluginError('webpack:build-dev', err);
        gutil.log('[webpack:build]', stats.toString());
        done();
    });
});

//gulp default task
gulp.task('watch', function () {
    gulp.watch('./src/**/*.ts?(x)', ['webpack:build']);
});

//gulp task serializer task
//gulp.task('dist', ['tslint', 'minifyJs', 'minifyCss']);

gulp.task('dist', function () {
    runSequence('webpack:build', 'minifyJs', 'minifyCss');
});

//gulp tslint command to check typescript coding standards
gulp.task('tslint', function () {
    return gulp.src('./src/**/*.ts?(x)')
        .pipe(tslint({
            formatter: "verbose"
        }))
        .pipe(tslint.report())
});
// gulp.task("remove_logging", function () {
//     return gulp.src("./dist/bundle.js")
//         .pipe(
//         gulp_remove_logging({
//             // Options (optional) 
//             // eg: 
//             // namespace: ['console', 'window.console'] 
//         })
//         )
//         .pipe(
//         gulp.dest('./dist')
//         );
// });

// gulp.task('strip_console', function () {
//     return gulp.src('./dist/bundle.js')
//         .pipe(stripDebug())
//         .pipe(gulp.dest('./dist'));
// });

// gulp.task('compress', function (cb) {
//     pump([
//         gulp.src('./dist/*.js'),
//         uglify(),
//         gulp.dest('./dist')
//     ],
//         cb
//     );
// });

gulp.task('minifyJs', function () {
    return gulp.src('./dist/*.js')
        .pipe(concat('bundle.js'))
        .pipe(uglify({
            compress:{
                drop_console :true
            }
        }))
        .pipe(gulp.dest('./dist'))
});

gulp.task('minifyCss', function () {
    gulp.src('./dist/**/*.css')
        .pipe(concat('bundle.css'))
        .pipe(production(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        })))
        .pipe(gulp.dest('./dist'));
});

gulp.task('clean', ['webpack:build'], function () {
    del(['build/generated/non-essential-vendor-bundle.js', 'build/generated/non-essential-vendor-bundle.js.map']).
        then(console.log('Deleted non-essential bundles'));
});

gulp.task('replace_1', function () {
    gulp.src(["./index.html"])
        .pipe(replace('test', 'work avo'))
        .pipe(gulp.dest('./dist'))
});

//gulp command to run default task
gulp.task('default', ['watch']);
