import { Injectable } from '@angular/core';
import { BaseService } from '../../core/base/base.service';
import { Endpoints } from '../../core/constants';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  username :any;
  password:any;
  actortype : string;
  constructor( public http: HttpClient ) { 
  }
  submitLoginForm(){
    const data:any ={
      "username":this.username,
      "password":this.password,
	    "actortype":this.actortype
    }
    return this.http.post(environment.baseUrl + Endpoints.POST_LOGIN_INFO, data);
  }
}
