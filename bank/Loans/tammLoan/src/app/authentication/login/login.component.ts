import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username:any;
    password:any;
    actortype:any = "bank";
   loginForm = new FormGroup({
    username: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
  });
  constructor(
    private loginService : LoginService,
    private router: Router,
    private toastr: ToastrService
  ) { 
  }
 
  ngOnInit() {
  }
 
  onSubmit() {
    this.loginService.username = this.loginForm.value.username;
    this.loginService.password = this.loginForm.value.password;
    this.loginService.actortype = "bank";  
    
    this.loginService.submitLoginForm().subscribe((response: any) => {  
      this.router.navigateByUrl('/loans');
    },
    (err) => {
      this.toastr.error("Invalid username or password", 'Could not login. Please try again');
    });
  }
}
