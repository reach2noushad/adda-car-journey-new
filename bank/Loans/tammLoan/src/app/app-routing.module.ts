import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './authentication/login/login.component';
import { LoansComponent } from './loans/loans.component';
import { PendingApprovalsComponent } from './pending-approvals/pending-approvals.component';
import { HistoryComponent } from './history/history.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path:'pendingApprovals', component: PendingApprovalsComponent },
  { path: 'loans', component: LoansComponent },
  { path: 'history', component: HistoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
