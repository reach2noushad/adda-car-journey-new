export class Endpoints {
    public static POST_LOGIN_INFO = "user/login";
    public static GET_PENDING_LOAN_DETAILS = "loan/pendingrequests";
    public static GET_BOOKING_DETAILS = "bookings?id=";
    public static POST_APPROVE_LOAN = "loan/approve";
    public static GET_LOAN_HISTORY = "loan/history";
    public static GET_VIEW_DETAILS = "bookings";
}
