import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Endpoints } from '../../core/constants';

@Injectable()
export class BaseService {

  constructor(public http: HttpClient) { }
  
  /**
   * Makes the http get request call using the http client
   * @param {string} url
   */
  get(url: string) {
    return this.http.get(environment.baseUrl + url);
  }



  /**
   * Makes the http get request call using the http client
   * @param {string} url
   * @param {HttpParams} urlParams
   * @param {customeHeader} any
   * @return {Observable<any>}
   */
  getWithHeader(url: string, urlParams?: HttpParams, customeHeader?: any): Observable<any> {
    return this.http.get(environment.baseUrl + url, { params: urlParams, headers: customeHeader });
  }




  /**
   * Makes the http post request call using the http client
   *
   * @param {string} url
   * @param {any} data
   */
  post(url: string, data: any) {
    return this.http.post(environment.baseUrl + url, data);
  }

  /**
   * Makes the http post request call using the http client and supplied header
   *
   * @param {string} url
   * @param {any} data
   * @param {HttpHeaders} header
   * @returns {Observable}
   */
  postWithHeader(url: string, data: any, header: HttpHeaders): Observable<any> {
    return this.http.post(url, data, { headers: header });
  }

  /**
   * Makes the http put request call using the http client
   *
   * @param {string} url
   * @param {any} data
   */
  put(url: string, data: any) {
    return this
      .http
      .put(environment.baseUrl + url, JSON.stringify(data));
  }

  /**
   * Makes the http delete request call using the http client
   *
   * @param {string} url
   */
  delete(url: string) {
    return this
      .http
      .delete(environment.baseUrl + url);
  }

  /**
   * Makes the http patch request call using the http client
   *
   * @param {string} url
   * @param {any} data
   */
  patch(url: string, data: any) {
    return this
      .http
      .patch(environment.baseUrl + url, data);
  }
}
