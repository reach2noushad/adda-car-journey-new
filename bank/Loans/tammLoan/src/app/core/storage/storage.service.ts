import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class StorageService {
  constructor(public storage: LocalStorageService, @Inject(PLATFORM_ID) public platformId: Object) { }

  /**
   *  @description method to save value to storage
   *  @param item string
   *  @param value any
   *  @returns void
   */
  setItem(item: string, value: any): void {
    if (isPlatformBrowser(this.platformId)) {
      this.storage.store(item, value);
    }
  }

  /**
     * @description method to get storage value
     * @param item string
     * @returns any
     */
  getItem(item: string): any {
    if (isPlatformBrowser(this.platformId)) {
      return this.storage.retrieve(item);
    }
  }
  /**
   * @description method to remove storage value
   * @param item string
   * @returns any
   */
  removeItem(item: string): any {
    if (isPlatformBrowser(this.platformId)) {
      return this.storage.clear(item);
    }
  }
}
