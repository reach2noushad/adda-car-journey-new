import { Component, OnInit } from '@angular/core';
import { LoansService } from '../../loans/loans.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  currentListNumber: any;
  constructor(
    private loansService : LoansService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
  }
  approveLoan(){
    this.loansService.setLoaderTrue();
      this.loansService.approveLoan().subscribe((response: any) => {
       this.loansService.setLoaderFalse();
       console.log("Loan Approved", this.loansService.loanApproveDate);
       this.loansService.updatependingDetails();
       this.loansService.updateHistory();
       this.toastr.success('Loan Approved Successfully', 'Loan Approved');

        this.loansService.pendingListNumber$.subscribe((res) => {
          console.log(res, 'curentCount')
          this.currentListNumber = res
        })

        this.loansService.getPendingListNumber(this.currentListNumber - 1);

      },
        (err) => {
          console.log("error");
          this.toastr.error("Loan approval failed", 'Please try again');
        });
    }
  
}
