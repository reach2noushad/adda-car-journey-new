import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor( 
    private router: Router,
    private toastr: ToastrService,
     ) { }

  ngOnInit() {
  }
  disableBackButton()
      {
      window.history.forward()
      } 
  logOut(){
    this.router.navigate(['']);
    this.toastr.success('Logged out Successfully', 'Log Out');
  }
}
