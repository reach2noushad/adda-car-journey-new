import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { LoansService } from './loans.service';

@Component({
  selector: 'app-loans',
  templateUrl: './loans.component.html',
  styleUrls: ['./loans.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoansComponent implements OnInit {
  details:any;
  pendingCount: any;
  isFooter : boolean = false;
  pendingDetails: any;
  constructor(
    private loansService : LoansService
  ) { }

  ngOnInit() {
    this.loansService.pendingListNumber$.subscribe((res) => {
      if(res){
        this.pendingCount = res;
        this.isFooter = true;
      }
      else {
        this.pendingCount = 0;
        this.isFooter = false;
      }
      
    })

  }

  getBookingDetails(){   
    this.loansService.getBookingDetails().subscribe((response: any) => {  
      console.log("Response for Get Booking Details", response);
    },
    (err) => {
      console.log("error");
    });
  }
}
