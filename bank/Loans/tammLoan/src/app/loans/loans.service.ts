import { Injectable } from '@angular/core';
import { BaseService } from '../core/base/base.service';
import { Endpoints } from '../core/constants';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoansService {

  loanRefNo :any;
  bookingID : any;
  loanApproveDate : any;
  public showLoader = new BehaviorSubject(false);
  showLoaderRefered = this.showLoader.asObservable();
  isPendingLists : boolean = false;
  // pending Details Update observable
  public pendingDetailsUpdate = new BehaviorSubject(false);
  pendingDetailsUpdateRefered = this.pendingDetailsUpdate.asObservable();
  
//  history Update observable
  public historyUpdate = new BehaviorSubject(false);
  historyUpdateRefered = this.historyUpdate.asObservable();

// for getting pending list number 
  public pendingListNumber = new BehaviorSubject(false);
  pendingListNumber$ = this.pendingListNumber.asObservable();

  constructor(public http: HttpClient) { }
  // api for pending details
  getPendingLoanDetails() {
    return this.http.get(environment.baseUrl + Endpoints.GET_PENDING_LOAN_DETAILS);
  }
  //api for booking details
  getBookingDetails() {
    return this.http.get(environment.baseUrl + Endpoints.GET_BOOKING_DETAILS + this.bookingID);
  }
  //api for login
  approveLoan() {
    const data: any = {
      "loanrefno": this.loanRefNo,
      "bookingid": this.bookingID,
      "loanapproveddate": this.loanApproveDate
    }
    return this.http.post(environment.baseUrl + Endpoints.POST_APPROVE_LOAN, data);
  }
  //api for history
  getLoanHistoryDetails(){
    return this.http.get(environment.baseUrl + Endpoints.GET_LOAN_HISTORY);
  }
  // api for view moew
  getViewMoreDetails(id){
    return this.http.get(environment.baseUrl + Endpoints.GET_VIEW_DETAILS + '?id=' + id);
  }

  // loader 
  setLoaderTrue() {
    this.showLoader.next(true);
  }
  setLoaderFalse() {
    this.showLoader.next(false);
  }
  updatependingDetails() {
    this.pendingDetailsUpdate.next(true);
  }
  updateHistory() {
    this.historyUpdate.next(true);
  }

  // function for pendingListNumeber 
  getPendingListNumber(length){
    this.pendingListNumber.next(length);
  }

  destroySubscription(){
    this.showLoader.complete();
  }
}
