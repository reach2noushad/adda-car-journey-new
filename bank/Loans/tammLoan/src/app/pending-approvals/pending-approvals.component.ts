import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoansService } from '../loans/loans.service';
@Component({
  selector: 'app-pending-approvals',
  templateUrl: './pending-approvals.component.html',
  styleUrls: ['./pending-approvals.component.scss']
})
export class PendingApprovalsComponent implements OnInit ,OnDestroy{

  details: any;
  viewDetails: any;
  showLoader: any;
  pendingListlength: any;
  isPendingLists : boolean = false;

  constructor(
    private loansService: LoansService
  ) { }

  ngOnInit() {
    this.getPendingLoanDetails();
    this.loansService.showLoaderRefered.subscribe((res) => { this.showLoader = res })
    this.loansService.pendingDetailsUpdateRefered.subscribe((res) => {
      if(res == true){
        this.getPendingLoanDetails();
      }
    })
  }

  getPendingLoanDetails() {
    this.loansService.getPendingLoanDetails().subscribe((response: any) => {
      this.details = response.pendingloans;
      if(this.details){
        this.pendingListlength = this.details.length;
        this.isPendingLists = true;
        this.viewDetails = response.pendingloans[0];
        this.loansService.bookingID = this.viewDetails.bookingid;
        this.loansService.loanRefNo = this.viewDetails.loanrefno;
        this.loansService.loanApproveDate = this.viewDetails.loanapplieddate;
        this.loansService.getPendingListNumber(this.details.length);
      }
      else{
        this.isPendingLists = false;
      }     
    },
      (err) => {
        console.log("error");
      });
  }
  viewLoanDetails(data: any) {
    this.viewDetails = data;
    this.loansService.bookingID = this.viewDetails.bookingid;
    this.loansService.loanRefNo = this.viewDetails.loanrefno;
    this.loansService.loanApproveDate = this.viewDetails.loanapplieddate;
  }
  ngOnDestroy(){
    this.loansService.destroySubscription();
  }

}
