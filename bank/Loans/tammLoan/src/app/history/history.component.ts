import { Component, OnInit,ViewChild,ElementRef,HostListener } from '@angular/core';
import {trigger, transition, style, animate} from '@angular/animations';
import { LoansService } from '../loans/loans.service';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  animations: [trigger ('fade',[
    //state(),
    transition( 'void => *',[
       style({ opacity: 0 }), 
       animate(800, style({opacity: 1}))
     
    ]),
  ])
 ] 
})
export class HistoryComponent implements OnInit {
 details: any;
 showMoreDetails= false;
 showHistoryStatus: any;
 statusAdded = false;
 showBookingId: any;
 viewMoreContents:any;
 showDetails: any;
 accountHolderName: string;
 currentRowIndex: any;
 scrollAdded: any;
 @ViewChild('tableBodyElement', {static: false}) tableBodyElement: ElementRef;
 @ViewChild('tableWrapper', {static: false}) tableWrapper: ElementRef;
  constructor(
    private loansService: LoansService
  ) { }

  ngOnInit() {
    this.LoanHistoryDetails();
    this.loansService.historyUpdateRefered.subscribe((res) => {
      if (res == true) {
        console.log(res);
        this.LoanHistoryDetails();
      }
    })
  }
  LoanHistoryDetails(){
    this.loansService.getLoanHistoryDetails().subscribe((response: any) => {
      this.details = response.approvedloans;
    },
      (err) => {
        console.log("error");
      });
}

  viewMoreDetails(detail,index){
    this.showDetails = detail; 
    this.currentRowIndex = index;
    this.showMoreDetails = true;
    this.showBookingId = detail.bookingid;
    this.accountHolderName = detail.applicantfname + ' '+ detail.applicantlname;
    this.loansService.getViewMoreDetails(this.showBookingId).subscribe((response: any) => {
      this.viewMoreContents = response.details;
     // console.log(this.viewMoreContents.bookingid,'hi')
    },
      (err) => {
        console.log("error");
      });
  }
  closeViewMore(){
    this.showMoreDetails = false;
    this.LoanHistoryDetails();
    this.currentRowIndex = null;
  
  }
  setDate(data){
    if(data.loanstatus == 1){
      return data.loanapproveddate
    }
      
  }
  tableScroll(){
    this.tableBodyElement.nativeElement.style.width =  this.tableWrapper.nativeElement.scrollLeft + this.tableWrapper.nativeElement.offsetWidth + 'px';
    if(this.tableBodyElement.nativeElement.scrollHeight > this.tableBodyElement.nativeElement.offsetHeight ){
      this.scrollAdded = true;
    }else{
     this.scrollAdded = false;
    }
   }
   @HostListener('window:resize', ['$event'])
     onResize(event) {
       this.tableScroll()
   }
}
