/** palyer information */
export interface IPlayerInfo {
    id?: string,
    firstname?: string,
    lastname?: string,
    coachid?: string,
    mentorid?: string,
    groupid?: string,
    cohortid?: string,
    linkedinurl?: string,
    ismentor?: boolean,
    summaryofskillsandstrengths?: string,
    avatarurl?: string,
    designation?: string,
    function?: string,
    company?: string,
    yearsofexperience?: string,
    yearsoffirm?: string,
    yearsatcurrentrole?: string,
    highesteducationalqualification?: string,
    yearsinindustry?: string,
    yearsinmanagement?: string,
    currentindustryid?: string,
    priorindustryid?: string,
    managementlevelid?: string,
    modifieddate?: string,
    createdon?: string,
    username?: string
    roleId?: number;
    seriesId?: string;
    isanonymous?: boolean;
    [key: string]: any;
    seriesRoleMapping?:any;
    userCoachMapping?:any;
    groupname?: string;
};

export interface IConfirmationModalData {
    callback: Function;
    messageText: string;
    title: string;
    warningText?: string;
    okBtnText?: string;
    cancelBtnText?: string;
}

export interface IUserInfo {
    userName: string;
}

export interface ICompanyDetails {
    name?: string;
    industryid?: string;
    businessmodelid?: string;
    incorporationid?: string;
    businesstypeid?: string;
    description?: string;
    individualInvestors?: string;
    managementTeam?: string;
    financialInstitutions?: string;
    generalPublic?: string;
    ownership: any
}


export interface IClientrRelationValue {
    name?: string,
    value?: string,
    dimensionValueId?: string
}

export interface IPipeLineData {
    preQualifiedSuspects?: number;
    qualifiedProspects?: number;
    opportunityPursuits?: number;
    dealsBeingNegotiated?: number;
    valueOfPipeline?: number;
}

export interface IBusinessPipeline {
    code?: string;
    dimensionValues?: [IDimensionValues];
    id: string;
    name: string
}

export interface IinvestorSentiment {
    name?: string
    parameterGroupValues?: IParameterGroup[];
    value?: number;
}

export interface IParameterGroup {
    description?: string;
    maxValue?: number;
    minValue?: number;
    order?: number
}

export interface IGeographyDetails {
    name?: string;
    parameterGroupValue?: IParameterGroup[];
    dimensionValues?: IDimensionValues[];
}

export interface IDimensionValues {
    value?: number;
    dimensionValueId?: string;
}

export interface IDropDown {
    id?: string;
    name?: string;
    avatarUrl?: string
}

export interface IGameRules {
    playMode?: IGameRulesSructure;
    timeLimit?: IGameRulesSructure;
    overAllTimeLimitValue?: IGameRulesSructure;
    timeLimitPerMove?: IGameRulesSructure;
    stageDuration?: IGameRulesSructure;
    virtualGameDurationInMonths?: IGameRulesSructure;
    eventLimitPerMove?: IGameRulesSructure;
    actionsPerMoveLimit?: IGameRulesSructure;
    allowDelegation?: IGameRulesSructure;
    delegationActionPercentage?: IGameRulesSructure;
    delegationEffectivenessPercent?: IGameRulesSructure
    savePointsLimit?: IGameRulesSructure;
    coachAdviceLimit?: IGameRulesSructure;
    budgetEditMode?: IGameRulesSructure;
    deviationPercentage?: IGameRulesSructure;
    url?: IGameRulesSructure;
    allowGroupCollaboration?: IGameRulesSructure;
    timeLimitPerMoveValue?: IGameRulesSructure;
    eventsMaxLimitPerMove?: IGameRulesSructure;
    displayFiscalYearOfCompany?: IGameRulesSructure;
    budgetHorizon?: IGameRulesSructure
    year?: IGameRulesSructure;
    startMonth?: IGameRulesSructure;
    useFixedEvents?: IGameRulesSructure
}

export interface IGameRulesSructure {
    description?: string
    name?: string
    value?: any
    key?: any
}

export interface ICurrentUserProfile {
    id: string,
    firstname: string;
    lastname: string;
    username: string;
    role: string;
    designation: string;
    function: string;
    company: string;
    yearsofexperience: any;
    yearsoffirm: any;
    yearsatcurrentrole: any;
    coachUsername: string;
    coachid: string;
    mentorUsername: string;
    mentorid: string;
    highesteducationalqualification: string;
    groupid: string;
    groupname: string;
    cohortid: string;
    linkedinurl: string;
    currentIndustry: string;
    currentindustryid: string;
    priorIndustry: string;
    priorindustryid: string;
    yearsinindustry: any;
    yearsinmanagement: any;
    managementLevel: string;
    managementlevelid: string;
    ismentor: boolean;
    avatarurl: string;
    summaryofskillsandstrengths: string
    isanonymous: boolean
    seriesRoleMapping: any;
    userCoachMapping: any

}

export interface IHistory {
    id: string;
    move: Object;
    actions: any;
    events: any;
    notes: any;
    informationsSought: any;
    advicesSought: any;
}

export interface IGameYear {
    Year: number;
    Quarter: number;
    Month: number;
}

export interface IFiscalYear {
    Year: string;
    Month: string;
}

export interface IChangePassword {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
    [key: string]: string;
}

export interface IDialogues {
    USER_ALREADY_EXIST?: string;
    INVALID_CREDENTIALS?: string;
    SEARCH_PLAYER?: string;
    UNAUTHORISED_USER?: string;
    NO_COACH_ASSIGNED?: string;
    ADVICE_LIMIT?: string;
    INVALID_MESSAGE?: string;
    INVALID_SELECTION?: string;
    NO_ACTIONS_LEFT?: string;
    BUDGET_NOT_FREEZED?: string;
    BUDGET_FREEZED?: string;
    ACTION_ALREADY_REVERSED?: string;
    ACTION_NOT_REVERSIBLE?: string;
    CHOOSE_DIMENSION?: string;
    GAME_OVER?: string;
    TIME_OVER?: string;
    MOVE_TIME_OVER?: string;
    INVALID_ROLE?: string;
    PASSWORD_MISMATCH?: string;
    FIELDS_MISSING?: string;
    PLAYER_SAVED?: string;
    CONFIRM_LEAVE_PAGE?: string;
    TAKE_NEXT_MOVE?: string;
    FINISH_GAME_CONFIRMATION?: string
    UNSAVED_VALUES?: string;
    CONFIRM_FREEZE?: string;
    CONFIRM_RESET?: string;
    CONFIRM_SIGNOUT?: string;
    LOGOUT?: string;
    PASSWORD_UPDATED?: string;
    MISSING_USER_ID?: string;
    INVALID_USER_ID?: string;
    ROLE_NOT_FOUND?: string;
    WRONG_PASSWORD?: string;
    MISSING_CREDENTIALS?: string;
    ALREADY_LOGIN?: string;
    TARGET_NOT_AVAILABLE?: string;
    PLAYERS_REQUIRED?: string;
    GAME_SHARED?: string;
    MESSAGE_SENT?: string;
    COACH_ASSIGNED?: string;
    ADVICES_CREATE_MESSAGE?: string;
    ADVICE_LIMIT_REACHED?: string;
    GAME_COMPLETED?: string
    GAME_INITIALIZED?: string;
    GAME_STARTED?: string;
    GAME_SAVED?: string;
    GAME_ALREADY_ACTIVE?: string;
    ACTION_REVERSED?: string;
    ACTION_TAKEN?: string;
    ACTION_LIMIT_REACHED?: string;
    DELEGATED_ACTION_NOT_ALLOWED?: string;
    CURRENT_ACTION_IS_NOT_DELEGATABLE?: string;
    SAVE?: string;
    UPDATE?: string;
    NOT_MEETING_TARGETED_SALES?: string;
    NOT_MEETING_TARGETED_PAT?: string;
    BUDGETED_VALUE_WRONG?: string;
    NEXT_MOVE_PROCESSED?: string;
    DISCARD_MESSAGE?: string;
    INTERNAL_SERVER_ERROR?: string;
    ABANDON_GAME_CONFIRMATION?: string;
    REVIEW_BUDGET?: string;
    ABANDON_GAMES_IN_NOTIFICATION?: string;
    AWAITING_MENTOR_REQUEST?: string
    FEATURE_NOT_AVAILABLE?: string
}

export interface IContentType {
    ACTION_CATEGORY: string,
    ACTION_SEARCH_KEY: string,
    ACTION_SUB_CATEGORY: string,
    ACTION_TAG_BLOCK_1: string,
    ACTION_TAG_BLOCK_2: string,
    ACTION_TAG_BLOCK_3: string,
    ACTION_TAG_BLOCK_4: string,
    DIMENSION_VALUE: string,
    EVENT: string,
    GAME_STATUS: string,
    PARAMETER: string,
    VIRTUAL_ADVICE: string,
    YEAR: string,
    MONTH: string,
    QUARTER: string
}