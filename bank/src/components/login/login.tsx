import * as React from 'react';

// loginActionCreator from '../../actions/login/loginactioncreator';
import loginStore from '../../store/login/loginstore';
import actions from '../../actions/base/actiontypes';
import { IConfirmationModalData } from '../../interface/interface';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
// import ProgressBar from '../utility/progressbar';

// import utilitystore from '../../store/utility/utilitystore'
import constants from '../../constants/constant';
// import authentication from '../../api/authentication';
import storageManager from '../../store/base/sessionstoremanager';


export default class Login extends React.Component<any, any> {
    constructor() {
        super();
        this.state = {
            userRoles: [],
            selectedRoleId: ''
        }
    }
    refs: any;
    private isUserRemembered: boolean = false;
    private userInfo: any = {
        username: '',
        password: '',
        role: ''
    }
    componentDidMount() {
        loginStore.addChangeListener(actions.LOGIN, this.postLogin);
        // let isUserRemember: string = CookieManager.getCookie('isUserRemember');
        // if (isUserRemember === 'true') {
        //     let userName = CookieManager.getCookie('rememberedUserName');
        //     // this.refs.rememberMe.checked = true;
        //     this.refs.usernameInput.value = userName;
        // } else {
        //     this.refs.usernameInput.value = '';
        // }
    };

    componentWillUnmount() {
        
        
    }

    componentWillMount() {
        let userInfo = storageManager.getValue('userInfo');
        if (userInfo && userInfo.id) {
            //userActionCreator.userSignOut();
        }

        window.onbeforeunload = null;
    }


    private postLogin = (userInfo: any) => {
       

    }



    /**loginClick method fires on login button click */
    private loginClick = () => {
        

    };

    private rememberMe = () => {

    
    }

    /**inputOnKeyUp method fires on Enter key press */
    private inputOnKeyUp = (event: React.KeyboardEvent) => {
        if (event.keyCode == 13) {
            this.loginClick();
        }
    }

    

    render() {
        //login page without splitting header n footer
        return (
            <div>QR Login Page</div>

            
        )
    }

}


