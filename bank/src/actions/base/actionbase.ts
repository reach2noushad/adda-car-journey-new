import dispatcher from '../../app/dispatcher';

interface Action {
    actionType: string;
    data: {};
}
export default class ActionBase {

    protected dispatchAction(actionType: string, data?: any): void {
        let payload: Action = {
            actionType: actionType,
            data: data
        }
        dispatcher.dispatchAction(payload);
    }
}