import actions from '../base/actiontypes';
import ActionBase from '../base/actionbase';

/**
 * Utility action creator helper class
 */

interface Action {
    actionType: string;
    data: {};
}
class UtilityActionCreator extends ActionBase {
    /**UtilityActionCreator */
    updateProgress(percentComplete: number): void {
        this.dispatchAction(actions.UPDATE_PROGESSBAR, percentComplete);
    }

    gotoBackPage(): void {
        this.dispatchAction(actions.GOTO_PREVIOUSPAGE);
    }

    showProgress(): void {
        this.dispatchAction(actions.SHOW_PROGESSBAR);
    }

    HideProgress(text: any): void {
        this.dispatchAction(actions.HIDE_PROGESSBAR, text);
    }

    showLoading(value: boolean): void {
        this.dispatchAction(actions.SHOW_LOADING, value);
    }
    showResumeButton(): void {
        this.dispatchAction(actions.SHOW_RESUME_BUTTON);
    }

    removeMenuActive(): void {
        this.dispatchAction(actions.REMOVE_MENU_ACTIVE);
    }
    makeMenuActive(menuItem: any): void {
        this.dispatchAction(actions.MAKE_MENU_ACTIVE,menuItem);
    }
}

export default new UtilityActionCreator();