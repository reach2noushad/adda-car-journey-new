import actions from '../base/actiontypes';
import ActionBase from '../base/actionbase';
// import Authentication from '../../api/authentication';
import { IConfirmationModalData } from '../../interface/interface';
/**
 * Login action creator helper class
 */

interface Action {
    actionType: string;
    data: {};
}
class ModalPopupActionCreator extends ActionBase {
    /**login action */
    openModal(Component: any, hasCloseButton?: boolean, modalSize?: any): void {
        let componentDetail = {
            Component: Component,
            hasCloseButton: hasCloseButton === false ? hasCloseButton : true,
            modalSize: modalSize
        }
        this.dispatchAction(actions.OPEN_MODAL, componentDetail);
    }

    closeModal(): void {
        this.dispatchAction(actions.CLOSE_MODAL);
    }

    openConfirmBox(text: IConfirmationModalData): void {
        this.dispatchAction(actions.OPEN_CONFIRM, text);
    }
    openDeleteConfirmBox(text: IConfirmationModalData): void {
        this.dispatchAction(actions.OPEN_DELETE_CONFIRM, text);
    }
    openSignoutBox(text: IConfirmationModalData): void {
        this.dispatchAction(actions.OPEN_SIGNOUT, text);
    }

    closeConfirmBox(): void {
        this.dispatchAction(actions.CLOSE_CONFIRM);
    }

    openAlertBox(text: IConfirmationModalData): void {
        this.dispatchAction(actions.OPEN_ALERT, text);
    }

    onModalClose(): void {
        this.dispatchAction(actions.ON_MODAL_CLOSE);
    }

    openNotesModal() {
        this.dispatchAction(actions.OPEN_NOTES_MODAL);
    }
}

export default new ModalPopupActionCreator();