import tutorialKeys from './tutorialKeys'

module constants {
    export const ROUTES = {
        BASE: '/',
        LOGIN: '/login',
        ADMIN: '/admin',
        PLAYER: '/player',
        COACH: '/coach',
        USERHOME: '/home',
        DASHBOARD: 'game',
        PLAYER_DASHBOARD: '/player_dashboard',
        TARGERTS: 'targetsandachievements',
        RULES: 'gamerules',
        FINANCIALS: 'info/financials',
        BUDGETS: 'info/budgets',
        INVESTOR_SENTIMENT: 'info/investorsentiment2',
        PRODUCTS: 'info/products2',
        MARKETS: 'info/marketsandcompetition',
        CLIENT_RELATIONS: 'info/clientrelationships',
        PIPELINE: 'info/businesspipeline',
        PEOPLE: 'info/people',
        GEOPOLITICAL_CONDITIONS: 'info/geopoliticalconditions2',
        COMPANY: 'info/companydetails',
        REVIEW: 'review',
        PROFILE: 'profile',
        TRENDS: 'trends',
        NOTES: 'addnotes',
        MOST_PEOPLE_DO: '/advice/whatwouldmostpeopledo',
        VIRTUAL_ADVICE: '/advice/askvirtualadvice',
        ASK_COACH: '/advice/askcoach',
        TAKE_ACTION: 'action',
        REVIEW_MENTEE: 'reviewmentees',
        DYNAMIC_COMPONENT: 'info/:dynamic',
        /*****Admin routes */
        SERIES: 'series',
        USER_SERIES: 'userseries',
        CLONE_SERIES: 'cloneseries',
        SOLO_PLAYER: 'soloplayer',
        EVENTS: 'admin/events',
        CUSTOMEVENTS: 'admin/customevents',
        CREATE_COMPETENCY: 'admin/createcompetency',
        EVENTOUTCOMES: 'admin/eventoutcomes',
        EVENTPROBABILITY: 'admin/eventprobability',
        TRIGGEREDNOTIFICATIONS: 'admin/triggerednotifications',
        TRIGGEREDNOTIFICATIONS_ACTIONS: 'admin/actiontriggerednotifications',
        ADMINVIRTUALADVICES: 'admin/virtualadvices',
        ADDPLAYER: 'admin/addplayer',
        ADMIN_PROFILE: 'admin/profile',
        MANAGE_USER: 'admin/manageuser',
        CONTENT_VERIFICATION: 'admin/contentverification',
        AUTHORING_TARGETS: 'admin/authoringtargets',
        AUTHORING_COMPETENCY: 'admin/authoringcompetency',

        ACTIONSANDCONSEQUESNCES: 'admin/actionsandconsequences',
        CUSTOMACTIONS: 'admin/customactions',
        ACTION_CONSEQUENCES: 'admin/consequences',
        EVENTACTIONMAPPING: 'admin/eventactionmapping',
        DEPENDENTEVENTS: 'admin/dependentevents',
        EVENT_TRIGGEREDEVENTS: 'admin/triggeredevents-event',
        ACTION_TRIGGEREDEVENTS: 'admin/triggeredevents-action',
        ACTIONTAG_MAPS: 'admin/action-tag-maps',
        PARAMETERS: 'admin/parameters',
        GAME_RULES: 'admin/setupgamerules',
        MAIN_CONTAINTER: 'game',
        SERIES_EDIT: 'admin/editseries',
        MULTIPLAYER_LANDING: 'home/multiplayer',
        SOLO_GAME_LANDING: 'home/sologame',
        COACH_LANDING: 'home/coach-series',
        EVENT_RECOMMENDATIONS: 'home/my-advice',
        REVIEW_MENTEES: 'home/review-mentees',
        REPORT_PAGE: 'home/report',
        REPORT_FRONT_PAGE: 'home/reportlist',
        MANAGE_USERS: 'home/manage-user',
        MANAGE_GROUPS: 'home/managegroups',
        HOME_PROFILE: 'home/profile',
        PRINT_REPORT: '/printreport',
        MESSAGE_COACH: 'home/message-coach',
        PARAMETER_ALERTS: 'admin/parameter-alerts',
        ACTION_EVENT_MAP: 'admin/actioneventmapping',
        TIMELINE_INTIAL_NOTIFICATIONS: 'admin/timeline-notifications',
        GAME_REPORT_TRENDS: 'home/gamereporttrends',
        GAME_REPORT_TRENDS_PRINT_VIEW: 'home/gamereporttrends-print',
        PUBLISH_SERIES: 'admin/publish-series',
        COACH_REVIEW: 'coach/review',
        COACH_INBOX: 'coach/inbox',
        MANAGE_MENTEES: 'coach/manage-mentees',
        PLAYER_REPORTS: 'home/playerreports',
        VIEW_PLAYER_REPORTS: 'home/view-player-reports',
        OPERATIONAL_DASHBOARD: 'home/operational-dashboard',
        VIEW_COMBINED_COMPETENCY_REPORTS: 'home/competency-reports',
        NOTIFICATION_MANAGER: 'home/notification-manager',
        SYSTEM_NOTIFICATIONS: 'home/system-notifications',
        CUSTOM_ACTION_REPORT: 'home/customactionsreport',
        REPORTS_LANDING: 'home/reportslanding'

    };
    export const INFO_BUTTIONS_OBJ_NAME = 'Dashboard';
    export const INFO_BUTTIONS_OBJ_NAME_OWNWRSHIP = 'Ownership';

    export const PAGES = {
        MAINMENULIST: {
            DASHBOARD: 'Dashboard',
            TARGET_AND_ACHIEVEMENTS: 'Targets',
            VIEW_GAME_RULES: 'View Setup',
            // COMPANY_DETAILS: 'COMPANY DETAILS',
            PLAYER_DETAILS: 'Player Details',
            MANAGE_USER: 'Manage User',
            VIEW_PLAYER_DETAILS: 'Profile',
            SIGN_OUT: 'Log Out',
            COACH_MESSAGEBOX: 'Inbox',
            USER_MANUAL: 'User Manual',
            REVIEW_GAME: 'Review Game',
            EXIT_REVIEW: 'Exit Review',
            ADD_NOTE: 'ADD_NOTE',
            REVIEW_MENTEES: 'Review Mentees',
            TIMELINE: 'Timeline',
            BUDGETS: 'Budget',
            TREND_REPORTS: 'Trends',
            COMPANY_INFO: 'CompanyInfo',
            ALERTS: 'Alerts',
            SHARE_GAME: 'Share Game',
            PASSWORD_CHANGE: 'Change Password',
            OPERATING_PLAN: 'OperatingPlan',
            USERHOME: 'Back to Dashboard',
            VIEW_GROUP_MEMBERS: 'View Group Members',
            REPORT_BUG: 'Report Bug',
            PUBLISH_SERIES: 'Publish Series',
            OWNERSHIP: 'Ownership',
            COMPANY_STRUCTURE: 'CompanyStructure',
        },
        SUBMENULIST: {
            FINANCIALS: 'FINANCIALS',
            BUDGETS_AND_TARGETS: 'BUDGETS',
            INVESTOR_SENTIMENT: 'INVESTOR SENTIMENT',
            PRODUCT_SERVICES: 'PRODUCTS AND SERVICES',
            TRENDS_AND_ANALYSIS: 'TRENDS AND ANALYSIS',
            GEOPOLITICAL_RISK: 'GEOPOLITICAL CONDITIONS',
            PEOPLE: 'PEOPLE',
            BUSINESS_PIPELINE: 'BUSINESS PIPELINE',
            MARKET_COMPETITION: 'MARKET AND COMPETITION',
            CLIENT_RELATIONSHIP: 'CLIENT RELATIONSHIPS',
            MOST_PEOPLE_DO: 'WHAT WOULD MOST PEOPLE DO',
            ASK_VIRTUAL_ADVICE: 'ASK VIRTUAL ADVICE',
            ASK_COACH: 'ASK YOUR COACH/MENTOR',
            TAKE_ACTION: 'ACTION',
            DYNAMIC_COMPONENT: 'Dynamic'
        },
        CODE: {
            FINANCIALS: 'FINANCIALS',
            BUDGETS_AND_TARGETS: 'BUDGETS_AND_TARGETS',
            INVESTOR_SENTIMENT: 'INVESTOR_SENTIMENT',
            PRODUCT_SERVICES: 'PRODUCT_SERVICES',
            COMPANY_DETAILS: 'COMPANY_DETAILS',
            GEOPOLITICAL_RISK: 'GEOPOLITICAL_RISK',
            PEOPLE: 'PEOPLE',
            BUSINESS_PIPELINE: 'BUSINESS_PIPELINE',
            MARKET_COMPETITION: 'MARKET_COMPETITION',
            CLIENT_RELATIONSHIP: 'CLIENT_RELATIONSHIP',
        }

    }

    export const USERS = {
        PLAYER: 'Player',
        //ADMIN: 'TenantAdmin',
        COACH: 'Coach',
        ADMIN: 'Author',
        GLOBAL_ADMIN: 'Administrator',
        SOLO: 'SOLO',
        MULTIPLAYER: 'MULTIPLAYER'
    }

    export const DIMENSIONS = {
        INDUSTRYVERTICALS: 'INDUSTRYVERTICALS',
        PRODUCTSERVICES: 'PRODUCTSERVICES',
        GEOGRAPHY: 'GEOGRAPHY',
        SOLUTIONAREA: 'SOLUTIONAREA'
    }


    export const PEOPLE_COLUMNS = {
        PERCENT_EMPLOYEES: 'EDF1',
        PERCENT_CB_COST: 'FCS1',
        PERCENT_ATTRITION: 'ESG1',
        EMP_SATISFACTION: 'ESS1'
    }

    export const PEOPLE_DROPDOWN = {
        FUNCTION: 'FUNCTION',
        LEVEL: 'LEVEL'
    }

    // export const GAMESERIESID = 'f53dcc77-d184-4b41-b559-e560f98b00cb'          /* Dev */
    // export const GAMESERIESID = '5d3d5ece-c7b5-4588-97ae-c12dd476218e';       /* Stage */
    export const GAMESERIESID = '';

    //export const GAMEEVENTID = 'af353700-ff47-11e6-909e-adbf62024fde'
    export const GAMEEVENTID = '060d8504-9bdc-411d-8a09-7e2d35775542';

    export const PARAMETER_COUNT = 5;

    export const SELECT_VALUE = '0';

    export const SORT_CONDITION = {
        ASCENDING: 'asc',
        DESCENDING: 'desc',
    }
    export const FILTER_CONDITION = {
        ALL: 'all',
        READ: 'read',
        UNREAD: 'unread'
    }

    export const NOTE_TYPE = {
        TEXT: 'text',
        AUDIO: 'audio',
        VIDEO: 'video'
    }

    export const PAGE_VIEW_TYPE = {
        FULL_PAGE: 'fullpage',
        TAB_VIEW: 'tabview'
    }
    export const ANALYSIS_REPORT_TYPE = {
        GAME_SUMMARY: 'game_summary',
        GAME_STATUS: 'status',
        TIME_SPENT: 'time_spent',
        SITUATIONAL_CHOICE: 'situational_choice',
        GROUP_RECOMMENDATION_SUMMARY: 'group_recommendations_summary',
        LEADER_PROFILE: 'leader_profile',
        COMPETENCY_TYPE: 'competency_type',
        TARGETS_PER_MILESTONE: 'targets_per_milestone',
        DATA_PRIORITISATION: 'parameter_groups_prioritization',
        BUSINESS_OBJECTIVES: 'action_tag_prioritization',
        TARGET_AND_ACHIEVEMENTS: 'targets_and_achievements'

    }

    export const GAME_STATUS = {
        NOTSTARTED: 'NotStarted',
        INPROGRESS: 'InProgress',
        PAUSED: 'Paused',
        SAVED: 'Saved',
        GAMEOVER: 'GameOver',
        COMPLETED: 'Completed',
        ABANDONED: 'Abandoned',
        STALE: 'Stale'
    }

    export const USER_STATUS = {
        ACTIVE: 'Active',
        INACTIVE: 'InActive',
        PAUSED: 'Paused'
    }

    export const BUDGET_MODE = {
        FIRSTMONTHOFEVERYEAR: 'First Month Of EveryYear',
        EVERYQUARTER: 'Every Quarter',
        NORESTRICTIONS: 'No Restrictions'
    }

    export const GAMESTAGEDURATION = {
        MONTH: 'Month',
        QUARTER: 'Quarter'
    }

    export const AUDIT_MAIN_PAGES = {
        NOTES: 'Notes',
        INFORMATION: 'Information',
        PEOPLE: 'People',
        GEO_POLITICAL_RISK: 'GeoPoliticalRisk',
        TRENDS: 'Trends',
        PRODUCTS_AND_SERVICE: 'ProductAndServices',
        FINANCIALS: 'Financials',
        CLIENT_RELATIONSHIP: 'ClientRelationship',
        INVESTOR_SENTIMENT: 'InvestorSentiment',
        PIPELINE: 'Pipeline',
        MARKET_AND_COMPETITION: 'MarketAndCompetition',
        BUDGETS_AND_TARGETS: 'BudgetsAndTargets',
        MOVE: 'Move',
        ALERTS: 'Alerts',
        ADVICE: 'Advice',
        ASK_YOUR_COACH: 'AskYourCoach',
        MOST_PEOPLE_DO: 'WhatWouldMostPeopleDo',
        VIRTUAL_ADVICE: 'VirtualAdvice',
        ASK_YOUR_GROUP: 'AskYourGroup',
        SHARE_GAME: 'ShareGame',
        DASHBOARD: 'Dashboard',
        ACTION: 'Action',
        TUTORIAL: 'Tutorial',
        COMPANY_DETAILS: 'CompanyDetails',
        DASHBOARD_ITEMS: 'DashboardItems',
        HISTORY: 'History',
        RULES: 'Rules',
        REVIEW_LESSON: 'ReviewLesson',
        TARGETS: 'TargetsAndAchievements',
        DYNAMIC_INFO_ITEMS: 'DynamicInfoItem',
        TIMELINE_ITEMS: 'TimelineItems',
        USER: 'User',
        ADVICE_REQUESTS: 'AdviceRequests',
        OPERATING_PLAN: 'OperatingPlan',
        CHAT_WINDOW: 'ChatWindow',
        WIDGETS: 'Widgets',
        EVENT: 'Event'
    }
    export const USER_SUBCATEGORY = {
        GameLogin: 'GameLogin',
        GameLogout: 'GameLogout',
        ProfileClick: 'ProfileClick',
        ProfileUpdate: 'ProfileUpdate'
    }

    export const TIMELINE_ITEMS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        NotificationRecieved: 'NotificationRecieved',
        WhatsHappening: 'WhatsHappening',
        IgnoreEvent: 'IgnoreEvent',
        EventCollapse: 'EventCollapse',
        EventReadMore: 'EventReadMore'
    }

    export const CHAT_WINDOW_SUBCATEGORY = {
        OnFocusOut: 'OnFocusOut',
        OnFocusIn: 'OnFocusIn',
    }

    export const WIDGETS_SUBCATEOGRY = {
        ReOrderClick: 'ReOrderClick',
        ReOrderSave: 'ReOrderSave',
    }

    export const DYNAMIC_INFO_ITEMS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const NOTES_SUBCATEGORY = {
        SaveTextNote: 'SaveTextNote',
        SaveVideoNote: 'SaveVideoNote',
        SaveAudioNote: 'SaveAudioNote'
    }

    export const PEOPLE_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const GEO_POLITICAL_RISK_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const TRENDS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        ShowReportBudgetTrend: 'ShowReportBudgetTrend',
        ShowReportTrend: 'ShowReportTrend',
        ShowReportTargetTrend: 'ShowReportTargetTrend',
        SaveTrendReport: 'SaveTrendReport',
        RemoveSavedTrendReport: 'RemoveSavedTrendReport'
    }

    export const PRODUCTS_AND_SERVICE_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const FINANCIALS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        DrillDownFirstLevel: 'DrillDownFirstLevel',
        DrillDownSecondLevel: 'DrillDownSecondLevel',
        DrillDownThirdLevel: 'DrillDownThirdLevel',
        DrillDownFourthLevel: 'DrillDownFourthLevel',
        GroupDrillDown: 'GroupDrillDown'
    }

    export const CLIENT_RELATIONSHIP_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const INVESTOR_SENTIMENT_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage'
    }

    export const PIPELINE_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const MARKET_AND_COMPETITION_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const BUDGETS_AND_TARGETS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        DrillDownFirstLevel: 'DrillDownFirstLevel',
        DrillDownSecondLevel: 'DrillDownSecondLevel',
        DrillDownThirdLevel: 'DrillDownThirdLevel',
        DrillDownFourthLevel: 'DrillDownFourthLevel',
        GroupDrillDown: 'GroupDrillDown',
        FreezeBudget: 'FreezeBudget',
        SaveBudget: 'SaveBudget',
        BudgetExceeded: 'BudgetExceeded',
        AutoCorrectBudget: 'AutoCorrectBudget'
    }

    export const MOVE_SUBCATEGORY = {
        OnMoveCompletion: 'OnMoveCompletion'
    }

    export const ADVICE_REQUESTS_CATEGORY = {
        IgnoreEvent: 'IgnoreEvent',
        ViewGameParameterGroupInfo: 'ViewGameParameterGroupInfo',
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        SuggestAction: 'SuggestAction',
        ViewGameInfo: 'ViewGameInfo',
        HideGameInfo: 'HideGameInfo',
    }

    export const OPERATING_PLAN_CATEOGORY = {
        CreatePlan: 'CreatePlan',
        UpdatePlan: 'UpdatePlan',
        CreatePlanHomepage: 'CreatePlanHomepage',
        LinkBudgetParameter: 'LinkBudgetParameter',
        DeletePlan: 'DeletePlan',
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        FreezeOperatingPlan: 'FreezeOperatingPlan',
    }

    export const ALERT_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        AlertCreated: 'AlertCreated',
        AlertUpdated: 'AlertUpdated',
        AlertDeleted: 'AlertDeleted'
    }

    export const ASK_YOUR_COACH_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        MessageSend: 'MessageSend',
        MessageReadOrUnread: 'MessageReadOrUnread',
        MessageFlagOrNot: 'MessageFlagOrNot',
        MessageReplied: 'MessageReplied',
        MessageRecieved: 'MessageRecieved',
        MessageSort: 'MessageSort'
    }

    export const MOST_PEOPLE_DO_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnSearch: 'OnSearch',
        TakeAction: 'TakeAction'
    }

    export const VIRTUAL_ADVICE_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange',
        TakeAction: 'TakeAction'
    }

    export const ASK_GROUP_SUBCATEGORY = {
        SendRequest: 'SendRequest',
        DeclineRecomendation: 'DeclineRecomendation',
        ViewRecommendation: 'ViewRecommendation',
        AcceptRecomendation: 'AcceptRecomendation',
        HideRecommendation: 'HideRecommendation',
        SendRequestAll: 'SendRequestAll',
        SendRecommendationHome: 'SendRecommendationHome',
        RecommendationReceivedHome: 'RecommendationReceivedHome',
    }

    export const SHARE_GAME_SUBCATEGORY = {
        ShareGameRecieved: 'ShareGameRecieved',
        ShareGameSent: 'ShareGameSent',
        ShareGameView: 'ShareGameView'
    }

    export const DASHBOARDITEMS_SUBCATEGORY = {
        PauseGame: 'PauseGame',
        ContinueGame: 'ContinueGame'
    }

    export const REVIEW_LESSON_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage'
    }

    export const HISTORY_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        OnDopdownChange: 'OnDropdownChange'
    }

    export const RULES_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage'
    }


    export const TARGETS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage'
    }

    export const ACTION_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage',
        SearchAction: 'SearchAction',
        TakeAction: 'TakeAction',
        ReverseAction_Current: 'ReverseAction_Current',
        ReverseAction_Previous: 'ReverseAction_Previous'
    }

    export const TUTORIAL_SUBCATEGORY = {
        TutorialSkipToEnd: 'TutorialSkipToEnd',
        TutorialUserStart: 'TutorialUserStart',
        TutorialBack: 'TutorialBack',
        TutorialAutoStart: 'TutorialAutoStart',
        TutorialComplete: 'TutorialComplete',
        TutorialNext: 'TutorialNext'
    }

    export const COMPANY_DETAILS_SUBCATEGORY = {
        OnSubCategoryHomePage: 'OnSubCategoryHomePage'
    }

    export const DELEGATE_EVENT_SUBCATEGORY = {
        EventDelegationRevoked: 'EventDelegationRevoked',
        EventDelegationCreated: 'EventDelegationCreated',
        EventDelegationAccepted: 'EventDelegationAccepted',
        EventDelegationRejected: 'EventDelegationRejected'
    }

    export const GAME_TIMELIMIT = {
        OVERALLGAME: '1',
        PERMOVE: '2',
        PERMOVEANDOVERALL: '3',
        NOLIMITS: '0'
    }

    export const NOTIFICATION_TYPE = {
        GAME_SHARED: 'GameShared',
        MESSAGE_SENT: 'MessageSent',
        THRESHOLD_ALERT: 'CategoryThresholdAlert',
        MENTOR_REQUEST_RECEVIED: 'AssignMentorRequestReceived',
        MENTOR_REQUEST_REJECTED: 'AssignMentorRequestRejected',
        MENTOR_REQUEST_ACCEPTED: 'AssignMentorRequestAccepted',
        MENTEE_REMOVED: 'AssignedMenteeRemoved',
        ANALYTICS_REPORT_GENERATED: 'GameAnalyticsReportGenerated',
        GAME_EVENT_DELEGATED: 'GameEventDelegated',
        GAME_QUERY_RECEIVED: 'GameQueryReceived'
    }

    export const MOVE_DURATION = {
        MONTH: 'Month',
        QUARTER: 'Quarter'
    }

    export const ADVICE_MAX_LIMIT = 5

    export const PASSWORD_CHANGE = {
        WRONGPASSWORD: 'wrongPassword',
        MISMATCH: 'passwordMismatch'
    }

    export const SOCKET_RECONNECT_COUNT = 5

    export const AUDIT_CONTENT_TYPE_KEY = {
        ACTION_CATEGORY: 'ACTION_CATEGORY',
        ACTION_SEARCH_KEY: 'ACTION_SEARCH_KEY',
        ACTION_SUB_CATEGORY: 'ACTION_SUB_CATEGORY',
        DIMENSION_VALUE: 'DIMENSION_VALUE',
        EVENT: 'EVENT',
        GAME_STATUS: 'GAME_STATUS',
        PARAMETER: 'PARAMETER',
        VIRTUAL_ADVICE: 'VIRTUAL_ADVICE',
        YEAR: 'YEAR',
        MONTH: 'MONTH',
        QUARTER: 'QUARTER'
    }

    export const HAVE_PLAYERS_ASSIGNED = 'HavePlayersAssigned'
    export const HAVE_INITIALIZED_GAMES = 'HaveInitializedGames'

    export const TEMPLATE_TYPE = {
        STATIC: 'Static',
        CHART: 'Chart',
        SCALE: 'Scale',
    }
    export const CHART_TYPE = {
        PIE: 'Pie',
        BAR: 'Bar'
    }
    export const REPLAY_SCREEN_NAVIGATION = {
        NEXT: 'NEXT',
        PREVIOUS: 'PREVIOUS'
    }

    export const VIEW_TYPE = {
        VIEW: 'view',
        EDIT: 'edit'
    }

    export const EVENT_LIST_TYPE = {
        MASTER_EVENTS: 'master',
        SERIES_EVENTS: 'series'
    }

    export const PARAMETER_LIST_TYPE = {
        MASTER_EVENTS: 'master',
        SERIES_EVENTS: 'series'
    }

    export const PARAMETER_GROUP_NAME = {
        BUDGET: 'Manage Budget',
        TARGETS: 'Targets & Performance'
    }

    export const PROFIlE_CARDS = {
        ABOUT: 'about',
        COMPANY: 'comapny',
        EXPERIENCE: 'experience',
        MISCELLANIOUS: 'miscellanious'
    }

    export const TIMELINE_CATEGORY = {
        EVENT: 'Event',
        ACTION: 'Action',
        NOTIFICATION: 'Notification',
        NOTE: 'Note'
    }

    export const CONDITIONAL_OPERATOR_DEFENITION: any = {
        "<": "Less than",
        ">": "Greater than",
        ">=": "Greater than or equal to",
        "<=": "Less than or equal to"
    }

    export const NOTIFICATION_RULE = {
        ENABLE_PARAMETER_ALERTS: 'EnableParameterAlertNotifications',
        DISABLE_PARAMETER_ALERTS: 'DisableParameterAlerts'
    }

    export const TUTORIALS = {
        TUTORIAL_DASHBOARD: 'TUTORIAL_DASHBOARD',
        TUTORIAL_TRENDS: 'TUTORIAL_TRENDS',
        TUTORIAL_BUDGET: 'TUTORIAL_BUDGET',
        TUTORIAL_ALERTS: 'TUTORIAL_ALERTS',
        TUTORIAL_OP_PLAN: 'TUTORIAL_OP_PLAN',
        TUTORIAL_CREATE_PLAN: 'TUTORIAL_CREATE_PLAN',
        TUTORIAL_OP_PLAN_AFTER_CREATE: 'TUTORIAL_OP_PLAN_AFTER_CREATE',
        TUTORIAL_SPECTATOR: 'TUTORIAL_SPECTATOR',
    }
    export const TUTORIAL_Keys = tutorialKeys.Keys;
    export const TUTORIAL_LOCALE = tutorialKeys.Locale;

    export const PARAMETER_TARGET_TENURE_TYPE = {
        Every_Year: 0,
        Every_Quarter: 1,
        Every_Month: 5,
        Specific_Year: 2,
        Specific_Quarter: 3,
        Specific_Month: 4
        // dummy: 4, //not using
    }

    export const CONTENT_VERIFICATION_TYPE: any = {
        "0": "Absolute value",
        "1": "Absolute value",
        "2": "Consequence dimension value",
        "3": "Dimension value",
        "4": "Outcome dimension value"
    }

    export const IDLE_TIME: number = 120000
    export const TARGET_ACTION = {
        CREATE: "create",
        UPDATE: "update",
    }

};


export default constants;