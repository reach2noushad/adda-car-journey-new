import * as React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory, IndexRoute, browserHistory } from 'react-router';
import Base from './base'
import Login from '../components/login/login';
import constants from '../constants/constant'

require('../../assets/less/styles.less');
// require('../../assets/js/jquery-1.7.2.min.js');
require('../../assets/js/jquery.js');
setTimeout(() => {
    require('../../assets/js/jquery-ui.min.1.8.23.js');
    require('../../assets/js/jquery.ui.touch-punch.min.js');
    require('../../assets/js/jquery.flot.js');
    require('../../assets/js/jquery.flot.selection.js');

    // jQuery.curCSS= function(element:any, prop:any, val:any) {
    //      return jQuery(element).css(prop, val);
    // }
    // require('../../assets/js/jquery.appear.js');
}, 500)


render((
    <Router history={hashHistory}>
        <Route path={constants.ROUTES.BASE} component={Base}>
            <IndexRoute component={Login} />


        </Route>
            
    </Router >
), document.getElementById('app'));


document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
});
document.addEventListener('gesturechange', function (e) {
    e.preventDefault();
});
document.addEventListener('gestureend', function (e) {
    e.preventDefault();
});



// $(document).ajaxStop(function () {
//     utilityaction.showLoading(false);
//     if (location.hash.indexOf('printreport') > 0) {
//         setTimeout(() => {
//             // window.print();
//             document.getElementById('printBtn').style.display = 'block';
//         }, 250)
//     }
// });



