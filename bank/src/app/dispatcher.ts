import { Dispatcher } from 'flux';
import userStore from '../store/users/userstore';

class AppDispatcher extends Dispatcher<any> {
    /**
   * A bridge function between the views and the dispatcher, marking the action
   * as a view action.  Another variant here could be handleServerAction.
   * @param  {object} action The data coming from the view.
   */
    dispatchAction(action: any) {
        setTimeout(() => {
            this.dispatch({
                source: 'VIEW_ACTION',
                action: action
            });
        }, 100);
    }
};
export default new AppDispatcher();

