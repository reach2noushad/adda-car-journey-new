
import AppDispatcher from '../../app/dispatcher';
import storeBase from '../base/storebase';
import actions from '../../actions/base/actiontypes';
import storageManager from '../base/sessionstoremanager';
import { IDialogues } from '../../interface/interface'
class LoginStore extends storeBase {
    constructor() {
        super();
        this.dispatchToken = AppDispatcher.register(this.handleActions);
    }
    //sets loggedin user info.for the time being it is setting only the role vale
    private loggedInUserInfo: any;
    private _userRole: any;
    private _userId: any;
    private userInfo: any;
    private _socketStatus: boolean;
    private _userLoginDetails: any;
    private _dialogues: IDialogues
    private _roleMaps:any;
    handleActions = (payload: any) => {

        let action = payload.action;
        switch (action.actionType) {
            case actions.LOGIN:
                this.setloggedInUser(action.data);
                this.emitChange(action.actionType, action.data);
                break;
            case actions.DIALOGUES_IN_APPLICATION:
                this.setDialogues(action.data.data);
                break;
        }

        return true; // No errors. Needed by promise in Dispatcher.
    }


    //used for setting the roleID for the time being
    public setloggedInUser(user: any) {
        // this._userRole = user.role;
        this._userId = user.userId;
        storageManager.setValue('userInfo', user);
    }

    public setUserRole(userRole: string) {
        this._userRole = userRole;
        storageManager.setValue('userRole', userRole);
    }
    public setRoleMap(roleMap: any) {
        this._roleMaps = roleMap;
        storageManager.setValue('roleMap', roleMap);
    }
    public get roleMaps() {
        if (this._roleMaps) {
            return this._roleMaps;

        } else {
            return storageManager.getValue('roleMaps')

        }
    }


    public get userRole() {
        if (this._userRole) {
            return this._userRole;

        } else {
            return storageManager.getValue('userRole')

        }
    }

    public get userId() {

        if (this._userId) {
            return this._userId;

        } else {
            return storageManager.getValue('userInfo').userId;
        }
    }

    public setSocketStatus(socketStatus: boolean) {
        this._socketStatus = socketStatus
    }

    public get socketStatus() {
        return this._socketStatus
    }


    public setUserLogginDetails(obj: any) {
        this._userLoginDetails = obj;
    }
    public get userLoginDetails() {
        return this._userLoginDetails;
    }

    private setDialogues(data: any) {
        this._dialogues = data
        storageManager.setValue('appDialogues', data);
    }

    public get dialogues() {        
        if (this._dialogues) {
            return this._dialogues;
        } else {
            return storageManager.getValue('appDialogues');
        }
    }

}

export default new LoginStore();
