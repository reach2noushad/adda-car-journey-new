import AppDispatcher from '../../app/dispatcher';
import storeBase from '../base/storebase';
import actions from '../../actions/base/actiontypes';
// import userStore from '../users/userstore';
// import constants from '../../constants/constant'

class UtilityStore extends storeBase {
    constructor() {
        super();
        this.dispatchToken = AppDispatcher.register(this.handleActions);
    }
    private _pageNavigation: any = [];
    private _activeTab: any;
    handleActions = (payload: any) => {
        let action = payload.action;

        switch (action.actionType) {
            case actions.OPEN_MODAL:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.CLOSE_MODAL:
                this.emitChange(action.actionType);
                break;
            case actions.OPEN_CONFIRM:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.OPEN_DELETE_CONFIRM:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.OPEN_SIGNOUT:
                this.emitChange(action.actionType, action.data);
                break
            case actions.CLOSE_CONFIRM:
                this.emitChange(action.actionType);
                break;
            case actions.OPEN_ALERT:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.CLOSE_ALERT:
                this.emitChange(action.actionType);
                break;
            case actions.ON_MODAL_CLOSE:
                this.emitChange(action.actionType);
                break;
            case actions.UPDATE_PROGESSBAR:
                this.emitChange(action.actionType, action.data);
                break;
            case actions.SHOW_LOADING:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.SHOW_RESUME_BUTTON:
                this.emitChange(action.actionType, action.data)
                break;
            case actions.GOTO_PREVIOUSPAGE:
                this._pageNavigation.pop();
                this.emitChange(action.actionType);
                break;
            case actions.REMOVE_MENU_ACTIVE:
                this.emitChange(action.actionType);
                break;
            case actions.OPEN_NOTES_MODAL:
                this.emitChange(action.actionType)
                break;
            case actions.MAKE_MENU_ACTIVE:
                this.emitChange(action.actionType, action.data)
                break;

        }

        return true; // No errors. Needed by promise in Dispatcher.
    }

    public updatePageNavigation() {
        this._pageNavigation.pop();
    }
    public get previousPage() {
        let prevPageIndex = this._pageNavigation.length > 1 ? this._pageNavigation.length - 2 : 0;
        let prevPage = this._pageNavigation[prevPageIndex];

        if (prevPage.code == 'Dynamic') {
            return prevPage.pageName;
        } else {
            return prevPage.pageName.replace(/_/g, " ");;
        }

    }

    public get currentPage() {
        let prevPage = this._pageNavigation[this._pageNavigation.length - 1];

        if (prevPage.code == 'Dynamic') {
            return prevPage.code;
        } else {
            return prevPage.pageName;
        }
    }

    public get activeTab() {
        return this._activeTab;
    }
    public setActiveTab(tab: any) {
        this._activeTab = tab;
    }



    public setPageNavigation(pageName: any, pageType: any) {
        this._pageNavigation.push(
            {
                pageName: pageName,
                code: pageType
            }
        );
    }

    // public get pageNavigation() {
    //     return this._pageNavigation;
    // }

}

export default new UtilityStore();
