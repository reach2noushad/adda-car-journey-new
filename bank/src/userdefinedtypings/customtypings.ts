﻿
const io: any = 0

declare module "react-bootstrap-typeahead" {
    const Typeahead: any;
    export {
        Typeahead
    }

}


declare module "react-image-cropper" {
    const Cropper: any;
    export {
        Cropper
    }
}

declare module "react-chartjs" {
    const Bar: any;
    const Pie: any;
    const Line: any;

    export {
        Bar, Pie, Line
    }
}
declare module "regression" {
    const Regression: any;
    export {
        Regression
    }
}

declare module "react-google-charts" {
    const Chart: any;
}

declare module "react-js-pagination" {
    const Pagination: any;
    export default Pagination
}
declare module "react-datepicker" {
    const DatePicker: any;
    export default DatePicker
}

// declare module "react-joyride" {
//     const Joyride: any;
//     export {
//         Joyride
//     }
// }
declare module "recharts" {
    const BarChart: any;
    const LineChart: any;
    const Bar: any;
    const Line: any;
    const Brush: any;
    const Cell: any;
    const CartesianGrid: any;
    const ReferenceLine: any;
    const ReferenceDot: any;
    const XAxis: any;
    const YAxis: any;
    const Tooltip: any;
    const Legend: any;
    const ErrorBar: any;
    const ResponsiveContainer: any;
    export {
        BarChart, LineChart, Bar, Line, Brush, Cell, CartesianGrid, ReferenceLine, ReferenceDot,
        XAxis, YAxis, Tooltip, Legend, ErrorBar, ResponsiveContainer
    }
}

