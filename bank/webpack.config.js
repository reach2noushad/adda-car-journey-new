var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: [
        './src/app/app.tsx'
    ],
    resolve: {
        extensions: ['', '.ts', '.tsx', '.js', '.less', '.css']
    },
    devServer: {
        historyApiFallback: true,
    },
    module: {
        loaders: [
           
            {
                test: [/\.ts(x?)$/],
                loaders: ['ts-loader'],
                exclude: path.join(__dirname, 'typings'),
                include: path.join(__dirname, 'src')
            },
            { test: /\.ts$/, loader: "webpack-strip?strip[]=debug,strip[]=console.log" },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader'),
            },
            {
                test: /\.(png|ttf|eot|woff|woff2|svg?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin("bundle.css")
        
    ],
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/dist/'
    },
}
