import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Switch, BrowserRouter } from 'react-router-dom';
import Base from './app/base';
import Login from './components/login/login';
import BlockchainTimeline from './components/timeline/blockchaintimeline';
import "circular-std";
import "typeface-roboto";
import 'font-awesome/css/font-awesome.min.css';
import Home from './components/home/home';
import CarHome from './components/car/carhome';
import history from './history';
import 'bootstrap/dist/css/bootstrap.min.css';
import BaseContainer from './app/basecontainer';
import constants from './constants/constant'

//require('../../assets/less/styles.less');
// require('../../assets/js/jquery-1.7.2.min.js');
require('../assets/js/jquery.js');
setTimeout(() => {
    require('../assets/js/jquery-ui.min.1.8.23.js');
    require('../assets/js/jquery.ui.touch-punch.min.js');
    require('../assets/js/jquery.flot.js');
    require('../assets/js/jquery.flot.selection.js');

    // jQuery.curCSS= function(element:any, prop:any, val:any) {
    //      return jQuery(element).css(prop, val);
    // }
    // require('../../assets/js/jquery.appear.js');
}, 500)
ReactDOM.render((


    // <BrowserRouter history={history}>
    //         <Base/>
    //         <Route exact path="/" component={Login} />
    //         <Route path="/home" component={Home} />
    //         <Route path="/car" component={CarHome} />
    //         <Route path="/timeline" component={BlockchainTimeline} />
    // </BrowserRouter>
    <Router history={history}>

        <Route  >
            
            <Route exact path="/"  component={Base} />
            <Route exact path="/"  component={Login} />
            <Route exact path="/login"  component={Base} />
            <Route exact path="/login"  component={Login} />
            
            <Route >
                <Route exact path="/home" component={BaseContainer} />
                <Route exact path="/home" component={Home} />
                <Route exact path="/car" component={BaseContainer} />
                <Route exact path="/car" component={CarHome} />
                <Route exact path={constants.ROUTES.BLOCKCHAIN_TIMELINE} component={BaseContainer} />
                <Route exact path={constants.ROUTES.BLOCKCHAIN_TIMELINE} component={BlockchainTimeline} />
            </Route>
            {/* <Route path="/home" component={Home} />
            <Route path="/car" component={CarHome} />
            <Route path="/timeline" component={BlockchainTimeline} /> */}
        </Route>

    </Router>
), document.getElementById('app'));