

export default class storageManager {

    public static setValue(key: string, value: any) {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    public static getValue(key: string) {
        if (sessionStorage.getItem(key) !== null && sessionStorage.getItem(key) != "undefined") {
            return JSON.parse(sessionStorage.getItem(key));
        } else {
            return false;
        }
    }

    public static removeValue(key: string) {
        sessionStorage.removeItem(key);
    }

    public static clearStorage() {
        sessionStorage.clear();
    }


    public static setLocalStorageValue(key: string, value: any) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    public static getLocalStorageValue(key: string) {
        if (localStorage.getItem(key) !== null) {
            return JSON.parse(localStorage.getItem(key));
        } else {
            return false;
        }
    }
    public static removeLocalStorageValue(key: string) {
        localStorage.removeItem(key);
    }
    public static clearLocalStorage() {
        localStorage.clear();
    }
}
