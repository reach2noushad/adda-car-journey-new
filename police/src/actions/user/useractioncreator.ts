import ActionBase from '../base/actionbase';
import actions from '../base/actiontypes';
import UserApi from '../../api/userinfoapi';

import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import { IConfirmationModalData } from '../../interface/interface';
import loginStore from '../../store/login/loginstore';

import userStore from '../../store/users/userstore';
import authentication from '../../api/authentication';
import { any } from 'prop-types';

class UserActionCreator extends ActionBase {
    
    /**
     * user logout
    */
    userSignOut = () => {
        this.dispatchAction(actions.LOGOUT,{});
        // UserInfoApi.userLogout((response: any) => {
        //     this.dispatchAction(actions.LOGOUT, response);
        // });
    }
    sendTransaction = (data:any) => {
       
        UserApi.sendTransaction(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_TRANSACTION, response);
            }
            
        });
    }
    checkTransactionStatus=(data:any)=>{
        UserApi.checkTransactionStatus(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.CHECK_TRANSACTION_STATUS, response);
            }
            
        });
    }
    sendCarQuotation=(data:any)=>{
        UserApi.sendCarQuotation(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SEND_CAR_QUOTATION, response);
            }
            
        });
    }
    saveBooking=(data:any)=>{
        UserApi.saveBooking(data,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.SAVE_BOOKING, response);
            }
            
        });
    }
    getAllBookings=(useraddress:any)=>{
        UserApi.getAllBookings(useraddress,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_ALL_BOOKINGS, response);
            }
            
        });
    }
    getBookingStatus=(bookingId:any)=>{
        UserApi.getBookingStatus(bookingId,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.GET_BOOKING_STATUS, response);
            }
            
        });
    }
    blockChainTimeline=(bookingId:any)=>{
        UserApi.blockChainTimeline(bookingId,(response: any) => {
            if(response)
            {
                this.dispatchAction(actions.BLOCKCHAIN_TIMELINE, response);
            }
            
        });
    }
}
export default new UserActionCreator();