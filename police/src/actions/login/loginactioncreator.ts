import actions from '../base/actiontypes';
import ActionBase from '../base/actionbase';
import Authentication from '../../api/authentication';
import { IConfirmationModalData } from '../../interface/interface';
//import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import loginStore from '../../store/login/loginstore';
/**
 * Login action creator helper class
 */

interface Action {
    actionType: string;
    data: {};
}
class LoginActionCreator extends ActionBase {
    getQrCode(loginsessionid:any): void {
        
        Authentication.getQrCode(loginsessionid, (response: any) => {
            if (response) {
                this.dispatchAction(actions.QRCODE, response);
            }
        
        });
    }
    checkLoginStatus(loginsessionid:any): void {
        
        Authentication.checkLoginStatus(loginsessionid, (response: any) => {
            if (response) {
                this.dispatchAction(actions.CHECK_LOGIN_STATUS, response);
            }
        
        });
    }
    
    
}

export default new LoginActionCreator();