import { config } from './config';
import utilityaction from '../../actions/utility/utilityaction'
import storageManager from '../../store/base/sessionstoremanager';
import modalactioncreator from '../../actions/utility/modalpopupaction';
//import RouterBase from '../../components/utility/routerbase';
//import constants from '../../constants/constant'

export default class Promise {
    private xmlHttpRequest: XMLHttpRequest = new XMLHttpRequest();
    private url: string = '';
    private type: string = '';
    private data: {};
    private _token = false;
    //private isAuditCall: boolean = false;
    private _hideLoading: boolean = false;
    private _async: boolean = true;
    /**
    * HTTP GET operation
    * @ param url
    * @ param callback
    */
    protected get = (url: string, callback: Function, hideLoading?: boolean) => {
       
        this._hideLoading = hideLoading ? hideLoading : false;
        this.setParams(url, 'GET', callback);
    }

    /**
    * HTTP POST operation
    * @ param url
    * @ param callback
    */
    protected post = (url: string, callback: Function, data?: {}, hideLoading?: boolean) => {
        let tempaArr = url.split('/');
        //this.isAuditCall = tempaArr[tempaArr.length - 1] == 'audits' ? true : false;
        this._hideLoading = hideLoading ? hideLoading : false;
        this.setParams(url, 'POST', callback, JSON.stringify(data));
    }

    /**
    * HTTP PUT operation
    * @ param url
    * @ param callback
    */
    protected put = (url: string, callback: Function, data: {}, hideLoading?: boolean) => {
        this._hideLoading = hideLoading ? hideLoading : false;
        this.setParams(url, 'PUT', callback, JSON.stringify(data));
    }

    /**
    * HTTP DELETE operation
    * @ param url
    * @ param callback
    */
    protected delete = (url: string, callback: Function, data: {}, hideLoading?: boolean) => {
        this._hideLoading = hideLoading ? hideLoading : false;
        this.setParams(url, 'DELETE', callback, JSON.stringify(data));
    }

    /**
    * HTTP PATCH operation
    * @ param url
    * @ param callback
    * @ param data
    */
    protected patch = (url: string, callback: Function, data: {}, hideLoading?: boolean) => {
        this._hideLoading = hideLoading ? hideLoading : false;
        this.setParams(url, 'PATCH', callback, JSON.stringify(data));
    }

    protected postForm = (url: string, callback: Function, data?: {}) => {
        
        if (!this._token) {
            this._token = storageManager.getValue('userInfo').id;
        }
        $.ajax({
            url: config.baseURL + url,
            //url: url,
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            headers: {
                //'Authorization': storageManager.getValue('userInfo').id
            },
            beforeSend: function (data) {
                
               // utilityaction.showLoading(true);
            },
            success: function (data) {
                return callback(data);

            }, error: (data) => {
                
                //this.handleError(data);
            }
        });
    }

    private fetch = (callback: Function) => {
        //alert(this.url);
        // if (!this._token) {
        //     this._token = storageManager.getValue('userInfo').id;
        // }
        // let authorizationObj: any = {
        //     'Authorization': storageManager.getValue('userInfo').id,
        // }
        
        //alert(config.baseURL);
        $.ajax({
            url: config.baseURL + this.url,
            //url:  this.url,
            data: this.data,
            processData: false,
            contentType: 'application/json',
            type: this.type,
            
            timeout: 0,
            statusCode: {
                403: function () {
                    console.log('403::')
                }
            },
            
            //headers: authorizationObj,
            beforeSend: (data) => {
                // if (!this.isAuditCall && !this._hideLoading) {
                //     utilityaction.showLoading(true);
                // }else{
                    //utilityaction.showLoading(false);
                //}
            },
            success: function (data) {
                return callback(data);

            }, error: (errorObj) => {
                
            }
        });
        
    }

    private setParams = (url: string, type: string, callback: Function, data?: {}, headers?: {}) => {
        this.url = url;
        this.type = type;
        this.data = data;
        this.fetch(callback);
    }

    private handleError = (errorObj: any) => {
        //utilityaction.showLoading(false);
        let messageText = 'Server Error. Please try again.';
        let userAlreadyLoggedIn: boolean = false;
        let data: any = '';
        if (errorObj && errorObj.responseText) {
            data = JSON.parse(errorObj.responseText);
            messageText = JSON.parse(errorObj.responseText).error
            // if (data && data.message && data.message == "AlreadyLoggedIn") {
            //     userAlreadyLoggedIn = true;
            // } else {
            //     messageText = JSON.parse(errorObj.responseText).error
            // }
        }

        console.log('error occured on parsing the request ', data);

        if (errorObj.status == 401) {
            let browserReload = false;
            messageText = JSON.parse(errorObj.responseText).error;
            if (sessionStorage.getItem('reloaded') && sessionStorage.getItem('reloaded') == 'true') {
                messageText = "Your session has been terminated as you have refreshed the browser.";
                browserReload = true;
            }

            let alertData: any = {
                callback() {

                    modalactioncreator.closeModal();
                    if (!browserReload) {
                        setTimeout(() => {
                            location.reload()
                        }, 0)
                    }
                    storageManager.clearStorage();

                },
                messageText: messageText,
                title: 'Error!'
            }
            //RouterBase.gotoLogin();
            modalactioncreator.openAlertBox(alertData);
        } else {
            let alertData: any = {
                callback() {
                },
                messageText: messageText,
                title: 'Error!'
            }
            modalactioncreator.openAlertBox(alertData);

        }


        // messageText = data.error.message;
        // if (userAlreadyLoggedIn) {
        //     let confirmObj: any = {
        //         callback: (isOk: boolean) => {
        //             if (isOk) {
        //                 this.forceLogin()
        //             } else {

        //             }
        //         },
        //         messageText: 'User is already logged in. Do you want to continue?',
        //         title: 'User already loggedin'
        //     }

        //     modalactioncreator.openConfirmBox(confirmObj);
        // } else {
        //     let alertData: any = {
        //         callback() {
        //         },
        //         messageText: messageText,
        //         title: 'Error!'
        //     }
        //     modalactioncreator.openAlertBox(alertData);
        // }
    }

}


/* if (errorObj && errorObj.responseText) {
            console.log('errorObj::', errorObj);
            if (typeof errorObj.responseText === 'string') {
                let obj;
                try {
                    obj = JSON.parse(errorObj.responseText);
                    messageText = JSON.parse(errorObj.responseText);
                } catch (error) {
                    messageText = obj;
                }
            } else {
                messageText = errorObj
            }
        }*/