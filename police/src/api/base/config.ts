
interface IConfig {
    baseURL: string;
    baseURL2?: string;
    baseURL_chat?: string;
    timeout: number;
    responseType: string;
    requestType: string;
    isAsync: boolean;
    url: string;
    baseURL_admin: string,
    socketURL_chat?: string
}

interface Window {
    _chatUrl: any;
}


var customDomain: any = window.location.protocol + '//' + window.location.hostname + ':';

// var customDomain: any = window.location.protocol + '//' + window.location.hostname + ':';
var environment: any = window.location.host.split(".")[0];
export const config: IConfig = {

    // baseURL: 'http://ec2-35-154-14-77.ap-south-1.compute.amazonaws.com:3000/',
    // baseURL_admin: customDomain + '4000/', /**dev url authoring module*/
    // baseURL: customDomain + '3000/', /**dev url */

    // baseURL_admin: 'http://192.168.2.237:4000/', /**dev url authoring module*/
    // baseURL: 'http://192.168.2.237:3000/', /**dev url */

    // baseURL: 'https://demo.cymorg.com/', /**Production */
    // baseURL_admin: 'https://demo.cymorg.com/', /**Production */

    //  baseURL_admin: 'https://staging.cymorg.com/', /**Staging */
    //  baseURL: 'https://staging.cymorg.com/', /**Staging  */

    //  socketURL_chat: 'wss://staging-chat.cymorg.com/websocket',/**staging url */
    //  baseURL_chat: 'https://staging-chat.cymorg.com/api/v1/',//staging url,

    baseURL_admin: window._API_ADMIN_BASE_URL,
    baseURL: window._API_BASE_URL,


    // baseURL_admin: '', /**Relative */
    // baseURL: '', /**Relative */


    // baseURL_admin: 'https://nucleus.cymorg.com/', /**Staging */
    // baseURL: 'https://nucleus.cymorg.com/', /**Staging  */

    // socketURL_chat: 'wss://chat.cymorg.com/websocket',/**demo url */
    // baseURL_chat: 'https://chat.cymorg.com/api/v1/',//demo url,


    // socketURL_chat: 'wss://' + sessionStorage.getItem('_chatUrl') + '/websocket',/**env based url */
    // baseURL_chat: 'https://' + sessionStorage.getItem('_chatUrl') + '/api/v1/',//env based url,

    //  socketURL_chat: 'wss://' + environment + '-chat.cymorg.com/websocket',/**env based url */
    //  baseURL_chat: 'https://' + environment + '-chat.cymorg.com/api/v1/',//env based url,



    // baseURL2: 'https://demo.cymorg.com/socket/', 
    isAsync: true,
    requestType: 'get',
    responseType: 'json',
    timeout: 10000,
    url: ''
    // headers: {
    //     'Accept': location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
    // },
};
