import * as React from 'react';
import './timeline.css';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
'reactstrap';
export default class BlockchainTimeline extends React.Component<any,any> {
    constructor(props?: any, context?: any) {
    super(props, context);

    }

    componentDidMount() {

    };

    componentWillUnmount() {

    }

    render() {

    return (

    <div className="carSection blockChainContainer">
        <div className="steperContainer">
            <div className="steperDataContainer bookingStatusBar">
                <Button color="link" className="stepetBackbtn">Back</Button>
                <h3 className="steperDatatilte">Buy a New Car</h3>

                <div className="statusbox">
                    <span className="statusTitle">Booking ID</span>
                    <p className="statusSub">C23R334</p>
                </div>
                <div className="statusbox">
                    <span className="statusTitle">Booking ID</span>
                    <button className="homeLoanbtn btn btn-outline-secondary">Awaiting Bank Loan</button>
                </div>
            </div>
        </div>
        <div className="dealerDetailsContainer">
            <div className="dealerDetailsWraper">
                <div className="dealerTilteWraper">
                    <div className="titleWraper">
                        <h2>Select a Dealer to begin</h2>
                    </div>
                </div>

                <div className="blockchainStepper">
                        <div className="step ">
                            <div className="v-stepper">
                                <div className="circle"></div>
                                <div className="line"></div>
                        </div>

                            <div className="content">
                                <p className="bChainLabel active">Next: Loan Application Approval</p>
                        
                        </div>
                    </div>

                    {/* <!-- active --> */}
                    <div className="step completed">
                            <div className="v-stepper">
                                <div className="circle"></div>
                                <div className="line"></div>
                        </div>

                            <div className="content">
                                <p className="bChainLabel">Loan Application Request</p>
                                <span className="sublabel">Sent on: 13-09-2019</span>

                                <div className="accordianWraper"></div>
                        </div>
                    </div>

                

                    {/* <!-- regular --> */}
                        <div className="step active">
                            <div className="v-stepper">
                            <div className="circle"></div>
                            <div className="line"></div>
                        </div>

                        <div className="content">
                                <p className="bChainLabel">Car Quotation Received</p>
                                <span className="sublabel">Sent on: 13-09-2019</span>
                          
                        </div>
                    </div>
                </div>


            </div>
        </div>


        <div>


        </div>
    </div>

    )
    }

    }