import * as React from 'react';
import Header from '../includes/header';
import './home.css';
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import constants from '../../constants/constant';
import history from '../../history';
import RouterBase from '../utility/routerbase';
import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import userStore from '../../store/users/userstore';
import actions from '../../actions/base/actiontypes';

export default class MyJourney extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);




        this.state = {
            allBookings: []
        };


    }
    private userInfo = storageManager.getValue('userInfo');
    componentDidMount() {
        userStore.addChangeListener(actions.GET_ALL_BOOKINGS, this.getAllBookings);
        userActionCreator.getAllBookings(this.userInfo.useraddress);
        //move this code to vlockchain timeline component did mount
        userStore.addChangeListener(actions.BLOCKCHAIN_TIMELINE, this.setBlockchainTimelineData);

    };

    componentWillUnmount() {
        userStore.removeChangeListener(actions.GET_ALL_BOOKINGS, this.getAllBookings);
        //move this code to vlockchain timeline component did mount
        userStore.removeChangeListener(actions.BLOCKCHAIN_TIMELINE, this.setBlockchainTimelineData);
    }
    
    getAllBookings = (response: any) => {
        if (response) {
            console.log('response', response);
            this.setState({
                allBookings: response.result
            });
        }
    }

    continueJourney = (bookingId: any) => {
        userStore.setBookingId(bookingId);
        this.props.driveACar();
    }
    viewBlockChainTimeline=(bookingId: any)=>{
        alert(bookingId);
        // RouterBase.changeRoute(constants.ROUTES.BLOCKCHAIN_TIMELINE);
        // this.props.history.push(constants.ROUTES.BLOCKCHAIN_TIMELINE)
        
        //move this code to vlockchain timeline component did mount
        userActionCreator.blockChainTimeline(bookingId);
    }
    //move this code to vlockchain timeline component did mount
    setBlockchainTimelineData=(response:any)=>{
        if(response)
        {
            console.log('setBlockchainTimelineData',response)
        }
    }
    render() {

        return (
            <div>
                {this.state.allBookings.length && this.state.allBookings.map((item: any, index: number) => {
                    return <div className="cardLayout bookedcarWraper" key={index}>
                        <div className="cardContainer ">
                            <div className="card active">
                                <div className="cardContent">
                                    <div className="image-wraper carLoanSelection ">
                                        <img src="/public/images/car.svg" alt="" />
                                        <Button outline color="secondary" className="homeLoanbtn">{item.status}</Button>
                                    </div>
                                    <div className="content-wraper">
                                        <h3 className="cardContentTitle bookedcar">Drive a Car</h3>
                                        <p className="bookingId">Booking ID: {item.bookingId}</p>
                                        {/* <img className="arrow" src="/public/images/right-arrow-forward.svg" alt="" /> */}

                                        <div className="blockchainNavWraper">
                                            <a className="cp" onClick={this.viewBlockChainTimeline.bind(this, item.bookingId)}>
                                                <span className="blockNavText">View Blockchain Timeline
                                                        <i className="fa fa-angle-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div className="ContinueBtnWraper">
                                    <a className={'cp'} onClick={this.continueJourney.bind(this, item.bookingId)}>
                                        <span>Continue
                                                <i className="fa fa-angle-right"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                )
                }
            </div>




        )
    }

}