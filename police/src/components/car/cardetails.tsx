import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import actions from '../../actions/base/actiontypes';

import './car.css';
import userActionCreator from '../../actions/user/useractioncreator';
import storageManager from '../../store/base/sessionstoremanager';
import loginStore from '../../store/login/loginstore';
import userStore from '../../store/users/userstore';
import constants from '../../constants/constant';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';

export default class CarDetails extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {

            active: false,
            quoteActive: false,
            activePage: 'carHome',
            bookingnumber: '',
            approveTransactionActive:false
        };
    }

    componentDidMount() {
        loginStore.addChangeListener(actions.SEND_TRANSACTION, this.getTransactionDetails);
        loginStore.addChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getTransactionStatus);
        userStore.addChangeListener(actions.SEND_CAR_QUOTATION,  this.afterSendQuotation);
        userStore.addChangeListener(actions.SAVE_BOOKING, this.changePage.bind(this,'transaction_success'));
        
    };

    componentWillUnmount() {
        loginStore.removeChangeListener(actions.SEND_TRANSACTION, this.getTransactionDetails);
        loginStore.removeChangeListener(actions.CHECK_TRANSACTION_STATUS, this.getTransactionStatus);
        userStore.removeChangeListener(actions.SEND_CAR_QUOTATION,  this.afterSendQuotation);
        userStore.removeChangeListener(actions.SAVE_BOOKING,  this.changePage.bind(this,'transaction_success'));
    }

    private checkTransactionStatusInterval: any;
    private getTransactionDetails = (response: any) => {
        if (response) {
            this.setState({
                bookingnumber: response.bookingnumber,
                approveTransactionActive:true
            })
            userStore.setBookingId(response.bookingnumber);
            //checking status in every 3sec
            this.checkTransactionStatusInterval = setInterval(this.checkTransactionStatus, 3000);

        }

    }
    private checkTransactionStatus = () => {
        let userInfo = storageManager.getValue('userInfo');
        userActionCreator.checkTransactionStatus(userInfo.useraddress);
        //userActionCreator.checkTransactionStatus('0xdfc77789e1e6eaeb884a3b631648539532cb78e2');
    }
    getQuote = () => {
        let userInfo = storageManager.getValue('userInfo');
        let data = {
            useraddress: userInfo.useraddress,
            //useraddress: '0xdfc77789e1e6eaeb884a3b631648539532cb78e2',
            transactiontype: constants.TRANSACTION.CAR_BOOKING,
            bookingid: ''
        }
        userActionCreator.sendTransaction(data);
    }
    afterSendQuotation=(response: any)=>{
        let userInfo = storageManager.getValue('userInfo');
        if(response && response.message)
        {
            let bookingDetails={
                bookingno: this.state.bookingnumber,
                useraddress:userInfo.useraddress,
                manufacturer : "Toyota",
                model: "Yaris",
                color : "Sunset Orange",
                price : "AED 58700",
                quotationsent:true,
                cardealername:"Al Futtaim Motors"
            }
            userActionCreator.saveBooking(bookingDetails);
        }
        
    }
    getTransactionStatus = (response: any) => {
        if (response && response.status) {
            //save booking api
            clearInterval(this.checkTransactionStatusInterval);
            let userInfo = storageManager.getValue('userInfo');
            //alert('success');
            //if transaction success
            //send car quotaion api
            let data={
                useraddress : userInfo.useraddress,
                credentialtype : "carquote",
                bookingid : this.state.bookingnumber
            }
            userActionCreator.sendCarQuotation(data);

            
            
        }
        else {
            //login failed
            //remove this codes
            // clearInterval(this.checkTransactionStatusInterval);
            // let userInfo = storageManager.getValue('userInfo');
            // //alert('success');
            // //if transaction success
            // //send car quotaion api
            // let data={
            //     useraddress : userInfo.useraddress,
            //     credentialtype : "carquote",
            //     bookingid : this.state.bookingnumber
            // }
            // userActionCreator.sendCarQuotation(data);
        }
    }
    private toggleHover = () => {
        this.setState({
            active: !this.state.active,

        });
    }
    private quoteToggleHover = () => {
        this.setState({

            quoteActive: !this.state.quoteActive,
        });
    }
    private changePage = (activePage: any) => {
        if (activePage == 'approve_transaction') {
            this.getQuote();
        }
        this.setState({
            activePage: activePage
        })

    }
    private closeModal = () => {
        if (userStore.bookingId) {
            userActionCreator.getBookingStatus(userStore.bookingId);
        }
        ModalPopupActionCreator.closeModal()
    }
    render() {

        return (
            <div>
                {this.state.activePage == 'carHome' && <div className="Navimages">
                    <img src="/public/images/buyacar.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'manufacturer')} className="buyNav cp" onMouseEnter={this.toggleHover}
                        onMouseLeave={this.toggleHover}>
                        <div className={this.state.active ? "imageNavWraper active" : "imageNavWraper"} ><span>BUY A NEW CAR</span></div></a>
                </div>}
                {this.state.activePage == 'manufacturer' && <div className="Navimages carDealers">
                    <img src="/public/images/Car-flow/Group 231.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'models')} className="carDealernav cp">
                        </a>
                </div>}
                {this.state.activePage == 'models' && <div className="Navimages carDealers selectCar">
                    <img src="/public/images/Car-flow/Group 232.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'quotes')} className="carDealernav selectcarNav cp">
                        </a>
                </div>}
                {this.state.activePage == 'quotes' && <div className="Navimages Quote">
                    <img src="/public/images/Car-flow/Group 233.png" alt="" />
                    <a onClick={this.changePage.bind(this, 'approve_transaction')} className="QuoteNav active cp" onMouseEnter={this.quoteToggleHover}
                        onMouseLeave={this.quoteToggleHover}>
                        <div className={this.state.quoteActive ? "quoteNavWraper active" : "quoteNavWraper"} ><span>GET A QUOTE</span></div></a>
                </div>}

                
                {this.state.activePage == 'approve_transaction' && <div className="Navimages mobaprovalSection">
                <img src="/public/images/Car-flow/Group 234.png" alt="" />
                    <div className="mobaprovalContainer">
                        <div className="mobileContainer">
                        
                        <div className="mobileWraper ">
                            <a >
                                <div className={this.state.approveTransactionActive?"mobile active":"mobile"}></div>
                            </a>
                            
                        </div>
                        <div className="mobileText">
                            <p className="approvaltext">Tap <span>Approve Transaction</span> in your mobile phone to continue...</p>
                            

                            
                            </div>
                        </div>

                        </div>
             </div>
                }
                {//transaction_success
                    this.state.activePage == 'transaction_success' && <div className="Navimages mobaprovalSection">
                <img src="/public/images/Car-flow/Group 234.png" alt="" />
                    <div className="mobaprovalContainer">
                        <div className="mobileContainer">
                        
                        <div onClick={this.closeModal} className="mobileWraper verified cp">
                            <a >
                                <div className="mobile"></div>
                            </a>
                            
                        </div>
                        <div className="mobileText">
                            
                            
                            <p className="verifiedText">You will receive a quote in your mobile phone in a moment</p>
                            <span className="verifiedIDno">Your Booking Reference: {this.state.bookingnumber}</span>

                            
                            </div>
                        </div>

                        </div>
             </div>
                }
            </div>
        )
    }

}


