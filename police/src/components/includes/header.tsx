import * as React from 'react';
import './header.css';
import storageManager from '../../store/base/sessionstoremanager';


export default class Header extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);

    }

    componentDidMount() {
        //console.log('userDetails111',userDetails);
    };

    componentWillUnmount() {

    }

    render() {
        let userInfo=storageManager.getValue('userInfo');
        return (
            <div className="navbar">
                <div className="logoWraper">
                    <div className="logoImageWraper">
                        <img src="/public/images/brand.png" alt="" />
                    </div>
                </div>

                <div className="loginDetailsContainer">
                    <div className="avatar">
                        <img src="/public/images/avatar.svg" alt="" />
                    </div>
                    <span>Logged in as:</span>
                    <p>{(userInfo && userInfo.status)?`${userInfo.userfulldetails['First Name']} ${userInfo.userfulldetails['Second Name']}`:''}</p>
                </div>
            </div>


        )
    }

}


