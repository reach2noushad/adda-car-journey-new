export class AppConstants {

  public static TITLE = [
    {
      id: 1,
      name: 'Mr',
    },
    {
      id: 2,
      name: 'Mrs'
    },
    {
      id: 3,
      name: 'Ms'
    },
    {
      id: 4,
      name: 'Others'
    },
  ];

}
