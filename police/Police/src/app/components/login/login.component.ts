import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiCalls } from '../../Apiservice/apiCalls';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CommonServiceService } from '../../Apiservice/common-service.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  passwordType: any = true;
  loginForm: any;

  constructor(
    private fb: FormBuilder,
    private apiCalls: ApiCalls,
    private snackBar: MatSnackBar,
    private router: Router,
    private commonServiceService: CommonServiceService) {
    this.loginForm = this.fb.group({
      username: ['police1', [Validators.required, Validators.minLength(7), Validators.maxLength(15)]],
      password: ['password', [Validators.required, Validators.maxLength(8), Validators.minLength(6)]],
      actortype: 'police'
    });
  }

  ngOnInit() {
  }

  changePasswordType(): any {
    this.passwordType = !this.passwordType;
  }

  onLoginSubmit(): any {
    const params = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
      actortype: this.loginForm.value.actortype
    };
    this.apiCalls.loginApi(params).subscribe(res => {
      this.commonServiceService.setUserResponse(res);
      this.commonServiceService.setUserLoggedStatusTrue();
      this.router.navigate(['/vehicle-registration']);
    }, err => {
      if (!err.error.msg) {
        err.error.msg = 'Could not complete the request';
      }
      this.snackBar.open(err.error.msg, '', {
        duration: 2000,
      });
    });
  }
}

