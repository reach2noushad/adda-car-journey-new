import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../../../Apiservice/common-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  headerMenus: any = [
    { name: 'Dashboard', url: '/dashboard' },
    { name: 'Vehicle Registration', url: '/vehicle-registration' },
    { name: 'Fine Management', url: '' },
    { name: 'Dispatch Reports', url: '' },
  ];
  userItemsArrayReferred: any;
  constructor(
    private router: Router,
    private commonServiceService: CommonServiceService) {
    this.userItemsArrayReferred = JSON.parse(localStorage.getItem('isUserLogged'));
  }

  ngOnInit() {
  }

  goLogout(): any {
    this.commonServiceService.goLogOut();
  }

}
