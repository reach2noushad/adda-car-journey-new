import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiCalls } from '../../../Apiservice/apiCalls';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonServiceService } from '../../../Apiservice/common-service.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  displayedColumns: string[] = ['Application Date', 'Ref.Reference', 'Booking ID',
    'Car Manufacture', 'Model', 'Registration Status', 'action'];
  historySource: any = [];
  sideNavDetails: any;
  filteredArray: any = [];
  historyForm: any;
  constructor(
    private apiCalls: ApiCalls, private commonServiceService: CommonServiceService,
    private formBuilder: FormBuilder,
  ) {
    this.commonServiceService.historyRefferd.subscribe((res) => { if (res) { this.ngOnInit(); } });
  }

  ngOnInit() {
    this.historyForm = this.formBuilder.group({
      searchText: [''],
      SortBy: ['Recent'],
    });
    this.apiCalls.getHistory().subscribe(res => {
      res.approvedregrequests.map((key) => {
        key.registrationapplieddate = key.vehicleregdetails.registrationapplieddate;
        key.vehicelregistrationrefno = key.vehicleregdetails.vehicelregistrationrefno;
        key.bookingno = key.vehicleregdetails.bookingno;
        key.carmake = key.bookingdetails.carmake;
        key.carmodel = key.bookingdetails.carmodel;
        key.registrationapproveddate = key.vehicleregdetails.registrationapproveddate;
        key.registrationapplieddate = key.vehicleregdetails.registrationapplieddate;
      });
      this.historySource = res.approvedregrequests;
      this.filteredArray = [...this.historySource];
    }, err => {
      this.historySource = [];
    });
    this.historyForm.controls.searchText.valueChanges.subscribe(
      (selectedValue) => {
        if (this.historySource) {
          this.filteredArray = this.commonServiceService.searchInTable(this.historySource, selectedValue);
        }
      }
    );
  }

  openSideNav(element): void {
    this.sideNavDetails = element;
    document.getElementById('history-sideNav').style.width = '576px';
  }
  closeSideNav(): void {
    document.getElementById('history-sideNav').style.width = '0';
  }

}
