import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../Apiservice/apiCalls';
import { CommonServiceService } from '../../../Apiservice/common-service.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  displayedColumns: string[] = ['Application Date', 'Ref Number', 'Booking ID',
    'First Name', 'Last Name', 'Car Manufacture', 'Model', ''];
  requestSource: any = [];
  changedIndex: any;
  filteredArray: any = [];
  requestForm: any = [];
  constructor(
    private apiCalls: ApiCalls,
    private commonServiceService: CommonServiceService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
  ) {

  }

  ngOnInit() {
    this.requestForm = this.formBuilder.group({
      searchText: [''],
      SortBy: ['Recent'],
    });
    this.apiCalls.getPendingRequest().subscribe(res => {
      if (res && res.pendingrequests) {
        res.pendingrequests.map((key) => {
          key.showLoader = false;
          key.completedLoader = false;
          key.registrationapplieddate = key.policeDetails.registrationapplieddate;
          key.vehicelregistrationrefno = key.policeDetails.vehicelregistrationrefno;
          key.bookingno = key.policeDetails.bookingno;
          key.applicantfname = key.loandetails.applicantfname;
          key.applicantlname = key.loandetails.applicantlname;
          key.carmake = key.bookingdetails.carmake;
          key.carmodel = key.bookingdetails.carmodel;
          key.insurancevendor = key.insurancedetails.insurancevendor;
          key.premiumamount = key.insurancedetails.premiumamount;
          key.smartpasssubmission = key.policeDetails.smartpasssubmission;
          key.carquotesubmission = key.policeDetails.carquotesubmission;
          key.drivinglicensesubmission = key.policeDetails.drivinglicensesubmission;
          key.insurancereceiptsubmission = key.policeDetails.insurancereceiptsubmission;
        });
        this.requestSource = res.pendingrequests;
        this.filteredArray = [...this.requestSource];
      }
    }, err => {
      this.requestSource = [];
      this.filteredArray = [];
    });
    this.requestForm.controls.searchText.valueChanges.subscribe(
      (selectedValue) => {
        if (this.requestSource) {
          this.filteredArray = this.commonServiceService.searchInTable(this.requestSource, selectedValue);
        }
      }
    );

  }

  resetLoader(): any {
    this.requestSource.map((key) => { key.showLoader = false; key.completedLoader = false; });
    this.filteredArray = [...this.requestSource];
  }

  getSourceIndex(index: any): any {
    this.changedIndex = index;
    this.resetLoader();
  }
  changeIndexToNull(): any {
    this.changedIndex = '';
    this.resetLoader();
  }

  callApprove(index, element): any {
    this.filteredArray.map((key, value) => {
      if (value === index) {
        this.filteredArray[value].showLoader = true;
      } else {
        this.filteredArray[value].showLoader = false;
      }
    });
    const param = {
      policerefno: element.policeDetails.vehicelregistrationrefno,
      bookingid: element.policeDetails.bookingno,
      policeapprovedate: this.commonServiceService.formateDate()
    };
    this.apiCalls.approveRequest(param).subscribe(res => {
      this.filteredArray[index].showLoader = false;
      this.filteredArray[index].completedLoader = true;
      setTimeout(() => {
        this.filteredArray[index].completedLoader = false;
      }, 2500);
      setTimeout(() => {
        this.ngOnInit();
        this.commonServiceService.callHistoryApi();
        this.changedIndex = '';
      }, 2500);

    }, err => {
      this.filteredArray[index].showLoader = false;
      if (!err.error.msg) {
        err.error.msg = 'Could not complete the request';
      }
      this.snackBar.open(err.error.msg, '', {
        duration: 2000,
      });
    });
  }

}
