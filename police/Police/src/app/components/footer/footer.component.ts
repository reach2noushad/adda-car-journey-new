import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  footerContents: any = [
    { name: 'Terms & Conditions', url: '' },
    { name: 'Accessibility Policy', url: '' },
    { name: 'Privacy Policy', url: '' },
    { name: 'Site Map', url: '' },
    { name: ' Copyright', url: '' },
    { name: ' Customer Service Center', url: '' },
    { name: 'Disclaimer', url: '' }];
  constructor() { }

  ngOnInit() {
  }

}
