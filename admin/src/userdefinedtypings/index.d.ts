interface JQueryStatic {
    curCSS: any;
}

interface Window {
    _chatURL_API: any;
    _chatURL_SOCKET: any;
    _API_BASE_URL: any;
    _API_ADMIN_BASE_URL: any;
}