import { EventEmitter } from 'events';

class Store extends EventEmitter {
    constructor() {
        super();
    }

    protected _dispatchToken: string;
    emitChange(CHANGE_EVENT: string, callbackData?: any) {
        this.emit(CHANGE_EVENT, callbackData);
    }

    addChangeListener(CHANGE_EVENT: string, callback: EventListener) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(CHANGE_EVENT: string, callback: EventListener) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    /**
     *  gets the dispatch token variable
     */
    public get dispatchToken(): string {
        return this._dispatchToken;
    }

    /**
     *  sets the dispatch token variable
     */
    public set dispatchToken(dispatchToken: string) {
        this._dispatchToken = dispatchToken;
    }
}

export default Store;
