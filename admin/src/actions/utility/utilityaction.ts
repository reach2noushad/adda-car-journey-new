import actions from '../base/actiontypes';
import ActionBase from '../base/actionbase';

/**
 * Utility action creator helper class
 */

interface Action {
    actionType: string;
    data: {};
}
class UtilityActionCreator extends ActionBase {
    
    // gotoBackPage(): void {
    //     this.dispatchAction(actions.GOTO_PREVIOUSPAGE);
    // }


    // showLoading(value: boolean): void {
    //     this.dispatchAction(actions.SHOW_LOADING, value);
    // }
    

    // removeMenuActive(): void {
    //     this.dispatchAction(actions.REMOVE_MENU_ACTIVE);
    // }
    // makeMenuActive(menuItem: any): void {
    //     this.dispatchAction(actions.MAKE_MENU_ACTIVE,menuItem);
    // }
}

export default new UtilityActionCreator();