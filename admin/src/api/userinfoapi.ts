import Promise from './base/promise'
import { URLs } from './base/urls'

import { config } from './base/config';



class UserApi extends Promise {
    constructor() {
        super()
    }
    
    // userLogout = (callback: Function) => {
    //     this.post(URLs.LOGOUT, callback, {});
    // }
    sendTransaction=(data:any,callback: Function)=>{
        
        this.post(URLs.SENDTRANSACTION, callback, data);
    }
    checkTransactionStatus=(data:any,callback: Function)=>{
        
        this.get(`mobile/isresponsereceived?useraddress=${data}`, callback);
    }
    sendCarQuotation=(data:any,callback: Function)=>{
        
        this.post(`mobile/sendcredential`, callback,data);
    }
    saveBooking=(data:any,callback: Function)=>{
        
        this.post(`bookings/save`, callback,data);
    }
    getAllBookings=(useraddress:any,callback: Function)=>{
        this.get(`user/bookings?useraddress=${useraddress}`, callback);
    }
    getBookingStatus=(bookingId:any,callback: Function)=>{
        this.get(`bookings/status?id=${bookingId}`, callback);
    }
    blockChainTimeline=(bookingId:any,callback: Function)=>{
        this.get(`bookings/blockchaintimeline?id=${bookingId}`, callback);
    }
}

export default new UserApi()