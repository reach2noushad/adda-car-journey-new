import * as React from 'react';

import constants from '../constants/constant';
// import modalPopupActionCreator from '../../actions/utility/modalpopupaction';
import Header from '../components/includes/header';
// import { enums } from '../utility/enums';
import loginStore from '../store/login/loginstore';
// import dashboardStore from '../../store/dashboard/dashboardstore';
import actions from '../actions/base/actiontypes';
// import utilityStore from '../../store/utility/utilitystore';
import RouterBase from '../components/utility/routerbase';
import storageManager from '../store/base/sessionstoremanager';
import ModalPopup from '../components/utility/modalpopup';


interface iState {
    activePage?: any;
    activeTab?: any;
    openTrendReports?: any;
    openAnalysisReports?: any;
    reportType?: any;
    storedTrendReportIcons?: any[];
    openMinimizedReports?: any[];
    showGroupMembers?: any;
    showStatus?: any;
}
export default class BaseContainer extends React.Component<any, iState> {
    constructor(props?: any, context?: any) {
        super(props, context);

    }

    private backToPrevPageButtonClicked: boolean = false

    componentWillMount() {
        
        let userDetails:any=storageManager.getValue('userInfo');
        
        if (!userDetails || (userDetails && !userDetails.status) || (userDetails &&userDetails.useraddress == '') ) {
            RouterBase.gotoLogin();
            this.props.history.push(constants.ROUTES.LOGIN)
        }
        
    }
    componentDidMount() {
      
        
    }

    componentWillUnmount() {
    }

    componentWillReceiveProps() {
        
    }

   

  


    


    // private changeRoutes() {
    //     // let activePage: string
    //     // if (this.props.route.path !== "*")
    //     //     activePage = routerHelper.setPageBasedonRoute(this.props.route.path)
    //     // else
    //     //     activePage = (this.props.location.pathname).toUpperCase()
    //     // console.log(this.props.routeParams.page)
    //     let activePage = routerHelper.setPageBasedonRoute(this.props.route.path);

    // }


    render() {
        
        return (
            <div>
                
                <div className="main" >
                    <Header />
                    {this.props.children}
                    <ModalPopup />
                </div>
                
            </div>
        )
    }
}
