import * as React from 'react';
//import AlertBox from '../components/utility/alertbox';
import ModalPopup from '../components/utility/modalpopup';
// import ConfirmBox from '../components/utility/confirmbox';
// import SignoutBox from '../components/utility/signoutbox';
// import LoadingIcon from '../components/utility/loadingicon';
// import ResumeGameContainer from '../components/utility/resumegamecontainer';
// import AddNotePopup from '../components/dashboard/notes/addnotesmodal';
// import DeleteConfirmBox from '../components/utility/deleteconfirmbox';
// import dashboardActionCreator from '../actions/dashboard/dashboardactioncreator';
// import userActionCreator from '../actions/user/useractioncreator';
// import userstore from '../store/users/userstore';
// import dashboardStore from '../store/dashboard/dashboardstore';
// import actions from '../actions/base/actiontypes';
export default class Base extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
    }

    componentDidMount() {
        
    }


    componentWillUnmount() {
        
    }

    

    // <LoadingIcon/>
    render() {
        return (
            <div>
                {this.props.children}
                {/* <AlertBox /> */}
                <ModalPopup />
                {/* <ConfirmBox />
                <SignoutBox />
                <LoadingIcon />
                <ResumeGameContainer />
                <AddNotePopup />
                <DeleteConfirmBox /> */}
            </div>
        )
    }

}


