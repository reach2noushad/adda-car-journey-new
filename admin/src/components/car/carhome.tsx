import * as React from 'react';
import Header from '../includes/header';
import CarDetails from './cardetails';
import ModalPopupActionCreator from '../../actions/utility/modalpopupaction';
import './car.css';
// import {Button} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from
    'reactstrap';
import RouterBase from '../utility/routerbase';
import constants from '../../constants/constant';
import userStore from '../../store/users/userstore';
import Timeline from '../../components/includes/timeline';
//var Modal = require('react-bootstrap-modal')

export default class CarHome extends React.Component<any, any> {
    constructor(props?: any, context?: any) {
        super(props, context);
        this.state = {
            showPopup: false,
            modal: false,
            active:false,
            timelineData:[]

        };
        this.toggle = this.toggle.bind(this);


    }

    componentDidMount() {
        //alert(userStore.bookingId);
    };

    componentWillUnmount() {

    }
    private timelineData:any=[];
    setTimelineData=(timelineData:any)=>{
        
        this.timelineData=timelineData
        
    }
    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
    private cancel = () => {
        //ModalPopupActionCreator.closeModal()
    }
    toggle() {
        ModalPopupActionCreator.openModal(<CarDetails />, true, 'md')
        // this.setState({
        //     modal: !this.state.modal
        // });

    }
    
    private toggleHover = () => {
        this.setState({
            active: !this.state.active
        });
    }
    render() {

        return (

            <div className="carSection">
                <Timeline setTimelineData={this.setTimelineData}/>
                {/* <div className="steperContainer">
                    <div className="steperDataContainer">
                        <Button onClick={this.backToPage} color="link" className="stepetBackbtn">Back</Button>
                        <h3 className="steperDatatilte">Buy a New Car</h3>

                        <div className="steperWraper">

                            <div className="">

                                <div className="step completed">
                                    <div className="v-stepper">
                                        <div className="circle"><span>1</span></div>
                                        <div className="line"></div>
                                    </div>

                                    <div className="content">
                                        <div className="stepDetailsWraper">
                                            <div className="details">
                                                <span className="stepPosition">Step 1</span>
                                                <p className="stepLabel">Buy a Car</p>
                                            </div>
                                            <div className="chkmark"></div>
                                        </div>
                                        <div className="subText active">
                                            <span>Car Quotation Received</span>
                                            <p>13-09-2019</p>
                                        </div>

                                    </div>


                                </div>


                                <div className="step active">
                                    <div className="v-stepper">
                                        <div className="circle">
                                            <span>2</span>
                                        </div>
                                        <div className="line"></div>
                                    </div>

                                    <div className="content">
                                        <div className="stepDetailsWraper">
                                            <div className="details">
                                                <span className="stepPosition">Step 2</span>
                                                <p className="stepLabel">Get Financing</p>
                                            </div>
                                            <div className="chkmark"></div>
                                        </div>
                                        <div className="subText">
                                            <span>Car Quotation Received</span>
                                            <p>13-09-2019</p>
                                        </div>
                                    </div>
                                </div>


                                <div className="step">
                                    <div className="v-stepper">
                                        <div className="circle">3</div>
                                        <div className="line"></div>
                                    </div>
                                    <div className="content">
                                        <div className="stepDetailsWraper">
                                            <div className="details">
                                                <span className="stepPosition">Step 3</span>
                                                <p className="stepLabel">Insure your Car</p>
                                            </div>
                                            <div className="chkmark"></div>
                                        </div>
                                        <div className="subText">
                                            <span>Car Quotation Received</span>
                                            <p>13-09-2019</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="step">
                                    <div className="v-stepper">
                                        <div className="circle">4</div>
                                        <div className="line"></div>
                                    </div>
                                    <div className="content">
                                        <div className="stepDetailsWraper">
                                            <div className="details">
                                                <span className="stepPosition">Step 4</span>
                                                <p className="stepLabel">Register your Car</p>
                                            </div>
                                            <div className="chkmark"></div>
                                        </div>
                                        <div className="subText">
                                            <span>Car Quotation Received</span>
                                            <p>13-09-2019</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div> */}
                <div className="dealerDetailsContainer">
                    <div className="dealerDetailsWraper">
                        <div className="dealerTilteWraper">
                            <div className="titleWraper">
                                <h2>Select a Dealer to begin</h2>
                            </div>
                            <div className="searchWraper">
                                <FormGroup>
                                    <Input type="search" className="search" id="Search" placeholder="search placeholder" />
                                </FormGroup>
                            </div>

                        </div>
                        <div className="applyCardContainer">
                            <div className="cardLayout cp" onMouseEnter={this.toggleHover}
                                onMouseLeave={this.toggleHover} onClick={this.toggle}>
                                <div className="cardContainer">
                                    <div className={this.state.active ? "card active" : "card"}>
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img src="/public/images/al_futtaim.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">Al Futtaim Motors Company LLC - Abu Dhabi</h3>
                                                
                                                {this.state.active && <div className="siteNav active">
                                                    <a >Visit Website</a>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout">
                                <div className="cardContainer">
                                    <div className="card  ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img src="/public/images/habtoor.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">Al Habtoor Royal Car LLC</h3>
                                                
                                                <div className="siteNav ">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout">
                                <div className="cardContainer">
                                    <div className="card  ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img src="/public/images/masaood.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle applytitle">Al Masaood Automobiles Company LLC</h3>
                                                
                                                <div className="siteNav ">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className="cardLayout" onClick={this.toggle}>
                                <div className="cardContainer">
                                    <div className="card active">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img style={{height:'50px',width:'50px'}} src="/public/images/al_futtaim.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Al Futtaim Motors Company LLC - Abu Dhabi</h3>
                                                
                                                <div className="siteNav active">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout" onClick={this.toggle}>
                                <div className="cardContainer">
                                    <div className="card ">
                                        <div className="cardContent">
                                            <div  className="image-wraper">
                                                <img style={{height:'50px',width:'50px'}} src="/public/images/habtoor.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Al Habtoor Royal Car LLC</h3>
                                                
                                                <div className="siteNav">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cardLayout" onClick={this.toggle}>
                                <div className="cardContainer">
                                    <div className="card ">
                                        <div className="cardContent">
                                            <div className="image-wraper">
                                                <img style={{height:'50px',width:'50px'}} src="/public/images/masaood.png" alt="" />
                                            </div>
                                            <div className="content-wraper">
                                                <h3 className="cardContentTitle">Al Masaood Automobiles Company LLC</h3>
                                                
                                                <div className="siteNav">
                                                    <a href="">Visit Website</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                    </div>


                </div>


                <div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggle}></ModalHeader>
                        <ModalBody>

                            <CarDetails />
                        </ModalBody>
                        {/* <ModalFooter>
                    <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
                    <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                </ModalFooter> */}
                    </Modal>
                </div>
            </div>
        )
    }

}