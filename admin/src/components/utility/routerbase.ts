import { Router, Route, Switch, BrowserRouter,HashRouter } from 'react-router-dom';
import constants from '../../constants/constant';
//import CookieManager from './cookies';
// import { IUserInfo } from '../../interface/interface';
import loginStore from '../../store/login/loginstore';
import history from '../../history';

// import adminActioncreator from '../../actions/administator/adminactioncreator';
import modalactioncreator from '../../actions/utility/modalpopupaction';
// import adminstore from '../../store/admin/adminstore';
/** Base class for routing */
export default class RouterBase {

    private static routesArray: any = [];
    /**changeRoute method pushes new path to the history */
    public static changeRoute = (path: string) => {
        RouterBase.routesArray.push(path);
        // if (loginStore.userRole == constants.USERS.ADMIN || loginStore.userRole == constants.USERS.GLOBAL_ADMIN) {
        //     if (adminStore._isSaved == false) {
        //         let Data: any = {
        //             callback(confirm: boolean) {
        //                 if (confirm) {
        //                     RouterBase.routesArray.push(path);
        //                     hashHistory.push(path);
        //                     adminStore.setIfSaved(true);
        //                 }
        //                 else {
        //                     //let data = adminStore.selectedCustomEvent;
        //                 }
        //             },
        //             messageText: loginStore.dialogues.LEAVE_PAGE_WITHOUT_SAVE,
        //             title: 'Confirm Navigation'
        //         }
        //         modalactioncreator.openConfirmBox(Data);
        //     }
        //     else {
        //         RouterBase.routesArray.push(path);
        //         hashHistory.push(path);
        //     }
        // }
        // else {
        //     RouterBase.routesArray.push(path);
        //     hashHistory.push(path);
        // }        //hashHistory.push(path);
        // browserHistory.push(path);
    }

    
    

    /**gotoLogin routes the application to the Login page */
    public static gotoLogin = () => {
        RouterBase.changeRoute(constants.ROUTES.LOGIN);
    }

    public static gotoDashBoard = () => {
        RouterBase.changeRoute(constants.ROUTES.DASHBOARD);
    }

    
    // public static gotoDynamicUI = () => {

    //     let pageName = dashboardstore.currentParameterGroupInfo.name.replace(/\s/g, '').toLowerCase();
    //     hashHistory.push('info/' + pageName);
    // }

    public static getRoutesArray = () => {
        return RouterBase.routesArray;
    }
    public static clearRoutesArray = () => {
        RouterBase.routesArray = []
    }
    // public static routeChange = (nextState: any, replaceState: any) => {

    //     let userDetails: any = CookieManager.getCookie('userName');
    //     if (userDetails && userDetails.length > 0) {
    //         if (nextState.location.pathname == constants.ROUTES.ADMIN && loginStore.userRole == constants.USERS.ADMIN && loginStore.userRole == constants.USERS.GLOBAL_ADMIN) {
    //             alert('Access Denied');
    //             replaceState({ nextPathname: nextState.location.pathname }, constants.ROUTES.LOGIN);
    //         }

    //     } else {
    //         replaceState({ nextPathname: nextState.location.pathname }, constants.ROUTES.LOGIN);
    //     }

    // }

}