import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiCalls } from './Apiservice/apiCalls';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AppCommonModule } from './components/common/app-common.module';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatMenuModule} from '@angular/material/menu';
import { JourneyComponent } from './components/journey/journey.component';
import { AdminComponent } from './components/admin/admin.component';
import { HeaderComponent } from './components/admin/header/header.component';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { CarJourneyComponent } from './components/journey/car-journey/car-journey.component';
import { BuildHomeComponent } from './components/journey/build-home/build-home.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    JourneyComponent,
    AdminComponent,
    HeaderComponent,
    SidebarComponent,
    CarJourneyComponent,
    BuildHomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    AppCommonModule,
    MatTabsModule,
    MatTableModule,
    MatSelectModule,
    FormsModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatMenuModule
  ],
  providers: [ApiCalls],
  bootstrap: [AppComponent]
})
export class AppModule { }
