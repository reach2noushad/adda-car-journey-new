import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  config = {
    panels: [
      {
        name: 'Journeys', routerLink: '/admin/journey', img: 'card_travel', activeRoute: false,
        subPannels: [
          { name: 'Car Journey', icon: 'directions_car', routerSubLink: '/admin/journey/car-journey' },
          { name: 'Build a Home', icon: 'home', routerSubLink: '/admin/journey/build-a-home' }
        ]
      },
      {
        name: 'Services', routerLink: '/admin/services', img: 'settings_applications', activeRoute: false,
        subPannels: [{}]
      },
    ]
  };

  constructor(
    private router: Router // Used in HTML
  ) {

  }

  ngOnInit() {
    this.activateRoutesCheck();
  }

  changeRoutes(subs): any {
    this.router.navigate([subs.routerSubLink]);
    this.activateRoutesCheck();
  }

  changesRoutesMain(): any {

  }


  activateRoutesCheck(): any {
    this.config.panels.map((key, value) => {
      if (this.router.url.indexOf(key.routerLink) > -1) {
        this.config.panels[value].activeRoute = true;
      } else {
        key.subPannels.forEach((ke) => {
          if (this.router.url.indexOf(ke.routerSubLink) > -1) {
            this.config.panels[value].activeRoute = true;
          } else {
            this.config.panels[value].activeRoute = false;
          }
        });
      }
    });

  }




}
