import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CommonServiceService } from '../../../Apiservice/common-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userItemsArrayReferred: any;
  componentName: any;
  constructor(
    private commonServiceService: CommonServiceService,
    private router: Router,

  ) {

    if (this.router.url.indexOf('/admin/journey')  > -1) {
      this.componentName = 'Journey';
    }

  }

  ngOnInit() {
    this.userItemsArrayReferred = JSON.parse(localStorage.getItem('isUserLogged'));
  }

  goLogout() {
    this.commonServiceService.goLogOut();
  }

}
