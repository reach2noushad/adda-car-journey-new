import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarJourneyComponent } from './car-journey.component';

describe('CarJourneyComponent', () => {
  let component: CarJourneyComponent;
  let fixture: ComponentFixture<CarJourneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarJourneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarJourneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
