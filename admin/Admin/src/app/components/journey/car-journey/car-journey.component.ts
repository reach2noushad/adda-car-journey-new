import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../Apiservice/apiCalls';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonServiceService } from '../../../Apiservice/common-service.service';
@Component({
  selector: 'app-car-journey',
  templateUrl: './car-journey.component.html',
  styleUrls: ['./car-journey.component.css']
})
export class CarJourneyComponent implements OnInit {

  carForm: any = [];
  carJourney: any;
  sideNavDetails: any = '';
  panelOpenState: any = false;
  panelOpenStateBank: any = false;
  panelOpenStateBank2: any = false;
  panelOpenStateInsurance: any = false;
  panelOpenStateRegistration: any = false;
  panelOpenStateRegistration2: any = false;
  filteredArray: any = [];
  selectedElement: any;
  constructor(
    private apiCalls: ApiCalls,
    private formBuilder: FormBuilder,
    private commonServiceService: CommonServiceService) { }

  ngOnInit() {
    this.carForm = this.formBuilder.group({
      searchText: [''],
      SortBy: ['Recent'],
    });
    this.apiCalls.getCarJourneyList().subscribe((res) => {
      if (res && res.carbooking) {
        this.carJourney = res.carbooking;
        this.filteredArray = [...this.carJourney];
      }
    }, err => {
      this.carJourney = [];
      this.filteredArray = [];
    });
    this.carForm.controls.searchText.valueChanges.subscribe(
      (selectedValue) => {
        if (this.carJourney) {
          this.filteredArray = this.commonServiceService.searchInTable(this.carJourney, selectedValue);
        }
      }
    );
  }


  openSideNav(element): void {
    this.selectedElement = element;
    this.apiCalls.getCarJourneySingleList(element.bookingid).subscribe((res) => {
      if (res) {
        this.sideNavDetails = res;
      }
    }, err => {
      this.sideNavDetails = '';
    });
    document.getElementById('car-journey-sideNav').style.width = '676px';
  }

  closeSideNav(): void {
    document.getElementById('car-journey-sideNav').style.width = '0';
  }


}
