import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { NavigationEnd, Router } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { JourneyComponent } from './components/journey/journey.component';
import { CarJourneyComponent } from './components/journey/car-journey/car-journey.component';
import { BuildHomeComponent } from './components/journey/build-home/build-home.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent, data: { title: 'Admin | Login', description: '', ogUrl: '' }
  },
  {
    path: 'login', component: LoginComponent, data: { title: 'Admin | Login', description: '', ogUrl: '' }
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'journey', component: JourneyComponent, data: {
          title: 'Admin | Journey',
          description: '', ogUrl: ''
        },
        children: [{
          path: 'car-journey', component: CarJourneyComponent, data: {
            title: 'Admin | Car Journey',
            description: '', ogUrl: ''
          }
        },
        {
          path: 'build-a-home', component: BuildHomeComponent, data: {
            title: 'Admin | Build a Home',
            description: '', ogUrl: ''
          }
        }]
      },
    ]
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private title: Title, private meta: Meta
  ) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        window.scroll(0, 0);
      }
    });
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    )
      .subscribe((event) => {
        const userEntries = JSON.parse(localStorage.getItem('isUserAdminLogged'));
        if (!userEntries || !userEntries.isLogged) {
          this.router.navigate(['/login']);
        } else {
          if (this.router.url === '/login' || this.router.url === '/') {
            this.router.navigate(['/journey']);
          } else {
            this.router.navigate([this.router.url]);
          }
        }
        this.title.setTitle(event.title);
        this.meta.updateTag({ name: 'og:url', content: event.ogUrl });
        this.meta.updateTag({ name: 'description', content: event.description });
      });
  }
}
