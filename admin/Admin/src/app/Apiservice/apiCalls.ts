import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrls } from './apiUrls';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ApiCalls {
    private baseUrl = 'http://115.114.36.146:3600/';
    constructor(public http: HttpClient) {

    }

    loginApi(postData: any): any {
        return this.http
            .post(this.baseUrl + ApiUrls.USER_LOGIN_POST, postData).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

    getCarJourneyList(): any {
        return this.http
            .get(this.baseUrl + ApiUrls.CAR_JOURNEY_LIST_GET).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

    getCarJourneySingleList(params): any {
        return this.http
            .get(this.baseUrl + ApiUrls.CAR_JOURNEY_SINGLE_LIST_GET + params).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

}
