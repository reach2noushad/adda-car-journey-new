import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  userItems: any = { isLogged: '', response: '' };


  constructor(private router: Router) {

  }

  setUserLoggedStatusTrue(): void {
    this.userItems.isLogged = true;
    this.setLocal();
  }

  setUserLoggedStatusFalse(): void {
    this.userItems.isLogged = false;
    this.setLocal();
  }

  setUserResponse(response: any): any {
    this.userItems.response = response;
    this.setLocal();
  }

  setLocal(): any {
    localStorage.setItem('isUserAdminLogged', JSON.stringify(this.userItems));
  }

  goLogOut(): any {
    this.userItems = { isLogged: '', response: '' };
    this.setLocal();
    this.router.navigate(['/login']);
  }

  searchInTable(originalArray, searchText): any {
    const saveOriginalArray = originalArray;
    if (!originalArray.length) {
      originalArray = [];
      return originalArray;
    }
    if (!searchText) {
      originalArray = [...saveOriginalArray];
    } else {
      const users = [...originalArray];
      const properties = Object.keys(users[0]);
      originalArray = users.filter((user) => {
        return properties.find((property) => {
          const valueString = user[property].toString().toLowerCase();
          return valueString.includes(searchText.toLowerCase());
        })
          ? user
          : null;
      });
      if (originalArray.length === 0 && saveOriginalArray.length > 0) {
        originalArray = [...saveOriginalArray];
      }
    }
    return originalArray;
  }



}
