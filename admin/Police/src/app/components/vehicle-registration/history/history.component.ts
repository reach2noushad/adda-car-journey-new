import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCalls } from '../../../Apiservice/apiCalls';
import { keyframes } from '@angular/animations';
import { CommonServiceService } from '../../../Apiservice/common-service.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  displayedColumns: string[] = ['Application Date', 'Ref.Reference', 'Booking ID',
    'Car Manufacture', 'Model', 'Registration Status', 'action'];
  historySource: any;
  SortBy: any = 'Recent';
  sideNavDetails: any;
  constructor(
    private apiCalls: ApiCalls, private commonServiceService: CommonServiceService
  ) {
    this.commonServiceService.historyRefferd.subscribe((res) => { if (res) { this.ngOnInit(); } });
  }

  ngOnInit() {
    this.apiCalls.getHistory().subscribe(res => {
      res.approvedregrequests.map((key) => { key.showLoader = false; key.completedLoader = false; });
      this.historySource = res.approvedregrequests;
    }, err => {
      this.historySource = [];
    });
  }

  openSideNav(element): void {
    this.sideNavDetails = element;
    document.getElementById('history-sideNav').style.width = '576px';
  }
  closeSideNav(): void {
    document.getElementById('history-sideNav').style.width = '0';
  }

}
