import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../Apiservice/apiCalls';
import { CommonServiceService } from '../../../Apiservice/common-service.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  displayedColumns: string[] = ['Application Date', 'Ref Number', 'Booking ID',
    'First Name', 'Last Name', 'Car Manufacture', 'Model', ''];
  requestSource: any;
  SortBy: any = 'Recent';
  changedIndex: any;

  constructor(
    private apiCalls: ApiCalls,
    private commonServiceService: CommonServiceService,
    private snackBar: MatSnackBar,
  ) {

  }

  ngOnInit() {
    this.apiCalls.getPendingRequest().subscribe(res => {
      if (res && res.pendingrequests) {
        res.pendingrequests.map((key) => { key.showLoader = false; key.completedLoader = false; });
      }
      this.requestSource = res.pendingrequests;
    }, err => {
      this.requestSource = [];
    });

  }

  resetLoader(): any {
    this.requestSource.map((key) => { key.showLoader = false; key.completedLoader = false; });
  }

  getSourceIndex(index: any): any {
    this.changedIndex = index;
    this.resetLoader();
  }
  changeIndexToNull(): any {
    this.changedIndex = '';
    this.resetLoader();
  }

  callApprove(index, element): any {
    this.requestSource.map((key, value) => {
      if (value === index) {
        this.requestSource[value].showLoader = true;
      } else {
        this.requestSource[value].showLoader = false;
      }
    });
    setTimeout(() => {
      this.requestSource[index].showLoader = false;
    }, 2000);
    const param = {
      policerefno: element.policeDetails.vehicelregistrationrefno,
      bookingid: element.policeDetails.bookingno,
      policeapprovedate: this.commonServiceService.formateDate()
    };
    this.apiCalls.approveRequest(param).subscribe(res => {
      setTimeout(() => {
        this.requestSource[index].completedLoader = true;
      }, 2000);
      setTimeout(() => {
        this.ngOnInit();
        this.commonServiceService.callHistoryApi();
        this.changedIndex = '';
      }, 2000);
    }, err => {
      this.requestSource[index].showLoader = false;
      if (!err.error.msg) {
        err.error.msg = 'Could not complete the request';
      }
      this.snackBar.open(err.error.msg, '', {
        duration: 2000,
      });
    });
  }

}
