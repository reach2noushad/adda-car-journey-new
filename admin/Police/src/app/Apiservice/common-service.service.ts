import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  userItems: any = { isLogged: '', response: '' };
  currentDate: any = new Date();
  public history = new BehaviorSubject(false);
  historyRefferd = this.history.asObservable();

  constructor(private router: Router) {

  }

  setUserLoggedStatusTrue(): void {
    this.userItems.isLogged = true;
    this.setLocal();
  }

  setUserLoggedStatusFalse(): void {
    this.userItems.isLogged = false;
    this.setLocal();
  }

  setUserResponse(response: any): any {
    this.userItems.response = response;
    this.setLocal();
  }

  setLocal(): any {
    localStorage.setItem('isUserLogged', JSON.stringify(this.userItems));
  }

  goLogOut(): any {
    this.userItems = { isLogged: '', response: '' };
    this.setLocal();
    this.router.navigate(['/login']);
  }

  formateDate(): any {
    const mm = this.currentDate.getMonth() + 1;
    const dd = this.currentDate.getDate();
    return [(dd > 9 ? '' : '0') + dd + '-', (mm > 9 ? '' : '0') + mm + '-', this.currentDate.getFullYear(),
    ].join('');
  }

  callHistoryApi(): any {
    this.history.next(true);
  }

}
