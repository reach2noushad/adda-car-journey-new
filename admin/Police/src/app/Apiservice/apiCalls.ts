import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrls } from './apiUrls';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ApiCalls {
    private baseUrl = 'http://115.114.36.146:3600/';
    constructor(public http: HttpClient) {

    }

    loginApi(postData: any): any {
        return this.http
            .post(this.baseUrl + ApiUrls.USER_LOGIN_POST, postData).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

    getPendingRequest(): any {
        return this.http
            .get(this.baseUrl + ApiUrls.PENDING_REQUESTS_GET).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

    approveRequest(postData: any): any {
        return this.http
            .post(this.baseUrl + ApiUrls.APPROVE_POST, postData).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }

    getHistory(): any {
        return this.http
            .get(this.baseUrl + ApiUrls.HISTORY_GET).pipe(
                map((response: any) => {
                    return response;
                }),
            );
    }
}
