export class ApiUrls {
    public static USER_LOGIN_POST = 'user/login';
    public static PENDING_REQUESTS_GET = 'police/pendingrequests';
    public static APPROVE_POST = 'police/approve';
    public static HISTORY_GET = 'police/history';

}
